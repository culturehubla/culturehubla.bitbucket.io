"use strict";

var CABLES=CABLES||{};
CABLES.OPS=CABLES.OPS||{};

var Ops=Ops || {};
Ops.Ui=Ops.Ui || {};
Ops.Gl=Ops.Gl || {};
Ops.Anim=Ops.Anim || {};
Ops.Math=Ops.Math || {};
Ops.User=Ops.User || {};
Ops.Patch=Ops.Patch || {};
Ops.Json3d=Ops.Json3d || {};
Ops.Gl.GLTF=Ops.Gl.GLTF || {};
Ops.Trigger=Ops.Trigger || {};
Ops.Boolean=Ops.Boolean || {};
Ops.WebAudio=Ops.WebAudio || {};
Ops.Gl.Phong=Ops.Gl.Phong || {};
Ops.Gl.Shader=Ops.Gl.Shader || {};
Ops.Gl.Meshes=Ops.Gl.Meshes || {};
Ops.Gl.Matrix=Ops.Gl.Matrix || {};
Ops.Gl.CubeMap=Ops.Gl.CubeMap || {};
Ops.Gl.Geometry=Ops.Gl.Geometry || {};
Ops.Gl.Textures=Ops.Gl.Textures || {};
Ops.Math.Compare=Ops.Math.Compare || {};
Ops.User.julianstein=Ops.User.julianstein || {};
Ops.Gl.ShaderEffects=Ops.Gl.ShaderEffects || {};
Ops.Gl.TextureEffects=Ops.Gl.TextureEffects || {};



// **************************************************************
// 
// Ops.Gl.MainLoop
// 
// **************************************************************

Ops.Gl.MainLoop = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const fpsLimit=op.inValue("FPS Limit",0);
const trigger=op.outTrigger("trigger");
const width=op.outValue("width");
const height=op.outValue("height");
const reduceLoadingFPS=op.inValueBool("Reduce FPS loading");
const clear=op.inValueBool("Clear",true);
const clearAlpha=op.inValueBool("ClearAlpha",true);
const fullscreen=op.inValueBool("Fullscreen Button",false);
const active=op.inValueBool("Active",true);
const hdpi=op.inValueBool("Hires Displays",false);

op.onAnimFrame=render;
hdpi.onChange=function()
{
    if(hdpi.get()) op.patch.cgl.pixelDensity=window.devicePixelRatio;
        else op.patch.cgl.pixelDensity=1;

    op.patch.cgl.updateSize();
    if(CABLES.UI) gui.setLayout();
};

active.onChange=function()
{
    op.patch.removeOnAnimFrame(op);

    if(active.get())
    {
        op.setUiAttrib({"extendTitle":""});
        op.onAnimFrame=render;
        op.patch.addOnAnimFrame(op);
        op.log("adding again!");
    }
    else
    {
        op.setUiAttrib({"extendTitle":"Inactive"});
    }

};

var cgl=op.patch.cgl;
var rframes=0;
var rframeStart=0;

if(!op.patch.cgl) op.uiAttr( { 'error': 'No webgl cgl context' } );

var identTranslate=vec3.create();
vec3.set(identTranslate, 0,0,0);
var identTranslateView=vec3.create();
vec3.set(identTranslateView, 0,0,-2);

fullscreen.onChange=updateFullscreenButton;
setTimeout(updateFullscreenButton,100);
var fsElement=null;

function updateFullscreenButton()
{
    function onMouseEnter()
    {
        if(fsElement)fsElement.style.display="block";
    }

    function onMouseLeave()
    {
        if(fsElement)fsElement.style.display="none";
    }

    op.patch.cgl.canvas.addEventListener('mouseleave', onMouseLeave);
    op.patch.cgl.canvas.addEventListener('mouseenter', onMouseEnter);

    if(fullscreen.get())
    {
        if(!fsElement)
        {
            fsElement = document.createElement('div');

            var container = op.patch.cgl.canvas.parentElement;
            if(container)container.appendChild(fsElement);

            fsElement.addEventListener('mouseenter', onMouseEnter);
            fsElement.addEventListener('click', function(e)
            {
                if(CABLES.UI && !e.shiftKey) gui.cycleRendererSize();
                else cgl.fullScreen();
            });
        }

        fsElement.style.padding="10px";
        fsElement.style.position="absolute";
        fsElement.style.right="5px";
        fsElement.style.top="5px";
        fsElement.style.width="20px";
        fsElement.style.height="20px";
        fsElement.style.cursor="pointer";
        fsElement.style['border-radius']="40px";
        fsElement.style.background="#444";
        fsElement.style["z-index"]="9999";
        fsElement.style.display="none";
        fsElement.innerHTML='<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 490 490" style="width:20px;height:20px;" xml:space="preserve" width="512px" height="512px"><g><path d="M173.792,301.792L21.333,454.251v-80.917c0-5.891-4.776-10.667-10.667-10.667C4.776,362.667,0,367.442,0,373.333V480     c0,5.891,4.776,10.667,10.667,10.667h106.667c5.891,0,10.667-4.776,10.667-10.667s-4.776-10.667-10.667-10.667H36.416     l152.459-152.459c4.093-4.237,3.975-10.99-0.262-15.083C184.479,297.799,177.926,297.799,173.792,301.792z" fill="#FFFFFF"/><path d="M480,0H373.333c-5.891,0-10.667,4.776-10.667,10.667c0,5.891,4.776,10.667,10.667,10.667h80.917L301.792,173.792     c-4.237,4.093-4.354,10.845-0.262,15.083c4.093,4.237,10.845,4.354,15.083,0.262c0.089-0.086,0.176-0.173,0.262-0.262     L469.333,36.416v80.917c0,5.891,4.776,10.667,10.667,10.667s10.667-4.776,10.667-10.667V10.667C490.667,4.776,485.891,0,480,0z" fill="#FFFFFF"/><path d="M36.416,21.333h80.917c5.891,0,10.667-4.776,10.667-10.667C128,4.776,123.224,0,117.333,0H10.667     C4.776,0,0,4.776,0,10.667v106.667C0,123.224,4.776,128,10.667,128c5.891,0,10.667-4.776,10.667-10.667V36.416l152.459,152.459     c4.237,4.093,10.99,3.975,15.083-0.262c3.992-4.134,3.992-10.687,0-14.82L36.416,21.333z" fill="#FFFFFF"/><path d="M480,362.667c-5.891,0-10.667,4.776-10.667,10.667v80.917L316.875,301.792c-4.237-4.093-10.99-3.976-15.083,0.261     c-3.993,4.134-3.993,10.688,0,14.821l152.459,152.459h-80.917c-5.891,0-10.667,4.776-10.667,10.667s4.776,10.667,10.667,10.667     H480c5.891,0,10.667-4.776,10.667-10.667V373.333C490.667,367.442,485.891,362.667,480,362.667z" fill="#FFFFFF"/></g></svg>';
    }
    else
    {
        if(fsElement)
        {
            fsElement.style.display="none";
            fsElement.remove();
            fsElement=null;
        }
    }
}

fpsLimit.onChange=function()
{
    op.patch.config.fpsLimit=fpsLimit.get()||0;
};

op.onDelete=function()
{
    cgl.gl.clearColor(0,0,0,0);
    cgl.gl.clear(cgl.gl.COLOR_BUFFER_BIT | cgl.gl.DEPTH_BUFFER_BIT);
};

op.patch.loading.setOnFinishedLoading(function(cb)
{
    op.patch.config.fpsLimit=fpsLimit.get();
});


function render(time)
{
    if(!active.get())return;
    if(cgl.aborted || cgl.canvas.clientWidth===0 || cgl.canvas.clientHeight===0)return;

    const startTime=performance.now();

    if(op.patch.loading.getProgress()<1.0 && reduceLoadingFPS.get())
    {
        op.patch.config.fpsLimit=5;
    }

    if(cgl.canvasWidth==-1)
    {
        cgl.setCanvas(op.patch.config.glCanvasId);
        return;
    }

    if(cgl.canvasWidth!=width.get() || cgl.canvasHeight!=height.get())
    {
        width.set(cgl.canvasWidth);
        height.set(cgl.canvasHeight);
    }

    if(CABLES.now()-rframeStart>1000)
    {
        CGL.fpsReport=CGL.fpsReport||[];
        if(op.patch.loading.getProgress()>=1.0 && rframeStart!==0)CGL.fpsReport.push(rframes);
        rframes=0;
        rframeStart=CABLES.now();
    }
    CGL.MESH.lastShader=null;
    CGL.MESH.lastMesh=null;

    cgl.renderStart(cgl,identTranslate,identTranslateView);

    if(clear.get())
    {
        cgl.gl.clearColor(0,0,0,1);
        cgl.gl.clear(cgl.gl.COLOR_BUFFER_BIT | cgl.gl.DEPTH_BUFFER_BIT);
    }

    trigger.trigger();

    if(CGL.MESH.lastMesh)CGL.MESH.lastMesh.unBind();

    if(CGL.Texture.previewTexture)
    {
        if(!CGL.Texture.texturePreviewer) CGL.Texture.texturePreviewer=new CGL.Texture.texturePreview(cgl);
        CGL.Texture.texturePreviewer.render(CGL.Texture.previewTexture);
    }
    cgl.renderEnd(cgl);

    if(clearAlpha.get())
    {
        cgl.gl.clearColor(1, 1, 1, 1);
        cgl.gl.colorMask(false, false, false, true);
        cgl.gl.clear(cgl.gl.COLOR_BUFFER_BIT);
        cgl.gl.colorMask(true, true, true, true);
    }

    if(!cgl.frameStore.phong)cgl.frameStore.phong={};
    rframes++;

    CGL.profileData.profileMainloopMs=performance.now()-startTime;

};











};

Ops.Gl.MainLoop.prototype = new CABLES.Op();
CABLES.OPS["b0472a1d-db16-4ba6-8787-f300fbdc77bb"]={f:Ops.Gl.MainLoop,objName:"Ops.Gl.MainLoop"};




// **************************************************************
// 
// Ops.Gl.Phong.LambertMaterial
// 
// **************************************************************

Ops.Gl.Phong.LambertMaterial = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={lambert_frag:"{{MODULES_HEAD}}\n\nIN vec3 norm;\nIN vec4 modelPos;\n\n// UNI mat4 normalMatrix;\nIN mat3 normalMatrix; // when instancing...\n\nIN vec2 texCoord;\n\nIN vec3 mvNormal;\nIN vec3 mvTangent;\nIN vec3 mvBiTangent;\n\nUNI vec4 color;//r,g,b,a;\n\nstruct Light {\n  vec3 pos;\n  vec3 color;\n  vec3 ambient;\n  vec3 specular;\n  float falloff;\n  float radius;\n  float mul;\n};\n\nUNI Light lights[NUM_LIGHTS];\n\nfloat getfallOff(Light light,float distLight)\n{\n    float denom = distLight / light.radius + 1.0;\n    float attenuation = 1.0 / (denom*denom);\n    float t = (attenuation - 0.1) / (1.0 - 0.1);\n\n    t=t* (20.0*light.falloff*20.0*light.falloff);\n\n    return min(1.0,max(t, 0.0));\n}\n\nvoid main()\n{\n    {{MODULE_BEGIN_FRAG}}\n\n    vec4 col=vec4(0.0);\n    vec3 normal = normalize(mat3(normalMatrix)*norm);\n\n    #ifdef DOUBLE_SIDED\n    if(!gl_FrontFacing) normal = normal*-1.0;\n    #endif\n\n    for(int l=0;l<NUM_LIGHTS;l++)\n    {\n        Light light=lights[l];\n\n        vec3 lightModelDiff=light.pos - modelPos.xyz;\n        vec3 lightDir = normalize(lightModelDiff);\n        vec3 lambert = vec3( max(dot(lightDir,normal), 0.0) );\n\n        vec3 newColor=lambert * light.color.rgb * light.mul;\n\n        newColor*=getfallOff(light, length(lightModelDiff));\n\n        col.rgb+=vec3(light.ambient);\n        col.rgb+=newColor;\n        // col.rgb=light.color.rgb;\n    }\n\n    col.rgb*=color.rgb;\n    col.a=color.a;\n\n    {{MODULE_COLOR}}\n\n    outColor=col;\n}\n",lambert_vert:"IN vec3 vPosition;\nIN vec3 attrVertNormal;\nIN vec2 attrTexCoord;\n\nIN vec3 attrTangent;\nIN vec3 attrBiTangent;\nIN float attrVertIndex;\n\nUNI mat4 projMatrix;\nUNI mat4 modelMatrix;\nUNI mat4 viewMatrix;\n\nOUT vec3 norm;\nOUT mat4 mvMatrix;\nOUT mat3 normalMatrix;\nOUT vec4 modelPos;\nOUT vec2 texCoord;\n{{MODULES_HEAD}}\n\nmat3 transposeMat3(mat3 m)\n{\n    return mat3(m[0][0], m[1][0], m[2][0],\n        m[0][1], m[1][1], m[2][1],\n        m[0][2], m[1][2], m[2][2]);\n}\n\nmat3 inverseMat3(mat3 m)\n{\n    float a00 = m[0][0], a01 = m[0][1], a02 = m[0][2];\n    float a10 = m[1][0], a11 = m[1][1], a12 = m[1][2];\n    float a20 = m[2][0], a21 = m[2][1], a22 = m[2][2];\n\n    float b01 = a22 * a11 - a12 * a21;\n    float b11 = -a22 * a10 + a12 * a20;\n    float b21 = a21 * a10 - a11 * a20;\n\n    float det = a00 * b01 + a01 * b11 + a02 * b21;\n\n    return mat3(b01, (-a22 * a01 + a02 * a21), (a12 * a01 - a02 * a11),\n        b11, (a22 * a00 - a02 * a20), (-a12 * a00 + a02 * a10),\n        b21, (-a21 * a00 + a01 * a20), (a11 * a00 - a01 * a10)) / det;\n}\n\nvoid main()\n{\n    vec4 pos = vec4( vPosition, 1. );\n    mat4 mMatrix=modelMatrix;\n    vec3 tangent=attrTangent,\n        bitangent=attrBiTangent;\n\n    texCoord=attrTexCoord;\n\n    norm=attrVertNormal;\n\n    {{MODULE_VERTEX_POSITION}}\n\n    normalMatrix = transposeMat3(inverseMat3(mat3(mMatrix)));\n\n\n    // this needs only to be done when instancing....\n\n    mvMatrix=viewMatrix*mMatrix;\n    modelPos=mMatrix*pos;\n\n    gl_Position = projMatrix * mvMatrix * pos;\n}\n",};
const execute=op.inTrigger("execute");
const r = op.inValueSlider("diffuse r", Math.random());
const g = op.inValueSlider("diffuse g", Math.random());
const b = op.inValueSlider("diffuse b", Math.random());
const a = op.inValueSlider("diffuse a", 1.0);

const inToggleDoubleSided = op.inBool("Double Sided", false);

inToggleDoubleSided.onChange = function () {
    shader.toggleDefine("DOUBLE_SIDED", inToggleDoubleSided.get());
};

const next=op.outTrigger("next");

r.setUiAttribs({ colorPick: true });

const cgl=op.patch.cgl;
const shader=new CGL.Shader(cgl,"LambertMaterial");
shader.define('NUM_LIGHTS','1');

const colUni=new CGL.Uniform(shader,'4f','color',r,g,b,a);

var outShader=op.outObject("Shader");
outShader.set(shader);

var MAX_LIGHTS=16;
var lights=[];
for(var i=0;i<MAX_LIGHTS;i++)
{
    var count=i;
    lights[count]={};
    lights[count].pos=new CGL.Uniform(shader,'3f','lights['+count+'].pos',[0,11,0]);
    lights[count].target=new CGL.Uniform(shader,'3f','lights['+count+'].target',[0,0,0]);
    lights[count].color=new CGL.Uniform(shader,'3f','lights['+count+'].color',[1,1,1]);
    lights[count].attenuation=new CGL.Uniform(shader,'f','lights['+count+'].attenuation',0.1);
    lights[count].type=new CGL.Uniform(shader,'f','lights['+count+'].type',0);
    lights[count].cone=new CGL.Uniform(shader,'f','lights['+count+'].cone',0.8);
    lights[count].mul=new CGL.Uniform(shader,'f','lights['+count+'].mul',1);
    lights[count].ambient=new CGL.Uniform(shader,'3f','lights['+count+'].ambient',1);
    lights[count].fallOff=new CGL.Uniform(shader,'f','lights['+count+'].falloff',0);
    lights[count].radius=new CGL.Uniform(shader,'f','lights['+count+'].radius',1000);
}

shader.setSource(attachments.lambert_vert,attachments.lambert_frag);

var numLights=-1;
var updateLights=function()
{
    var count=0;
    var i=0;
    var num=0;
    if(!cgl.frameStore.lightStack && (!cgl.frameStore.phong || !cgl.frameStore.phong.lights))
    {
        num=0;
    }
    else
    {
        if (cgl.frameStore.phong) {
            if (cgl.frameStore.phong.lights) {
                for(i in cgl.frameStore.phong.lights) num++;
            }
        }

        for (let light in cgl.frameStore.lightStack) {
            if (cgl.frameStore.lightStack[light].type === "point") {
                num++;
            }
        }
    }

    if(num!=numLights)
    {
        numLights=num;
        shader.define('NUM_LIGHTS',''+Math.max(numLights,1));
    }

    if(!cgl.frameStore.lightStack && (!cgl.frameStore.phong || !cgl.frameStore.phong.lights))
    {
        lights[count].pos.setValue([5,5,5]);
        lights[count].color.setValue([1,1,1]);
        lights[count].ambient.setValue([0.1,0.1,0.1]);
        lights[count].mul.setValue(1);
        lights[count].fallOff.setValue(0.5);
    }
    else
    {
        count=0;
        if(shader) {
            if (cgl.frameStore.phong) {
                if (cgl.frameStore.phong.lights) {
                    let length = cgl.frameStore.phong.lights.length;
                    for(let i = 0; i < length; i +=1) {
                        lights[count].pos.setValue(cgl.frameStore.phong.lights[i].pos);
                        // if(cgl.frameStore.phong.lights[i].changed)
                        {
                            cgl.frameStore.phong.lights[i].changed=false;
                            if(cgl.frameStore.phong.lights[i].target) lights[count].target.setValue(cgl.frameStore.phong.lights[i].target);

                            lights[count].fallOff.setValue(cgl.frameStore.phong.lights[i].fallOff);
                            lights[count].radius.setValue(cgl.frameStore.phong.lights[i].radius);
                            lights[count].color.setValue(cgl.frameStore.phong.lights[i].color);
                            lights[count].ambient.setValue(cgl.frameStore.phong.lights[i].ambient);
                            lights[count].attenuation.setValue(cgl.frameStore.phong.lights[i].attenuation);
                            lights[count].type.setValue(cgl.frameStore.phong.lights[i].type);
                            if(cgl.frameStore.phong.lights[i].cone) lights[count].cone.setValue(cgl.frameStore.phong.lights[i].cone);
                            if(cgl.frameStore.phong.lights[i].depthTex) lights[count].texDepthTex=cgl.frameStore.phong.lights[i].depthTex;

                            lights[count].mul.setValue(cgl.frameStore.phong.lights[i].mul||1);
                        }

                        count++;
                    }
                }
            }
            if (cgl.frameStore.lightStack) {
                if (cgl.frameStore.lightStack.length) {
                        for (let j = 0; j < cgl.frameStore.lightStack.length; j += 1) {
                            const light = cgl.frameStore.lightStack[j];
                                 if (light.type === "point") { // POINT LIGHT
                                    lights[count].pos.setValue(light.position);
                                    lights[count].fallOff.setValue(light.falloff);
                                    lights[count].radius.setValue(light.radius);
                                    lights[count].color.setValue(light.color);
                                    lights[count].ambient.setValue([0, 0, 0]);
                                    lights[count].type.setValue(0); // old point light type index
                                    lights[count].mul.setValue(light.intensity);
                                    count++;
                                 }
                        }
                    }
                }
        }
    }
};

function updateSpecular()
{
    if(inSpecular.get()==1)inSpecular.uniform.setValue(99999);
        else inSpecular.uniform.setValue(Math.exp(inSpecular.get()*8,2));
}

execute.onTriggered=function()
{
    if(!shader)
    {
        console.log("lambert has no shader...");
        return;
    }

    cgl.pushShader(shader);
    updateLights();
    next.trigger();
    cgl.popShader();
};


};

Ops.Gl.Phong.LambertMaterial.prototype = new CABLES.Op();
CABLES.OPS["eae9a731-3712-4891-9c22-dc7f4d80ce66"]={f:Ops.Gl.Phong.LambertMaterial,objName:"Ops.Gl.Phong.LambertMaterial"};




// **************************************************************
// 
// Ops.Gl.GLTF.GltfScene_v2
// 
// **************************************************************

Ops.Gl.GLTF.GltfScene_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};

const CHUNK_HEADER_SIZE=8;

var Gltf=class
{
    constructor()
    {
        this.json={};
        this.accBuffers=[];
        this.meshes=[];
        this.nodes=[];
        this.shaders=[];
        this.timing=[];
        this.startTime=performance.now();
        this.bounds=new CGL.BoundingBox();
    }

    getNode(n)
    {
        for(var i=0;i<this.nodes.length;i++)
        {
            if(this.nodes[i].name==n)return this.nodes[i];
        }
    }
};

function Utf8ArrayToStr(array)
{
    if(window.TextDecoder) return new TextDecoder("utf-8").decode(array);

    var out, i, len, c;
    var char2, char3;

    out = "";
    len = array.length;
    i = 0;
    while(i < len) {
        c = array[i++];
        switch(c >> 4)
        {
            case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
            // 0xxxxxxx
            out += String.fromCharCode(c);
            break;
            case 12: case 13:
            // 110x xxxx   10xx xxxx
            char2 = array[i++];
            out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
            break;
            case 14:
                // 1110 xxxx  10xx xxxx  10xx xxxx
                char2 = array[i++];
                char3 = array[i++];
                out += String.fromCharCode(((c & 0x0F) << 12) |
                    ((char2 & 0x3F) << 6) |
                    ((char3 & 0x3F) << 0));
                break;
        }
    }

    return out;
}

function readChunk(dv,bArr,arrayBuffer,offset)
{
    const chunk={};

    chunk.size=dv.getUint32(offset+0,le);

    // chunk.type = new TextDecoder("utf-8").decode(bArr.subarray(offset+4, offset+4+4));
    chunk.type=Utf8ArrayToStr(bArr.subarray(offset+4, offset+4+4));

    if(chunk.type=="BIN\0")
    {
        chunk.dataView=new DataView(arrayBuffer,offset+8,chunk.size);
    }
    else
    if(chunk.type=="JSON")
    {
        // const json = new TextDecoder("utf-8").decode(bArr.subarray(offset+8, offset+8+chunk.size));
        const json = Utf8ArrayToStr(bArr.subarray(offset+8, offset+8+chunk.size));

        try
        {
            const obj=JSON.parse(json);
            chunk.data=obj;
            outGenerator.set(obj.asset.generator);
        }
        catch(e)
        {
    }
    }
    else
    {
}

    return chunk;
}

function loadAnims(gltf)
{
    var k=0;
    for(var i=0;i<gltf.json.animations.length;i++)
    {
        var an=gltf.json.animations[i];

        for(var ia=0;ia<an.channels.length;ia++)
        {
            var chan=an.channels[ia];

            const node=gltf.nodes[chan.target.node];
            const sampler=an.samplers[chan.sampler];

            const acc=gltf.json.accessors[sampler.input];
            const bufferIn=gltf.accBuffers[sampler.input];

            const accOut=gltf.json.accessors[sampler.output];
            const bufferOut=gltf.accBuffers[sampler.output];

            var numComps=1;
            if(accOut.type=="VEC2")numComps=2;
            else if(accOut.type=="VEC3")numComps=3;
            else if(accOut.type=="VEC4")numComps=4;

            var anims=[];

            for(k=0;k<numComps;k++) anims.push(new CABLES.TL.Anim());

            if(sampler.interpolation=="LINEAR") {}
            else if(sampler.interpolation=="STEP") for(k=0;k<numComps;k++) anims[k].defaultEasing=CONSTANTS.ANIM.EASING_ABSOLUTE;
            else console.warn("[gltf] unknown interpolation",sampler.interpolation);

            for(var j=0;j<bufferIn.length;j++)
            {
                maxTime=Math.max(bufferIn[j],maxTime);

                for(k=0;k<numComps;k++)
                {
                    anims[k].setValue( bufferIn[j], bufferOut[j*numComps+k] );
                }
            }

            node.setAnim(chan.target.path,anims);
        }
    }
}

function parseGltf(arrayBuffer)
{
    var j=0,i=0;

    var gltf=new Gltf();

    if (!arrayBuffer) return;
    var byteArray = new Uint8Array(arrayBuffer);
    var pos=0;

    // var string = new TextDecoder("utf-8").decode(byteArray.subarray(pos, 4));
    var string = Utf8ArrayToStr(byteArray.subarray(pos, 4));
    pos+=4;
    if(string!='glTF')
    {
    return;
    }

    gltf.timing.push("Start parsing",Math.round((performance.now()-gltf.startTime)));

    const dv=new DataView(arrayBuffer);
    const version=dv.getUint32(pos,le);
    pos+=4;
    const size=dv.getUint32(pos,le);
    pos+=4;

    outVersion.set(version);

    var chunks=[];

    chunks.push(readChunk(dv,byteArray,arrayBuffer,pos));
    pos+=chunks[0].size+CHUNK_HEADER_SIZE;
    gltf.json=chunks[0].data;
    outJson.set(gltf.json);

    chunks.push(readChunk(dv,byteArray,arrayBuffer,pos));

    var views=chunks[0].data.bufferViews;
    var accessors=chunks[0].data.accessors;

    gltf.timing.push("Parse buffers",Math.round((performance.now()-gltf.startTime)));


    if(views)
    {
        for(i=0;i<accessors.length;i++)
        {
            const acc=accessors[i];
            const view=views[acc.bufferView];

            var numComps=0;
            if(acc.type=="SCALAR")numComps=1;
            else if(acc.type=="VEC2")numComps=2;
            else if(acc.type=="VEC3")numComps=3;
            else if(acc.type=="VEC4")numComps=4;
            else console.error('unknown accessor type',acc.type);

            var num=acc.count*numComps;
            var accPos = (view.byteOffset||0)+(acc.byteOffset||0);
            var stride = view.byteStride||0;
            var dataBuff=null;


            // 5120 (BYTE)	1
            // 5121(UNSIGNED_BYTE)	1
            // 5122 (SHORT)	2
            if(acc.componentType==5126) // FLOAT
            {
                stride=stride||4;
                dataBuff=new Float32Array(num);

                for(j=0;j<num;j++)
                {
                    dataBuff[j]=chunks[1].dataView.getFloat32(accPos,le);

                    if(stride!=4 && (j+1)%numComps===0)accPos+=stride-(numComps*4);
                    accPos+=4;
                }
            }
            else if(acc.componentType==5123) // UNSIGNED_SHORT
            {
                stride=stride||2;

                dataBuff=new Uint16Array(num);

                for(j=0;j<num;j++)
                {
                    dataBuff[j]=chunks[1].dataView.getUint16(accPos,le);

                    if(stride!=2 && (j+1) % numComps===0) accPos+=stride-(numComps*2);

                    accPos+=2;
                }
            }
            else
            {
                console.error("unknown component type",acc.componentType);
            }

            gltf.accBuffers.push(dataBuff);
        }
    }

    gltf.timing.push("Parse mesh groups",Math.round((performance.now()-gltf.startTime)));

    gltf.json.meshes=gltf.json.meshes||[];

    if(gltf.json.meshes)
    for(i=0;i<gltf.json.meshes.length;i++)
    {
        const mesh=new gltfMeshGroup(gltf,gltf.json.meshes[i]);
        gltf.meshes.push(mesh);
    }

    gltf.timing.push("Parse nodes",Math.round((performance.now()-gltf.startTime)));


    for(i=0;i<gltf.json.nodes.length;i++)
    {
        const node=new gltfNode(gltf.json.nodes[i],gltf);
        gltf.nodes.push(node);
    }

    for(i=0;i<gltf.nodes.length;i++)
    {
        const node=gltf.nodes[i];
        if(!node.isChild) node.calcBounds(gltf,null,gltf.bounds);
    }

    needsMatUpdate=true;

    gltf.timing.push("load anims", Math.round((performance.now()-gltf.startTime)));

    if(gltf.json.animations) loadAnims(gltf);

    gltf.timing.push("finished", Math.round((performance.now()-gltf.startTime)));



    return gltf;

}



var gltfMesh=class
{
    constructor(name,prim,gltf)
    {
        this.test=0;
        this.name=name;
        this.material=prim.material;
        this.mesh=null;
        this.geom=new CGL.Geometry("gltf_"+this.name);
        this.geom.verticesIndices = [];

        if(prim.hasOwnProperty("indices")) this.geom.verticesIndices=gltf.accBuffers[prim.indices];

        this.fillGeomAttribs(gltf,this.geom,prim.attributes);

        if(prim.targets)
            for(var j=0;j<prim.targets.length;j++)
            {
                var tgeom=new CGL.Geometry("gltf_"+this.name);
                if(prim.hasOwnProperty("indices")) tgeom.verticesIndices=gltf.accBuffers[prim.indices];
                this.fillGeomAttribs(gltf,tgeom,prim.targets[j]);
                this.geom.morphTargets.push(tgeom);
            }

        this.morphGeom=this.geom.copy();
        this.bounds=this.geom.getBounds();
    }
    fillGeomAttribs(gltf,geom,attribs)
    {
        if(attribs.hasOwnProperty("POSITION"))geom.vertices=gltf.accBuffers[attribs.POSITION];
        if(attribs.hasOwnProperty("NORMAL"))geom.vertexNormals=gltf.accBuffers[attribs.NORMAL];
        if(attribs.hasOwnProperty("TEXCOORD_0"))geom.texCoords=gltf.accBuffers[attribs.TEXCOORD_0];
        if(attribs.hasOwnProperty("TANGENT"))geom.tangents=gltf.accBuffers[attribs.TANGENT];
        if(attribs.hasOwnProperty("COLOR_0"))geom.vertexColors=gltf.accBuffers[attribs.COLOR_0];

// Implementation note: When normals and tangents are specified,
// client implementations should compute the bitangent by taking
// the cross product of the normal and tangent xyz vectors and
// multiplying against the w component of the tangent:
// bitangent = cross(normal, tangent.xyz) * tangent.w

        if(inSwitchNormalsYZ.get())
        {
            for(var i=0;i<geom.vertexNormals.length;i+=3)
            {
                var t=geom.vertexNormals[i+2];
                geom.vertexNormals[i+2]=geom.vertexNormals[i+1];
                geom.vertexNormals[i+1]=t;
            }
        }


        if(!geom.vertexNormals.length) geom.calculateNormals();


        geom.calcTangentsBitangents();
    }

    render(cgl,ignoreMaterial)
    {
        if(!this.geom)return;

        if(!this.mesh)
        {
            this.mesh=new CGL.Mesh(cgl,this.geom);
        }
        else
        {
            // update morphTargets
            if(this.geom.morphTargets.length)
            {
                this.test=time*11.7;

                if(this.test>=this.geom.morphTargets.length-1)this.test=0;

                const mt=this.geom.morphTargets[Math.floor(this.test)];
                const mt2=this.geom.morphTargets[Math.floor(this.test+1)];

                if(mt && mt.vertices)
                {
                    const fract=this.test%1;
                    for(var i=0;i<this.morphGeom.vertices.length;i++)
                    {
                        this.morphGeom.vertices[i]=
                            this.geom.vertices[i]+
                            (1.0-fract)*mt.vertices[i]+
                            fract*mt2.vertices[i];
                    }

                    this.mesh.updateVertices(this.morphGeom);
                }
            }


            const useMat=!ignoreMaterial && this.material!=-1 && gltf.shaders[this.material];

            // console.log(gltf.shaders);
            // console.log(this.material);

            if(useMat) cgl.pushShader(gltf.shaders[this.material]);

            this.mesh.render(cgl.getShader(),ignoreMaterial);

            if(useMat) cgl.popShader();

        }
    }

};var gltfMeshGroup=class
{
    constructor(gltf,m)
    {
        this.bounds=new CGL.BoundingBox();
        this.meshes=[];
        this.name=m.name;
        const prims=m.primitives;

        for(var i=0;i<prims.length;i++)
        {
            var mesh=new gltfMesh(this.name,prims[i],gltf);
            this.meshes.push(mesh);
            this.bounds.apply(mesh.bounds);
        }

        // console.log("mesh group bounds:",this.bounds._maxAxis);
    }

    render(cgl, ignoreMat)
    {
        for(var i=0;i<this.meshes.length;i++)
        {
            this.meshes[i].render(cgl,ignoreMat);
        }
    }
};

var gltfNode=class
{
    constructor(node,gltf)
    {
        this.isChild=node.isChild||false;
        this.name=node.name;
        this.hidden=false;
        this.mat=mat4.create();
        this._animMat=mat4.create();
        this._tempMat=mat4.create();
        this._tempQuat=quat.create();
        this._tempRotmat=mat4.create();
        this.mesh=null;
        this.children=[];
        this._node=node;


        if(node.translation) mat4.translate(this.mat,this.mat,node.translation);

        if(node.rotation)
        {
            var rotmat=mat4.create();
            this._rot=node.rotation;

            mat4.fromQuat(rotmat,node.rotation);
            mat4.mul(this.mat,this.mat,rotmat);
        }

        if(node.scale)
        {
            this._scale=node.scale;
            mat4.scale(this.mat,this.mat,this._scale);
        }

        if(node.hasOwnProperty("mesh"))
        {
            this.mesh=gltf.meshes[node.mesh];
        }

        if(node.children)
        {
            for(var i=0;i<node.children.length;i++)
            {
                gltf.json.nodes[i].isChild=true;
                if(gltf.nodes[node.children[i]]) gltf.nodes[node.children[i]].isChild=true;
                this.children.push(node.children[i]);
            }
        }
    }

    calcBounds(gltf,mat,bounds)
    {
        var localMat=mat4.create();
        if(mat)
        {
            mat4.copy(localMat,mat);
        }

        // mat=mat||mat4.create();

        if(this.mat)
            mat4.mul(localMat,localMat,this.mat);

        if(this.mesh)
        {
            var bb=this.mesh.bounds.copy();
            bb.mulMat4(localMat);
            bounds.apply(bb);

            boundingPoints.push(bb._min[0],bb._min[1],bb._min[2]);
            boundingPoints.push(bb._max[0],bb._max[1],bb._max[2]);
        }

        for(var i=0;i<this.children.length;i++)
        {
            if(gltf.nodes[this.children[i]] && gltf.nodes[this.children[i]].calcBounds)
            {
                bounds.apply(gltf.nodes[this.children[i]].calcBounds(gltf,localMat,bounds));
            }
        }

        if(bounds.changed)return bounds;
        else return null;
    }

    setAnim(path,anims)
    {
        if(path=="translation")this._animTrans=anims;
        else if(path=="rotation")this._animRot=anims;
        else if(path=="scale")this._animScale=anims;
        else console.warn("unknown anim path",path,anims);
    }

    transform(cgl,_time)
    {
        if(!_time )_time=time;

        if(!this._animTrans)
        {
            mat4.mul(cgl.mMatrix,cgl.mMatrix,this.mat);
        }
        else
        {
            mat4.identity(this._animMat);

            var playAnims=true;

            if(playAnims && this._animTrans)
            {
                mat4.translate(this._animMat,this._animMat,[
                    this._animTrans[0].getValue(time),
                    this._animTrans[1].getValue(time),
                    this._animTrans[2].getValue(time)]);
            }
            else
            if(this.translation) mat4.translate(this._animMat,this._animMat,this.translation);

            if(playAnims && this._animRot)
            {
                CABLES.TL.Anim.slerpQuaternion(time,this._tempQuat,this._animRot[0],this._animRot[1],this._animRot[2],this._animRot[3]);

                mat4.fromQuat(this._tempMat,this._tempQuat);
                mat4.mul(this._animMat,this._animMat,this._tempMat);
            }
            else if(this._rot)
            {
                mat4.fromQuat(this._tempRotmat,this._rot);
                mat4.mul(this._animMat,this._animMat,this._tempRotmat);
            }

            if(playAnims && this._animScale)
            {
                mat4.scale(this._animMat,this._animMat,[
                    this._animScale[0].getValue(time),
                    this._animScale[1].getValue(time),
                    this._animScale[2].getValue(time)]);
            } else if(this._scale) mat4.scale(this._animMat,this._animMat,this._scale);

            mat4.mul(cgl.mMatrix,cgl.mMatrix,this._animMat);
        }
    }

    render(cgl,dontTransform,dontDrawMesh,ignoreMaterial,ignoreChilds,drawHidden,_time)
    {
        // dontTransform,drawMesh,ignoreMaterial,
        if(this.hidden && !drawHidden) return;

        if(!dontTransform) cgl.pushModelMatrix();
        if(!dontTransform) this.transform(cgl,_time||time);

        if(this.mesh && !dontDrawMesh) this.mesh.render(cgl,ignoreMaterial);

        if(!ignoreChilds)
            for(var i=0;i<this.children.length;i++)
                if(gltf.nodes[this.children[i]])
                    gltf.nodes[this.children[i]].render(cgl,dontTransform,dontDrawMesh,ignoreMaterial,ignoreChilds,drawHidden,_time);

        if(!dontTransform)cgl.popModelMatrix();
    }

};var tab=null;

function closeTab()
{
    if(tab)gui.mainTabs.closeTab(tab.id);
}

function printNode(html,node,level)
{
    if(!gltf)return;

    html+='<tr class="row">';
    var i=0;
    var ident="";

    for(i=0;i<level;i++)
    {
        // var last=level-1==i;
        // var identClass=last?"identnormal":"identlast";

        var identClass="identBg";
        if(i==0)identClass="identBgLevel0";
        // if(i==level-1)identClass="identBgLast";
        ident+='<td class="ident  '+identClass+'" ><div style=""></div></td>';
    }
    var id=CABLES.uuid();
    html+=ident;
    html+='<td colspan="'+(20-level)+'">';

    if(node.mesh && node.mesh.meshes.length)html+='<span class="icon icon-cube"></span>&nbsp;';
    else html+='<span class="icon icon-circle"></span> &nbsp;';

    html+=node.name;
    html+='</td>';
    html+='<td>';
    html+='</td>';

    // html+='<td>';

    if(node.mesh)
    {
        html+='<td>';
        for(i=0;i<node.mesh.meshes.length;i++)
        {
            html+=node.mesh.meshes[i].name;
            // html+=' ('+node.mesh.meshes[i].geom.vertices.length/3+' verts) ';
        }
        html+='</td>';
        html+='<td>';
        for(i=0;i<node.mesh.meshes.length;i++)
        {
            if(node.mesh.meshes[i].material)
                html+=gltf.json.materials[node.mesh.meshes[i].material].name;
        }
        html+='</td>';

    }
    else
    {
        html+='<td>-</td><td>-</td>';
    }
    html+='<td>';
    if(node._animRot || node._animScale || node._animTrans) html+='Yes';
    else html+='-';

    html+='</td>';
    html+='<td>';
    var hideclass='';
    if(node.hidden)hideclass='node-hidden';

    html+='<a onclick="gui.patch().scene.getOpById(\''+op.id+'\').exposeNode(\''+node.name+'\')" class="treebutton">Expose</a>';
    html+='&nbsp;';

    html+='<span class="icon iconhover icon-eye '+hideclass+'" onclick="gui.patch().scene.getOpById(\''+op.id+'\').toggleNodeVisibility(\''+node.name+'\');this.classList.toggle(\'node-hidden\');"></span>';
    html+='</td>';

    html+="</tr>";

    if(node.children)
        for(i=0;i<node.children.length;i++)
            html=printNode(html,gltf.nodes[node.children[i]],level+1);

    return html;
}

function printMaterial(mat,idx)
{
    var html='<tr>';
    html+=' <td>'+idx+'</td>';
    html+=' <td>'+mat.name+'</td>';
    // html+=' <td><a onclick="" class="treebutton">Assign</a><td>';



    html+=' <td>';
    if(mat.pbrMetallicRoughness && mat.pbrMetallicRoughness.baseColorFactor)
    {
        var rgb='';
        rgb+=''+Math.round(mat.pbrMetallicRoughness.baseColorFactor[0]*255);
        rgb+=','+Math.round(mat.pbrMetallicRoughness.baseColorFactor[1]*255);
        rgb+=','+Math.round(mat.pbrMetallicRoughness.baseColorFactor[2]*255);

        html+='<div style="width:15px;height:15px;;background-color:rgb('+rgb+')">&nbsp;</a>';

        // html+='<td>';
    }
    html+=' <td style="">'+(gltf.shaders[idx]?"-":'<a onclick="gui.patch().scene.getOpById(\''+op.id+'\').assignMaterial(\''+mat.name+'\')" class="treebutton">Assign</a>')+'<td>';
    html+='<td>';



    // console.log();


    html+='</tr>';
    return html;
}

function printInfo()
{
    if(!gltf)return;

    const sizes={};

    var html='<div style="overflow:scroll;width:100%;height:100%">';


    html+='generator:'+gltf.json.asset.generator;


    if(!gltf.json.materials || gltf.json.materials.length==0)
    {
        html+='<h3>Materials</h3>';
        html+="No materials";
    }
    else
    {
        html+='<h3>Materials ('+gltf.json.materials.length+')</h3>';
        html+='<table class="table treetable">';
        html+='<tr>';
        html+=' <th>Index</th>';
        html+=' <th>Name</th>';
        html+=' <th>Color</th>';
        html+=' <th>Function</th>';
        html+=' <th></th>';
        html+='</tr>';
        for(var i=0;i<gltf.json.materials.length;i++)
        {
            html+=printMaterial(gltf.json.materials[i],i);
        }
        html+='</table>';
    }

    html+='<h3>Nodes ('+gltf.nodes.length+')</h3>';
    html+='<table class="table treetable">';

    html+='<tr>';
    html+=' <th colspan="21">Name</th>';
    html+=' <th>Mesh</th>';
    html+=' <th>Material</th>';
    html+=' <th>Anim</th>';
    html+=' <th>Show</th>';
    html+='</tr>';

    for(var i=0;i<gltf.nodes.length;i++)
    {
        if(!gltf.nodes[i].isChild)
            html=printNode(html,gltf.nodes[i],1);
    }
    html+='</table>';

    html+='<h3>Meshes ('+gltf.json.meshes.length+')</h3>';

    html+='<table class="table treetable">';
    html+='<tr>';
    html+=' <th>Name</th>';
    html+=' <th>Material</th>';
    html+=' <th>Vertices</th>';
    html+=' <th>Attributes</th>';
    html+='</tr>';


    var sizeBufferViews=[];
    sizes.meshes=0;

    for(var i=0;i<gltf.json.meshes.length;i++)
    {
        html+='<tr>';
        html+='<td>'+gltf.json.meshes[i].name+"</td>";

        html+='<td>';
        for(var j=0;j<gltf.json.meshes[i].primitives.length;j++)
        {
            if(gltf.json.meshes[i].primitives[j].material)
                html+=gltf.json.materials[gltf.json.meshes[i].primitives[j].material].name;
                else html+='None';
        }
        html+='</td>';

        html+='<td>';
        for(var j=0;j<gltf.json.meshes[i].primitives.length;j++)
        {
            // html+=gltf.json.meshes[i].primitives[j].indices;
            if(gltf.json.meshes[i].primitives[j].attributes.POSITION)
            {
                html+=gltf.json.accessors[gltf.json.meshes[i].primitives[j].attributes.POSITION].count;

            }
        }
        html+='</td>';

        html+='<td>';
        for(var j=0;j<gltf.json.meshes[i].primitives.length;j++)
            html+=Object.keys(gltf.json.meshes[i].primitives[j].attributes);
        html+='</td>';

        html+='</tr>';


        for(var j=0;j<gltf.json.meshes[i].primitives.length;j++)
        {
            var bufView=gltf.json.accessors[gltf.json.meshes[i].primitives[j].indices].bufferView;

            if(sizeBufferViews.indexOf(bufView)==-1)
            {
                sizeBufferViews.push(bufView);
                sizes.meshes+=gltf.json.bufferViews[bufView].byteLength;
            }


            for(var k in gltf.json.meshes[i].primitives[j].attributes)
            {
                const attr=gltf.json.meshes[i].primitives[j].attributes[k];
                const bufView=gltf.json.accessors[attr].bufferView;

                if(sizeBufferViews.indexOf(bufView)==-1)
                {
                    sizeBufferViews.push(bufView);
                    sizes.meshes+=gltf.json.bufferViews[bufView].byteLength;
                }


            }

        }


    }
    html+='</table>';



    if(gltf.json.animations)
    {
        html+='<h3>Animations ('+gltf.json.animations.length+')</h3>';
        html+='<table class="table treetable">';
        html+='<tr>';
        html+='  <th>Name</th>';
        html+='  <th>Target node</th>';
        html+='  <th>Path</th>';
        html+='  <th>Interpolation</th>';
        html+='  <th>Keys</th>';
        html+='</tr>';

        sizes.animations=0;

        for(var i=0;i<gltf.json.animations.length;i++)
        {
            for(var j=0;j< gltf.json.animations[i].samplers.length;j++)
            {
                var bufView=gltf.json.accessors[gltf.json.animations[i].samplers[j].input].bufferView;
                if(sizeBufferViews.indexOf(bufView)==-1)
                {
                    sizeBufferViews.push(bufView);
                    sizes.animations+=gltf.json.bufferViews[bufView].byteLength;
                }

                var bufView=gltf.json.accessors[gltf.json.animations[i].samplers[j].output].bufferView;
                if(sizeBufferViews.indexOf(bufView)==-1)
                {
                    sizeBufferViews.push(bufView);
                    sizes.animations+=gltf.json.bufferViews[bufView].byteLength;
                }

            }




            for(var j=0;j< gltf.json.animations[i].channels.length;j++)
            {
                html+='<tr>';
                html+='  <td>'+gltf.json.animations[i].name+' ('+i+')</td>';

                html+='  <td>'+gltf.nodes[gltf.json.animations[i].channels[j].target.node].name+'</td>';
                html+='  <td>';
                html+=gltf.json.animations[i].channels[j].target.path+' ';
                html+='  </td>';

                const smplidx=gltf.json.animations[i].channels[j].sampler;
                const smplr=gltf.json.animations[i].samplers[smplidx];

                html+='  <td>'+smplr.interpolation+'</td>'


                html+='  <td>'+gltf.json.accessors[smplr.output].count;+'</td>'



                html+='</tr>';

            }
        }
        html+='</table>';

    }
    else
    {
        html+='<h3>Animations (0)</h3>';
    }

    if(gltf.json.images)
    {
        html+='<h3>Images ('+gltf.json.images.length+')</h3>';
        html+='<table class="table treetable">';

        html+='<tr>';
        html+='  <th>name</th>';
        html+='  <th>type</th>';
        html+='</tr>';

        sizes.images=0;

        for(var i=0;i<gltf.json.images.length;i++)
        {
            sizes.images+=gltf.json.bufferViews[gltf.json.images[i].bufferView].byteLength;

            html+='<tr>';
            html+='<td>'+gltf.json.images[i].name+'</td>';
            html+='<td>'+gltf.json.images[i].mimeType+'</td>';
            html+='<tr>';
        }
        html+='</table>';
    }


    // html+='data size: '+;
    const sizeBin=gltf.json.buffers[0].byteLength;
    html+='<h3>Binary Data ('+readableSize(sizeBin)+')</h3>';


    html+='<table class="table treetable">';
    html+='<tr>';
    html+='  <th>name</th>';
    html+='  <th>size</th>';
    html+='  <th>%</th>';
    html+='</tr>';
    var sizeUnknown=sizeBin;
    for(var i in sizes)
    {
        // html+=i+':'+Math.round(sizes[i]/1024);
        html+='<tr>';
        html+='<td>'+i+'</td>';
        html+='<td>'+readableSize(sizes[i])+' </td>';
        html+='<td>'+Math.round(sizes[i]/sizeBin*100)+'% </td>';
        html+='<tr>';
        sizeUnknown-=sizes[i];
    }

    if(sizeUnknown!=0)
    {
        html+='<tr>';
        html+='<td>unknown</td>';
        html+='<td>'+readableSize(sizeUnknown)+' </td>';
        html+='<td>'+Math.round(sizeUnknown/sizeBin*100)+'% </td>';
        html+='<tr>';
    }

    html+='</table>';
    html+='</div>';

    // CABLES.UI.MODAL.show(html);

    // closeTab();
    tab=new CABLES.UI.Tab("GLTF",{"icon":"cube","infotext":"tab_gltf","padding":true,"singleton":true});
    gui.mainTabs.addTab(tab,true);
    tab.html(html);

    console.log(gltf);
}

function readableSize(n)
{
    if(n>1024)return Math.round(n/1024)+' kb';
    if(n>1024*500)return Math.round(n/1024)+' mb';
    else return n+' bytes';

}


// https://raw.githubusercontent.com/KhronosGroup/glTF/master/specification/2.0/figures/gltfOverview-2.0.0b.png

const
    dataPort=op.inString("data"),
    inExec=op.inTrigger("Render"),
    inFile=op.inUrl("glb File"),
    inRender=op.inBool("Draw",true),
    inCenter=op.inSwitch("Center",["None","XYZ","XZ"],"XYZ"),
    inRescale=op.inBool("Rescale",true),
    inRescaleSize=op.inFloat("Rescale Size",2.5),
    inShow=op.inTriggerButton("Show Structure"),
    inTime=op.inFloat("Time"),
    inTimeLine=op.inBool("Sync to timeline",false),
    inLoop=op.inBool("Loop",true),

    inSwitchNormalsYZ=op.inBool("Switch Normals",false),

    inMaterials=op.inObject("Materials"),


    nextBefore=op.outTrigger("Render Before"),
    next=op.outTrigger("Next"),
    outGenerator=op.outString("Generator"),
    outVersion=op.outNumber("GLTF Version"),
    outAnimLength=op.outNumber("Anim Length",0),
    outAnimTime=op.outNumber("Anim Time",0),
    outJson=op.outObject("Json"),
    outPoints=op.outArray("BoundingPoints"),
    outBounds=op.outObject("Bounds"),
    outAnimFinished=op.outTrigger("Finished");

op.setPortGroup("Timing",[inTime,inTimeLine,inLoop]);

const le=true; //little endian
const cgl=op.patch.cgl;
inFile.onChange=reloadSoon;

var boundingPoints=[];
var gltf=null;
var maxTime=0;
var time=0;
var needsMatUpdate=true;
var timedLoader=null;
var loadingId=null;
var data=null;
var scale=vec3.create();
var lastTime=0;
var doCenter=false;

const boundsCenter=vec3.create();

inShow.onTriggered=printInfo;
dataPort.setUiAttribs({"hideParam":true,"hidePort":true});
dataPort.onChange=loadData;

op.setPortGroup("Transform",[inRescale,inRescaleSize,inCenter]);

inCenter.onChange=function()
{
    doCenter=inCenter.get()!="None";
    updateCenter();

};

function updateCenter()
{

    if(gltf && gltf.bounds)
    {
        boundsCenter.set(gltf.bounds.center);
        boundsCenter[0]=-boundsCenter[0];
        boundsCenter[1]=-boundsCenter[1];
        boundsCenter[2]=-boundsCenter[2];

        if(inCenter.get()=="XZ")
            boundsCenter[1]=-gltf.bounds.minY;
    }
}


inSwitchNormalsYZ.onChange=function()
{
    reloadSoon();
};

inRescale.onChange=function()
{
    inRescaleSize.setUiAttribs({greyout:!inRescale.get()});
};

inMaterials.onChange=function()
{
    needsMatUpdate=true;
};

op.onDelete=function()
{
    closeTab();
};

inTimeLine.onChange=function()
{
    inTime.setUiAttribs({greyout:inTimeLine.get()});
};


inExec.onTriggered=function()
{
    if(inTimeLine.get()) time=op.patch.timer.getTime();
    else time=Math.max(0,inTime.get());

    if(inLoop.get())
    {
        time = time % maxTime;
        if(time<lastTime)outAnimFinished.trigger();
    }
    else
    {
        if(maxTime >0 && time>=maxTime)outAnimFinished.trigger();
    }
    lastTime=time;

    cgl.pushModelMatrix();

    outAnimTime.set(time);

    if(gltf && gltf.bounds)
    {
        if(inRescale.get())
        {
            const sc=inRescaleSize.get()/gltf.bounds.maxAxis;
            vec3.set(scale,sc,sc,sc);
            mat4.scale(cgl.mMatrix,cgl.mMatrix,scale);
        }
        if(doCenter)
        {
            mat4.translate(cgl.mMatrix,cgl.mMatrix,boundsCenter);
        }
    }

    cgl.frameStore.currentScene=gltf;
    nextBefore.trigger();

    if(needsMatUpdate) updateMaterials();

    if(gltf && inRender.get())
    {
        gltf.time=time;

        if(gltf.bounds && CABLES.UI && (CABLES.UI.renderHelper || gui.patch().isCurrentOp(op)))
        {
            if(CABLES.UI.renderHelper)cgl.pushShader(CABLES.GL_MARKER.getDefaultShader(cgl));
            else cgl.pushShader(CABLES.GL_MARKER.getSelectedShader(cgl));
            gltf.bounds.render(cgl);
            cgl.popShader();
        }

        for(var i=0;i<gltf.nodes.length;i++)
            if(!gltf.nodes[i].isChild) gltf.nodes[i].render(cgl);
    }

    next.trigger();
    cgl.frameStore.currentScene=null;

    cgl.popModelMatrix();
};

function loadBin(addCacheBuster)
{
    if(!loadingId)loadingId=cgl.patch.loading.start('gltf',inFile.get());

    var url = op.patch.getFilePath(String(inFile.get()));
    if(addCacheBuster)url+='?rnd='+CABLES.generateUUID();

    var oReq = new XMLHttpRequest();
    oReq.open("GET", url, true);
    oReq.responseType = "arraybuffer";
    closeTab();

    oReq.onload = function (oEvent)
    {
        boundingPoints=[];

        maxTime=0;
        var arrayBuffer = oReq.response;
        gltf=parseGltf(arrayBuffer);
        cgl.patch.loading.finished(loadingId);
        needsMatUpdate=true;
        op.refreshParams();
        outAnimLength.set(maxTime);
        hideNodesFromData();
        if(tab)printInfo();


        outPoints.set(boundingPoints);
        if(gltf && gltf.bounds)outBounds.set(gltf.bounds);
        updateCenter();
    };

    oReq.send(null);
}

op.onFileChanged=function(fn)
{

    if(fn && fn.length>3 && inFile.get() && inFile.get().indexOf(fn)>-1) reloadSoon(true);

};

op.onFileChanged=function(fn)
{
    if(inFile.get() && inFile.get().indexOf(fn)>-1)
    {
        reloadSoon(true);
    }
};


function reloadSoon(nocache)
{

    clearTimeout(timedLoader);
    timedLoader=setTimeout(function()
    {
        loadBin(nocache);
    },30);
}


function updateMaterials()
{
    if(!gltf) return;

    gltf.shaders={};

    for(var j=0;j<inMaterials.links.length;j++)
    {
        const op=inMaterials.links[j].portOut.parent;
        const portShader=op.getPort("Shader");
        const portName=op.getPort("Material Name");

        if(portShader && portName && portShader.get())
        {
            const name=portName.get();
            if(gltf.json.materials)
                for(var i=0;i<gltf.json.materials.length;i++)
                    if(gltf.json.materials[i].name==name)
                    {
                        if(gltf.shaders[i])
                        {
                            op.warn("double material assignment:",name);
                        }
                        gltf.shaders[i]=portShader.get();
                    }
        }
    }
    needsMatUpdate=false;
    if(tab)printInfo();

}

function hideNodesFromData()
{
    if(!data)loadData();

    if(gltf && data && data.hiddenNodes)
    {
        for(var i in data.hiddenNodes)
        {
            const n=gltf.getNode(i);
            if(n) n.hidden=true;
            else op.warn("node to be hidden not found",i,n);
        }
    }
}


function loadData()
{
    data=dataPort.get()

    if(!data || data==="")data={};
    else data=JSON.parse(data);

    if(gltf)hideNodesFromData();

    return data;
}

function saveData()
{
    dataPort.set(JSON.stringify(data));
}


op.exposeNode=function(name)
{
    var newop=gui.patch().scene.addOp("Ops.Gl.GLTF.GltfNode");
    newop.getPort("Node Name").set(name);
    op.patch.link(op,next.name,newop,"Render");
    gui.patch().focusOp(newop.id,true);
    CABLES.UI.MODAL.hide();
};

op.assignMaterial=function(name)
{
    var newop=gui.patch().scene.addOp("Ops.Gl.GLTF.GltfSetMaterial");
    newop.getPort("Material Name").set(name);
    op.patch.link(op,inMaterials.name,newop,"Material");
    gui.patch().focusOp(newop.id,true);
    CABLES.UI.MODAL.hide();
};

op.toggleNodeVisibility=function(name)
{
    const n=gltf.getNode(name);

    n.hidden=!n.hidden;

    data.hiddenNodes=data.hiddenNodes||{};

    if(n)
        if(n.hidden)data.hiddenNodes[name]=true;
        else delete data.hiddenNodes[name];

    saveData();
}





};

Ops.Gl.GLTF.GltfScene_v2.prototype = new CABLES.Op();
CABLES.OPS["daafedce-998a-4b34-8af3-6b62a66948ed"]={f:Ops.Gl.GLTF.GltfScene_v2,objName:"Ops.Gl.GLTF.GltfScene_v2"};




// **************************************************************
// 
// Ops.Gl.GLTF.GltfSetMaterial
// 
// **************************************************************

Ops.Gl.GLTF.GltfSetMaterial = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    inShader=op.inObject("Shader"),
    inName=op.inString("Material Name","none"),
    outMat=op.outObject("Material");

inName.onChange=
inShader.onChange=function()
{
    op.setTitle("Material "+inName.get());
    outMat.set(null);
    outMat.set(inShader.get());
};

};

Ops.Gl.GLTF.GltfSetMaterial.prototype = new CABLES.Op();
CABLES.OPS["baf968ea-e4df-4fca-9cda-e6ddd38a4200"]={f:Ops.Gl.GLTF.GltfSetMaterial,objName:"Ops.Gl.GLTF.GltfSetMaterial"};




// **************************************************************
// 
// Ops.Gl.CubeMap.CubeMapFromTextures_v2
// 
// **************************************************************

Ops.Gl.CubeMap.CubeMapFromTextures_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
let loadingId=0;
let skyboxCubemap=null;
const gl=op.patch.cgl.gl;
const cgl=op.patch.cgl;


const inFilenames=[];

const titles = [
"posx", "negx",
"posy", "negy",
"posz", "negz"
];

titles.forEach(function(title) { // create inlet ports
   const inFilename = op.inUrl(title, "image");
   inFilename.onChange = loadImagesLater; // assign on change handlers
   inFilenames.push(inFilename);
});

const inFlipY = op.inBool("Flip Y", false);
var outTex=op.outObject("cubemap");
inFlipY.onChange = loadImagesLater;

var timeoutLater=null;

function loadImagesLater()
{
    clearTimeout(timeoutLater);
    timeoutLater=setTimeout(loadImages,100);

}

function loadImage(src) {
    return new Promise((resolve, reject) => {
        const image = new Image();
        image.crossOrigin = '';
        image.addEventListener("load", () => resolve(image));
        image.addEventListener("error", (err) => reject(err));
        image.src = src;
    });
}

function loadImages() {
    op.setUiError("loadingerror", null);
    op.setUiError("loading", "Loading images..." , 0);

    skyboxCubemap = null;
    loadingId=cgl.patch.loading.start('cubemap texture','');

    const images = Promise.all(
        inFilenames.map(inFile => inFile.get()) // get file address
        .filter(Boolean) // remove all 0's (empty file adresses) so we only resolve
        .map(filename => loadImage(filename))) // map to resolver function
        .then((images) => { // wait for all images to be loaded and only then continue
            if (images.length === 6) {
                cgl.gl.pixelStorei(
                    cgl.gl.UNPACK_FLIP_Y_WEBGL,
                    inFlipY.get()
                );
                skyboxCubemap = cgl.gl.createTexture(cgl.gl.TEXTURE_CUBE_MAP);
                cgl.gl.bindTexture(cgl.gl.TEXTURE_CUBE_MAP, skyboxCubemap);

                cgl.gl.texParameteri(cgl.gl.TEXTURE_CUBE_MAP, cgl.gl.TEXTURE_WRAP_S, cgl.gl.CLAMP_TO_EDGE);
                cgl.gl.texParameteri(cgl.gl.TEXTURE_CUBE_MAP, cgl.gl.TEXTURE_WRAP_T, cgl.gl.CLAMP_TO_EDGE);
                cgl.gl.texParameteri(cgl.gl.TEXTURE_CUBE_MAP, cgl.gl.TEXTURE_MIN_FILTER, cgl.gl.LINEAR_MIPMAP_LINEAR);
                cgl.gl.texParameteri(cgl.gl.TEXTURE_CUBE_MAP, cgl.gl.TEXTURE_MAG_FILTER, cgl.gl.LINEAR);

                if (inFlipY.get()) {
                    const temp = images[2];
                    images[2] = images[3];
                    images[3] = temp;
                }

                images.forEach((img, index) => {
                    cgl.gl.bindTexture(
                        cgl.gl.TEXTURE_CUBE_MAP,
                        skyboxCubemap
                    );

                    cgl.gl.texImage2D(
                        cgl.gl.TEXTURE_CUBE_MAP_POSITIVE_X + index,
                        0,
                        cgl.gl.RGBA,
                        cgl.gl.RGBA,
                        cgl.gl.UNSIGNED_BYTE,
                        img
                    );
                });

            cgl.gl.generateMipmap(cgl.gl.TEXTURE_CUBE_MAP);

            outTex.set({ "cubemap": skyboxCubemap });

            cgl.gl.bindTexture(cgl.gl.TEXTURE_CUBE_MAP, null);
            cgl.patch.loading.finished(loadingId);

            op.setUiError("loading", null);
        }
    })
    .catch(err => {
        op.error("error", err);
        op.setUiError("loadingerror", "Could not load textures!" , 2);
    });
}

};

Ops.Gl.CubeMap.CubeMapFromTextures_v2.prototype = new CABLES.Op();
CABLES.OPS["44e837c5-ecd8-42cf-9be6-4db2283b9cbf"]={f:Ops.Gl.CubeMap.CubeMapFromTextures_v2,objName:"Ops.Gl.CubeMap.CubeMapFromTextures_v2"};




// **************************************************************
// 
// Ops.Gl.Matrix.TransformView
// 
// **************************************************************

Ops.Gl.Matrix.TransformView = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    render=op.inTrigger('render'),
    posX=op.inValueFloat("posX"),
    posY=op.inValueFloat("posY"),
    posZ=op.inValueFloat("posZ"),
    scale=op.inValueFloat("scale"),
    rotX=op.inValueFloat("rotX"),
    rotY=op.inValueFloat("rotY"),
    rotZ=op.inValueFloat("rotZ"),
    trigger=op.outTrigger('trigger');

op.setPortGroup("Position",[posX,posY,posZ]);
op.setPortGroup("Scale",[scale]);
op.setPortGroup("Rotation",[rotX,rotZ,rotY]);

var cgl=op.patch.cgl;
var vPos=vec3.create();
var vScale=vec3.create();
var transMatrix = mat4.create();
mat4.identity(transMatrix);

var doScale=false;
var doTranslate=false;

var translationChanged=true;
var scaleChanged=true;
var rotChanged=true;

render.onTriggered=function()
{
    var updateMatrix=false;
    if(translationChanged)
    {
        updateTranslation();
        updateMatrix=true;
    }
    if(scaleChanged)
    {
        updateScale();
        updateMatrix=true;
    }
    if(rotChanged)
    {
        updateMatrix=true;
    }
    if(updateMatrix)doUpdateMatrix();

    cgl.pushViewMatrix();
    mat4.multiply(cgl.vMatrix,cgl.vMatrix,transMatrix);

    trigger.trigger();
    cgl.popViewMatrix();

    if(CABLES.UI && gui.patch().isCurrentOp(op))
        gui.setTransformGizmo(
            {
                posX:posX,
                posY:posY,
                posZ:posZ,
            });
};

op.transform3d=function()
{
    return {
            pos:[posX,posY,posZ]
        };

};

var doUpdateMatrix=function()
{
    mat4.identity(transMatrix);
    if(doTranslate)mat4.translate(transMatrix,transMatrix, vPos);

    if(rotX.get()!==0)mat4.rotateX(transMatrix,transMatrix, rotX.get()*CGL.DEG2RAD);
    if(rotY.get()!==0)mat4.rotateY(transMatrix,transMatrix, rotY.get()*CGL.DEG2RAD);
    if(rotZ.get()!==0)mat4.rotateZ(transMatrix,transMatrix, rotZ.get()*CGL.DEG2RAD);

    if(doScale)mat4.scale(transMatrix,transMatrix, vScale);
    rotChanged=false;
};

function updateTranslation()
{
    doTranslate=false;
    if(posX.get()!==0.0 || posY.get()!==0.0 || posZ.get()!==0.0) doTranslate=true;
    vec3.set(vPos, posX.get(),posY.get(),posZ.get());
    translationChanged=false;
}

function updateScale()
{
    doScale=false;
    if(scale.get()!==0.0)doScale=true;
    vec3.set(vScale, scale.get(),scale.get(),scale.get());
    scaleChanged=false;
}

var translateChanged=function()
{
    translationChanged=true;
};

var scaleChanged=function()
{
    scaleChanged=true;
};

var rotChanged=function()
{
    rotChanged=true;
};


rotX.onChange=rotChanged;
rotY.onChange=rotChanged;
rotZ.onChange=rotChanged;

scale.onChange=scaleChanged;

posX.onChange=translateChanged;
posY.onChange=translateChanged;
posZ.onChange=translateChanged;

rotX.set(0.0);
rotY.set(0.0);
rotZ.set(0.0);

scale.set(1.0);

posX.set(0.0);
posY.set(0.0);
posZ.set(0.0);

doUpdateMatrix();



};

Ops.Gl.Matrix.TransformView.prototype = new CABLES.Op();
CABLES.OPS["0b3e04f7-323e-4ac8-8a22-a21e2f36e0e9"]={f:Ops.Gl.Matrix.TransformView,objName:"Ops.Gl.Matrix.TransformView"};




// **************************************************************
// 
// Ops.Gl.Shader.BasicMaterial_v3
// 
// **************************************************************

Ops.Gl.Shader.BasicMaterial_v3 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={basicmaterial_frag:"{{MODULES_HEAD}}\n\nIN vec2 texCoord;\nUNI vec4 color;\n// UNI float r;\n// UNI float g;\n// UNI float b;\n// UNI float a;\n\n#ifdef HAS_TEXTURES\n    IN vec2 texCoordOrig;\n    #ifdef HAS_TEXTURE_DIFFUSE\n        UNI sampler2D tex;\n    #endif\n    #ifdef HAS_TEXTURE_OPACITY\n        UNI sampler2D texOpacity;\n   #endif\n#endif\n\nvoid main()\n{\n    {{MODULE_BEGIN_FRAG}}\n    vec4 col=color;\n\n    #ifdef HAS_TEXTURES\n        vec2 uv=texCoord;\n\n        #ifdef HAS_TEXTURE_DIFFUSE\n            col=texture(tex,uv);\n\n            #ifdef COLORIZE_TEXTURE\n                col.r*=color.r;\n                col.g*=color.g;\n                col.b*=color.b;\n            #endif\n        #endif\n        col.a*=color.a;\n        #ifdef HAS_TEXTURE_OPACITY\n            #ifdef TRANSFORMALPHATEXCOORDS\n                uv=texCoordOrig;\n            #endif\n            #ifdef ALPHA_MASK_ALPHA\n                col.a*=texture(texOpacity,uv).a;\n            #endif\n            #ifdef ALPHA_MASK_LUMI\n                col.a*=dot(vec3(0.2126,0.7152,0.0722), texture(texOpacity,uv).rgb);\n            #endif\n            #ifdef ALPHA_MASK_R\n                col.a*=texture(texOpacity,uv).r;\n            #endif\n            #ifdef ALPHA_MASK_G\n                col.a*=texture(texOpacity,uv).g;\n            #endif\n            #ifdef ALPHA_MASK_B\n                col.a*=texture(texOpacity,uv).b;\n            #endif\n            // #endif\n        #endif\n    #endif\n\n    {{MODULE_COLOR}}\n\n    #ifdef DISCARDTRANS\n        if(col.a<0.2) discard;\n    #endif\n\n    outColor = col;\n}\n",basicmaterial_vert:"IN vec3 vPosition;\nIN vec2 attrTexCoord;\nIN vec3 attrVertNormal;\nIN float attrVertIndex;\n\n{{MODULES_HEAD}}\n\nOUT vec3 norm;\nOUT vec2 texCoord;\nOUT vec2 texCoordOrig;\n\nUNI mat4 projMatrix;\nUNI mat4 modelMatrix;\nUNI mat4 viewMatrix;\n\n#ifdef HAS_TEXTURES\n    UNI float diffuseRepeatX;\n    UNI float diffuseRepeatY;\n    UNI float texOffsetX;\n    UNI float texOffsetY;\n#endif\n\nvoid main()\n{\n    mat4 mMatrix=modelMatrix;\n    mat4 mvMatrix;\n\n    norm=attrVertNormal;\n    texCoordOrig=attrTexCoord;\n    texCoord=attrTexCoord;\n    #ifdef HAS_TEXTURES\n        texCoord.x=texCoord.x*diffuseRepeatX+texOffsetX;\n        texCoord.y=(1.0-texCoord.y)*diffuseRepeatY+texOffsetY;\n    #endif\n\n    vec4 pos = vec4(vPosition, 1.0);\n\n    #ifdef BILLBOARD\n       vec3 position=vPosition;\n       mvMatrix=viewMatrix*modelMatrix;\n\n       gl_Position = projMatrix * mvMatrix * vec4((\n           position.x * vec3(\n               mvMatrix[0][0],\n               mvMatrix[1][0],\n               mvMatrix[2][0] ) +\n           position.y * vec3(\n               mvMatrix[0][1],\n               mvMatrix[1][1],\n               mvMatrix[2][1]) ), 1.0);\n    #endif\n\n    {{MODULE_VERTEX_POSITION}}\n\n    #ifndef BILLBOARD\n        mvMatrix=viewMatrix * mMatrix;\n    #endif\n\n\n    #ifndef BILLBOARD\n        // gl_Position = projMatrix * viewMatrix * modelMatrix * pos;\n        gl_Position = projMatrix * mvMatrix * pos;\n    #endif\n}\n",};
const render=op.inTrigger("render");


const trigger=op.outTrigger('trigger');
const shaderOut=op.outObject("shader");

shaderOut.ignoreValueSerialize=true;

const cgl=op.patch.cgl;

op.toWorkPortsNeedToBeLinked(render);

const shader=new CGL.Shader(cgl,"basicmaterialnew");
shader.setModules(['MODULE_VERTEX_POSITION','MODULE_COLOR','MODULE_BEGIN_FRAG']);
shader.setSource(attachments.basicmaterial_vert,attachments.basicmaterial_frag);
shaderOut.set(shader);

render.onTriggered=doRender;


// rgba colors
const r=op.inValueSlider("r",Math.random());
const g=op.inValueSlider("g",Math.random());
const b=op.inValueSlider("b",Math.random());
const a=op.inValueSlider("a",1);
r.setUiAttribs({"colorPick":true});

const uniColor=new CGL.Uniform(shader,'4f','color',r,g,b,a);

// diffuse outTexture

const diffuseTexture=op.inTexture("texture");
var diffuseTextureUniform=null;
diffuseTexture.onChange=updateDiffuseTexture;

const colorizeTexture=op.inValueBool("colorizeTexture",false);

// opacity texture
const textureOpacity=op.inTexture("textureOpacity");
var textureOpacityUniform=null;

const alphaMaskSource=op.inSwitch("Alpha Mask Source",["Luminance","R","G","B","A"],"Luminance");
alphaMaskSource.setUiAttribs({greyout:true});
textureOpacity.onChange=updateOpacity;

const texCoordAlpha=op.inValueBool("Opacity TexCoords Transform",false);
const discardTransPxl=op.inValueBool("Discard Transparent Pixels");


// texture coords

const diffuseRepeatX=op.inValue("diffuseRepeatX",1);
const diffuseRepeatY=op.inValue("diffuseRepeatY",1);
const diffuseOffsetX=op.inValue("Tex Offset X",0);
const diffuseOffsetY=op.inValue("Tex Offset Y",0);

const diffuseRepeatXUniform=new CGL.Uniform(shader,'f','diffuseRepeatX',diffuseRepeatX);
const diffuseRepeatYUniform=new CGL.Uniform(shader,'f','diffuseRepeatY',diffuseRepeatY);
const diffuseOffsetXUniform=new CGL.Uniform(shader,'f','texOffsetX',diffuseOffsetX);
const diffuseOffsetYUniform=new CGL.Uniform(shader,'f','texOffsetY',diffuseOffsetY);

const doBillboard=op.inValueBool("billboard",false);

alphaMaskSource.onChange=
    doBillboard.onChange=
    discardTransPxl.onChange=
    texCoordAlpha.onChange=
    colorizeTexture.onChange=updateDefines;


op.setPortGroup("Color",[r,g,b,a]);
op.setPortGroup("Color Texture",[diffuseTexture,colorizeTexture]);
op.setPortGroup("Opacity",[textureOpacity,alphaMaskSource,discardTransPxl,texCoordAlpha]);
op.setPortGroup("Texture Transform",[diffuseRepeatX,diffuseRepeatY,diffuseOffsetX,diffuseOffsetY]);


updateOpacity();
updateDiffuseTexture();



op.preRender=function()
{
    shader.bind();
    doRender();
};

function doRender()
{
    if(!shader)return;

    cgl.pushShader(shader);
    shader.popTextures();

    if(diffuseTextureUniform && diffuseTexture.get()) shader.pushTexture(diffuseTextureUniform,diffuseTexture.get().tex);
    if(textureOpacityUniform && textureOpacity.get()) shader.pushTexture(textureOpacityUniform,textureOpacity.get().tex);
    trigger.trigger();

    cgl.popShader();
}

function updateOpacity()
{
    if(textureOpacity.get())
    {
        if(textureOpacityUniform!==null)return;
        shader.removeUniform('texOpacity');
        shader.define('HAS_TEXTURE_OPACITY');
        if(!textureOpacityUniform)textureOpacityUniform=new CGL.Uniform(shader,'t','texOpacity',1);

        alphaMaskSource.setUiAttribs({greyout:false});
        discardTransPxl.setUiAttribs({greyout:false});
        texCoordAlpha.setUiAttribs({greyout:false});
    }
    else
    {
        shader.removeUniform('texOpacity');
        shader.removeDefine('HAS_TEXTURE_OPACITY');
        textureOpacityUniform=null;

        alphaMaskSource.setUiAttribs({greyout:true});
        discardTransPxl.setUiAttribs({greyout:true});
        texCoordAlpha.setUiAttribs({greyout:true});
    }

    updateDefines();
}

function updateDiffuseTexture()
{
    if(diffuseTexture.get())
    {
        if(!shader.hasDefine('HAS_TEXTURE_DIFFUSE'))shader.define('HAS_TEXTURE_DIFFUSE');
        if(!diffuseTextureUniform)diffuseTextureUniform=new CGL.Uniform(shader,'t','texDiffuse',0);

        diffuseRepeatX.setUiAttribs({greyout:false});
        diffuseRepeatY.setUiAttribs({greyout:false});
        diffuseOffsetX.setUiAttribs({greyout:false});
        diffuseOffsetY.setUiAttribs({greyout:false});
        colorizeTexture.setUiAttribs({greyout:false});
    }
    else
    {
        shader.removeUniform('texDiffuse');
        shader.removeDefine('HAS_TEXTURE_DIFFUSE');
        diffuseTextureUniform=null;

        diffuseRepeatX.setUiAttribs({greyout:true});
        diffuseRepeatY.setUiAttribs({greyout:true});
        diffuseOffsetX.setUiAttribs({greyout:true});
        diffuseOffsetY.setUiAttribs({greyout:true});
        colorizeTexture.setUiAttribs({greyout:true});
    }
}

function updateDefines()
{
    shader.toggleDefine('COLORIZE_TEXTURE', colorizeTexture.get());
    shader.toggleDefine('TRANSFORMALPHATEXCOORDS', texCoordAlpha.get());
    shader.toggleDefine('DISCARDTRANS', discardTransPxl.get());
    shader.toggleDefine('BILLBOARD', doBillboard.get());

    shader.toggleDefine('ALPHA_MASK_ALPHA', alphaMaskSource.get()=='Alpha Channel');
    shader.toggleDefine('ALPHA_MASK_LUMI', alphaMaskSource.get()=='Luminance');
    shader.toggleDefine('ALPHA_MASK_R', alphaMaskSource.get()=='R');
    shader.toggleDefine("ALPHA_MASK_G", alphaMaskSource.get()=='G');
    shader.toggleDefine('ALPHA_MASK_B', alphaMaskSource.get()=='B');
}




};

Ops.Gl.Shader.BasicMaterial_v3.prototype = new CABLES.Op();
CABLES.OPS["ec55d252-3843-41b1-b731-0482dbd9e72b"]={f:Ops.Gl.Shader.BasicMaterial_v3,objName:"Ops.Gl.Shader.BasicMaterial_v3"};




// **************************************************************
// 
// Ops.Patch.LoadingStatus
// 
// **************************************************************

Ops.Patch.LoadingStatus = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
var self=this;
const cgl=op.patch.cgl;
const patch=op.patch;

this.exe=this.addInPort(new CABLES.Port(op,"exe",CABLES.OP_PORT_TYPE_FUNCTION));
this.finished=this.addOutPort(new CABLES.Port(op,"finished",CABLES.OP_PORT_TYPE_FUNCTION));
var result=this.addOutPort(new CABLES.Port(op,"status",CABLES.OP_PORT_TYPE_VALUE));
var isFinishedPort = op.outValue('all loaded', false);
var preRenderStatus=this.addOutPort(new CABLES.Port(op,"preRenderStatus",CABLES.OP_PORT_TYPE_VALUE));
var preRenderTimeFrames=this.addInPort(new CABLES.Port(op,"preRenderTimes",CABLES.OP_PORT_TYPE_VALUE));
var preRenderOps=op.inValueBool("PreRender Ops");
var startTimeLine=op.inBool("Play Timeline",true);
preRenderStatus.set(0);
this.numAssets=this.addOutPort(new CABLES.Port(op,"numAssets",CABLES.OP_PORT_TYPE_VALUE));
this.loading=this.addOutPort(new CABLES.Port(op,"loading",CABLES.OP_PORT_TYPE_FUNCTION));
var loadingFinished=op.outTrigger("loading finished");//this.addOutPort(new CABLES.Port(op,"loading finished",CABLES.OP_PORT_TYPE_FUNCTION));

var finishedAll=false;
var preRenderTimes=[];
var firstTime=true;

var identTranslate=vec3.create();
vec3.set(identTranslate, 0,0,0);
var identTranslateView=vec3.create();
vec3.set(identTranslateView, 0,0,-2);

document.body.classList.add("cables-loading");


var prerenderCount=0;
var preRenderAnimFrame=function(t)
{
    var time=preRenderTimes[prerenderCount];

    preRenderStatus.set(prerenderCount/(preRenderTimeFrames.anim.keys.length-1));

    op.patch.timer.setTime(time);
    cgl.renderStart(cgl,identTranslate,identTranslateView);

    self.finished.trigger();

    cgl.gl.clearColor(0,0,0,1);
    cgl.gl.clear(cgl.gl.COLOR_BUFFER_BIT | cgl.gl.DEPTH_BUFFER_BIT);

    self.loading.trigger();

    cgl.renderEnd(cgl);
    prerenderCount++;
};

this.onAnimFrame=null;
var loadingIdPrerender='';

this.onLoaded=function()
{
    if(preRenderTimeFrames.isAnimated())
    if(preRenderTimeFrames.anim)
        for(var i=0;i<preRenderTimeFrames.anim.keys.length;i++)
            preRenderTimes.push( preRenderTimeFrames.anim.keys[i].time );

    preRenderTimes.push(1);
};

function checkPreRender()
{
    if(patch.loading.getProgress()>=1.0)
    {
        if(preRenderTimeFrames.anim && prerenderCount>=preRenderTimeFrames.anim.keys.length)
        {
            self.onAnimFrame=function(){};
            isFinishedPort.set(true);
            finishedAll=true;
        }
        else
        {
            setTimeout(checkPreRender,30);
        }
    }
    else
    {
        setTimeout(checkPreRender,100);
    }
}

var loadingId=patch.loading.start('delayloading','delayloading');
setTimeout(function()
{
    patch.loading.finished(loadingId);
},100);

this.exe.onTriggered= function()
{
    result.set(patch.loading.getProgress());
    self.numAssets.set(patch.loading.getNumAssets());

    if(patch.loading.getProgress()>=1.0 && finishedAll)
    {
        if(firstTime)
        {
            if(preRenderOps.get()) op.patch.preRenderOps();
            loadingFinished.trigger();
            op.patch.timer.setTime(0);
            if(startTimeLine.get())
            {
                op.patch.timer.play();
                isFinishedPort.set(true);
            }
            firstTime=false;
        }

        self.finished.trigger();
        document.body.classList.remove("cables-loading");
        document.body.classList.add("cables-loaded");
    }
    else
    {
        if(!preRenderTimeFrames.anim)
        {
            finishedAll=true;
            self.onAnimFrame=function(){};
        }

        if(preRenderTimeFrames.anim && patch.loading.getProgress()>=1.0
            && prerenderCount<preRenderTimeFrames.anim.keys.length
        )
        {
            self.onAnimFrame=preRenderAnimFrame;
            checkPreRender();
            self.loading.trigger();
        }

        if(patch.loading.getProgress()<1.0)
        {
            self.loading.trigger();
            op.patch.timer.setTime(0);
            op.patch.timer.pause();
        }
    }

};

};

Ops.Patch.LoadingStatus.prototype = new CABLES.Op();
CABLES.OPS["30f01b6d-b234-4ebe-a2c3-ebffd96a31e9"]={f:Ops.Patch.LoadingStatus,objName:"Ops.Patch.LoadingStatus"};




// **************************************************************
// 
// Ops.Gl.Meshes.Sphere_v2
// 
// **************************************************************

Ops.Gl.Meshes.Sphere_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    TAU = Math.PI * 2,
    cgl = op.patch.cgl,
    inTrigger = op.inTrigger("render"),
    inRadius = op.inValue("radius", 1),
    inStacks = op.inValue("stacks", 32),
    inSlices = op.inValue("slices", 32),
    inStacklimit = op.inValueSlider("Filloffset", 1),
    inDraw = op.inValueBool("Render", true),
    outTrigger = op.outTrigger("trigger"),
    outGeometry = op.outObject("geometry"),
    UP = vec3.fromValues(0,1,0),
    RIGHT = vec3.fromValues(1,0,0)
;

var
    geom = new CGL.Geometry("Sphere"),
    tmpNormal = vec3.create(),
    tmpVec = vec3.create(),
    needsRebuild = true,
    mesh
;

function buildMesh () {
    const
        stacks = Math.max(inStacks.get(),2),
        slices = Math.max(inSlices.get(),3),
        stackLimit = Math.min(Math.max(inStacklimit.get()*stacks,1),stacks),
        radius = inRadius.get()
    ;

    var
        positions = [],
        texcoords = [],
        normals = [],
        tangents = [],
        biTangents = [],
        indices = [],
        x,y,z,d,t,a,
        o,u,v,i,j
    ;

    for (i = o = 0; i < stacks + 1; i++) {
        v = (i/stacks-.5)*Math.PI;
        y = Math.sin(v);
        a = Math.cos(v);
        // for (j = 0; j < slices+1; j++) {
        for (j = slices; j >=0; j--) {
            u = (j/slices)*TAU;
            x = Math.cos(u)*a;
            z = Math.sin(u)*a;

            positions.push(x*radius,y*radius,z*radius);
            // texcoords.push(i/(stacks+1),j/slices);
            texcoords.push(j/slices,i/(stacks+1));

            d = Math.sqrt(x*x+y*y+z*z);
            normals.push(
                tmpNormal[0] = x/d,
                tmpNormal[1] = y/d,
                tmpNormal[2] = z/d
            );

            if (y == d) t = RIGHT;
            else t = UP;
            vec3.cross(tmpVec, tmpNormal, t);
            vec3.normalize(tmpVec,tmpVec);
            Array.prototype.push.apply(tangents, tmpVec);
            vec3.cross(tmpVec, tmpVec, tmpNormal);
            Array.prototype.push.apply(biTangents, tmpVec);
        }
        if (i == 0 || i > stackLimit) continue;
        for(j = 0; j < slices; j++,o++) {
            indices.push(
                o,o+1,o+slices+1,
                o+1,o+slices+2,o+slices+1
            );
        }
        o++;
    }

    // set geometry
    geom.clear();
    geom.vertices = positions;
    geom.texCoords = texcoords;
    geom.vertexNormals = normals;
    geom.tangents = tangents;
    geom.biTangents = biTangents;
    geom.verticesIndices = indices;

    outGeometry.set(null);
    outGeometry.set(geom);

    if (!mesh) mesh = new CGL.Mesh(cgl, geom);
    else mesh.setGeom(geom);

    needsRebuild = false;
}

// set event handlers
inTrigger.onTriggered = function () {
    if (needsRebuild) buildMesh();
    if (inDraw.get()) mesh.render(cgl.getShader());
    outTrigger.trigger();
};

inStacks.onChange =
inSlices.onChange =
inStacklimit.onChange =
inRadius.onChange = function() {
    // only calculate once, even after multiple settings could were changed
    needsRebuild = true;
};

// set lifecycle handlers
op.onDelete = function () { if(mesh)mesh.dispose(); };


};

Ops.Gl.Meshes.Sphere_v2.prototype = new CABLES.Op();
CABLES.OPS["450b4d68-2278-4d9f-9849-0abdfa37ef69"]={f:Ops.Gl.Meshes.Sphere_v2,objName:"Ops.Gl.Meshes.Sphere_v2"};




// **************************************************************
// 
// Ops.Gl.Matrix.Scale
// 
// **************************************************************

Ops.Gl.Matrix.Scale = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    render=op.inTrigger("render"),
    scale=op.inValueFloat("scale",1.0),
    trigger=op.outTrigger("trigger");

const cgl=op.patch.cgl;
const vScale=vec3.create();

scale.onChange=scaleChanged;
scaleChanged();

render.onTriggered=function()
{
    cgl.pushModelMatrix();
    mat4.scale(cgl.mMatrix,cgl.mMatrix, vScale);
    trigger.trigger();
    cgl.popModelMatrix();
};

function scaleChanged()
{
    var s=scale.get();
    vec3.set(vScale, s,s,s);
}



};

Ops.Gl.Matrix.Scale.prototype = new CABLES.Op();
CABLES.OPS["50e7f565-0cdb-47ca-912b-87c04e2f00e3"]={f:Ops.Gl.Matrix.Scale,objName:"Ops.Gl.Matrix.Scale"};




// **************************************************************
// 
// Ops.Gl.Texture_v2
// 
// **************************************************************

Ops.Gl.Texture_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    filename=op.inUrl("File","image"),
    tfilter=op.inSwitch("Filter",['nearest','linear','mipmap']),
    wrap=op.inValueSelect("Wrap",['repeat','mirrored repeat','clamp to edge'],"clamp to edge"),
    aniso=op.inSwitch("Anisotropic",[0,1,2,4,8,16],0),
    flip=op.inValueBool("Flip",false),
    unpackAlpha=op.inValueBool("Pre Multiplied Alpha",false),
    active=op.inValueBool("Active",true),
    textureOut=op.outTexture("Texture"),
    width=op.outValue("Width"),
    height=op.outValue("Height"),
    ratio=op.outValue("Aspect Ratio"),
    loaded=op.outValue("Loaded");

op.setPortGroup("Size",[width,height]);

unpackAlpha.hidePort();

op.toWorkPortsNeedToBeLinked(textureOut);

const cgl=op.patch.cgl;

var loadedFilename=null;
var loadingId=null;
var tex=null;
var cgl_filter=0;
var cgl_wrap=0;
var cgl_aniso=0;
var timedLoader=0;

filename.onChange=flip.onChange=function(){reloadSoon();};
aniso.onChange=tfilter.onChange=onFilterChange;
wrap.onChange=onWrapChange;
unpackAlpha.onChange=function(){ reloadSoon(); };

loaded.set(false);

tfilter.set('mipmap');
wrap.set('repeat');

textureOut.set(CGL.Texture.getEmptyTexture(cgl));

active.onChange=function()
{
    if(active.get())
    {
        if(loadedFilename!=filename.get()) realReload();
        else textureOut.set(tex);
    }
    else textureOut.set(CGL.Texture.getEmptyTexture(cgl));
};

var setTempTexture=function()
{
    var t=CGL.Texture.getTempTexture(cgl);
    textureOut.set(t);
};

function reloadSoon(nocache)
{
    clearTimeout(timedLoader);
    timedLoader=setTimeout(function()
    {
        realReload(nocache);
    },30);
}

function realReload(nocache)
{
    if(!active.get())return;
    if(!loadingId)loadingId=cgl.patch.loading.start('textureOp',filename.get());

    var url=op.patch.getFilePath(String(filename.get()));
    if(nocache)url+='?rnd='+CABLES.generateUUID();


    loadedFilename=filename.get();

    if((filename.get() && filename.get().length>1))
    {
        loaded.set(false);

        op.setUiAttrib({"extendTitle":CABLES.basename(url)});
        op.refreshParams();

        if(tex)tex.delete();
        tex=CGL.Texture.load(cgl,url,
            function(err)
            {
                if(err)
                {
                    setTempTexture();
                    console.log(err);
                    op.setUiError('urlerror','could not load texture:<br/>"'+filename.get()+'"',2);
                    cgl.patch.loading.finished(loadingId);
                    return;
                }
                else op.setUiError('urlerror',null);
                textureOut.set(tex);
                width.set(tex.width);
                height.set(tex.height);
                ratio.set(tex.width/tex.height);

                if(!tex.isPowerOfTwo())  op.setUiError('npot','Texture dimensions not power of two! - Texture filtering will not work in WebGL 1.',0);
                else op.setUiError('npot',null);

                textureOut.set(null);
                textureOut.set(tex);


                loaded.set(true);
                cgl.patch.loading.finished(loadingId);

            },{
                anisotropic:cgl_aniso,
                wrap:cgl_wrap,
                flip:flip.get(),
                unpackAlpha:unpackAlpha.get(),
                filter:cgl_filter
            });

        textureOut.set(null);
        textureOut.set(tex);

    }
    else
    {
        cgl.patch.loading.finished(loadingId);
        setTempTexture();
    }
}

function onFilterChange()
{
    if(tfilter.get()=='nearest') cgl_filter=CGL.Texture.FILTER_NEAREST;
    else if(tfilter.get()=='linear') cgl_filter=CGL.Texture.FILTER_LINEAR;
    else if(tfilter.get()=='mipmap') cgl_filter=CGL.Texture.FILTER_MIPMAP;
    else if(tfilter.get()=='Anisotropic') cgl_filter=CGL.Texture.FILTER_ANISOTROPIC;

    cgl_aniso=parseFloat(aniso.get());

    reloadSoon();
}

function onWrapChange()
{
    if(wrap.get()=='repeat') cgl_wrap=CGL.Texture.WRAP_REPEAT;
    if(wrap.get()=='mirrored repeat') cgl_wrap=CGL.Texture.WRAP_MIRRORED_REPEAT;
    if(wrap.get()=='clamp to edge') cgl_wrap=CGL.Texture.WRAP_CLAMP_TO_EDGE;

    reloadSoon();
}

op.onFileChanged=function(fn)
{
    if(filename.get() && filename.get().indexOf(fn)>-1)
    {
        textureOut.set(null);
        textureOut.set(CGL.Texture.getTempTexture(cgl));
        realReload(true);
    }
};



};

Ops.Gl.Texture_v2.prototype = new CABLES.Op();
CABLES.OPS["790f3702-9833-464e-8e37-6f0f813f7e16"]={f:Ops.Gl.Texture_v2,objName:"Ops.Gl.Texture_v2"};




// **************************************************************
// 
// Ops.Gl.Shader.MatCapMaterialNew_v2
// 
// **************************************************************

Ops.Gl.Shader.MatCapMaterialNew_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={matcap_frag:"\n{{MODULES_HEAD}}\n\nIN vec3 norm;\nIN vec2 texCoord;\nIN vec3 vNorm;\nUNI mat4 viewMatrix;\n\nUNI float opacity;\n\nUNI float r;\nUNI float g;\nUNI float b;\n\nIN vec3 e;\n\n\nUNI sampler2D texMatcap;\n\n#ifdef HAS_DIFFUSE_TEXTURE\n   UNI sampler2D texDiffuse;\n#endif\n\n#ifdef USE_SPECULAR_TEXTURE\n   UNI sampler2D texSpec;\n   UNI sampler2D texSpecMatCap;\n#endif\n\n#ifdef HAS_AO_TEXTURE\n    UNI sampler2D texAo;\n    UNI float aoIntensity;\n#endif\n\n#ifdef HAS_NORMAL_TEXTURE\n   IN vec3 vBiTangent;\n   IN vec3 vTangent;\n\n   UNI sampler2D texNormal;\n   UNI mat4 normalMatrix;\n\n   vec2 vNormt;\n#endif\n\n#ifdef HAS_TEXTURE_OPACITY\n    UNI sampler2D texOpacity;\n#endif\n\n#ifdef CALC_SSNORMALS\n    // from https://www.enkisoftware.com/devlogpost-20150131-1-Normal_generation_in_the_pixel_shader\n    IN vec3 eye_relative_pos;\n#endif\n\n\nconst float normalScale=0.4;\n\nconst vec2 invAtan = vec2(0.1591, 0.3183);\nvec2 sampleSphericalMap(vec3 direction)\n{\n    vec2 uv = vec2(atan(direction.z, direction.x), asin(direction.y));\n    uv *= invAtan;\n    uv += 0.5;\n    return uv;\n}\n\n\nvoid main()\n{\n    vec2 vnOrig=vNorm.xy;\n    vec2 vn=vNorm.xy;\n\n    #ifdef PER_PIXEL\n\n        vec3 ref = reflect( e, vNorm );\n        // ref=(ref);\n\n        // ref.z+=1.;\n        // ref=normalize(ref);\n\n        // float m = 2. * sqrt(\n        //     pow(ref.x, 2.0)+\n        //     pow(ref.y, 2.0)+\n        //     pow(ref.z+1., 2.0)\n        // );\n\n        float m = 2.58284271247461903 * sqrt( (length(ref)) );\n\n        vn.xy = ref.xy / m + 0.5;\n    #endif\n\n    #ifdef HAS_TEXTURES\n        vec2 texCoords=texCoord;\n        {{MODULE_BEGIN_FRAG}}\n    #endif\n\n    #ifdef CALC_SSNORMALS\n    \tvec3 dFdxPos = dFdx( eye_relative_pos );\n    \tvec3 dFdyPos = dFdy( eye_relative_pos );\n    \tvec3 ssn = normalize( cross(dFdxPos,dFdyPos ));\n\n        vec3 rr = reflect( e, ssn );\n        float ssm = 2. * sqrt(\n            pow(rr.x, 2.0)+\n            pow(rr.y, 2.0)+\n            pow(rr.z + 1.0, 2.0)\n        );\n\n\n        vn = (rr.xy / ssm + 0.5);\n\n        vn.t=clamp(vn.t, 0.0, 1.0);\n        vn.s=clamp(vn.s, 0.0, 1.0);\n\n        // float dst = dot(abs(coord-center), vec2(1.0));\n        // float aaf = fwidth(dst);\n        // float alpha = smoothstep(radius - aaf, radius, dst);\n\n    #endif\n\n   #ifdef HAS_NORMAL_TEXTURE\n        vec3 tnorm=texture( texNormal, texCoord ).xyz * 2.0 - 1.0;\n\n        tnorm = normalize(tnorm*normalScale);\n\n        vec3 tangent;\n        vec3 binormal;\n\n        #ifdef CALC_TANGENT\n            vec3 c1 = cross(norm, vec3(0.0, 0.0, 1.0));\n//            vec3 c2 = cross(norm, vec3(0.0, 1.0, 0.0));\n//            if(length(c1)>length(c2)) tangent = c2;\n//                else tangent = c1;\n            tangent = c1;\n            tangent = normalize(tangent);\n            binormal = cross(norm, tangent);\n            binormal = normalize(binormal);\n        #endif\n\n        #ifndef CALC_TANGENT\n            tangent=normalize(vTangent);\n//            tangent.y*=-13.0;\n//            binormal=vBiTangent*norm;\n//            binormal.z*=-1.0;\n//            binormal=normalize(binormal);\n            binormal=normalize( cross( normalize(norm), normalize(vBiTangent) ));\n        // vBinormal = normalize( cross( vNormal, vTangent ) * tangent.w );\n\n        #endif\n\n        tnorm=normalize(tangent*tnorm.x + binormal*tnorm.y + norm*tnorm.z);\n\n        // vec3 n = normalize( mat3(normalMatrix) * (norm+tnorm*normalScale) );\n        vec3 n = normalize( mat3(normalMatrix) * (norm+tnorm*normalScale) );\n\n        vec3 re = reflect( e, n );\n        float m = 2. * sqrt(\n            pow(re.x, 2.0)+\n            pow(re.y, 2.0)+\n            pow(re.z + 1.0, 2.0)\n        );\n\n        vn = (re.xy / m + 0.5);\n\n    #endif\n\n// vn=clamp(vn,0.0,1.0);\n\n\n\n\n\n    vec4 col = texture( texMatcap, vec2(vn.x,1.-vn.y) );\n\n    #ifdef HAS_DIFFUSE_TEXTURE\n        col = col*texture( texDiffuse, texCoords);\n    #endif\n\n    col.r*=r;\n    col.g*=g;\n    col.b*=b;\n\n\n    #ifdef HAS_AO_TEXTURE\n        col = col*\n            mix(\n                vec4(1.0,1.0,1.0,1.0),\n                texture( texAo, texCoords),\n                aoIntensity\n                );\n    #endif\n\n    #ifdef USE_SPECULAR_TEXTURE\n        vec4 spec = texture( texSpecMatCap, vn );\n        spec*= texture( texSpec, texCoords );\n        col+=spec;\n    #endif\n\n    col.a*=opacity;\n    #ifdef HAS_TEXTURE_OPACITY\n            #ifdef TRANSFORMALPHATEXCOORDS\n                texCoords=vec2(texCoord.s,1.0-texCoord.t);\n            #endif\n            #ifdef ALPHA_MASK_ALPHA\n                col.a*=texture(texOpacity,texCoords).a;\n            #endif\n            #ifdef ALPHA_MASK_LUMI\n                col.a*=dot(vec3(0.2126,0.7152,0.0722), texture(texOpacity,texCoords).rgb);\n            #endif\n            #ifdef ALPHA_MASK_R\n                col.a*=texture(texOpacity,texCoords).r;\n            #endif\n            #ifdef ALPHA_MASK_G\n                col.a*=texture(texOpacity,texCoords).g;\n            #endif\n            #ifdef ALPHA_MASK_B\n                col.a*=texture(texOpacity,texCoords).b;\n            #endif\n            // #endif\n    #endif\n\n    {{MODULE_COLOR}}\n\n\n    // #ifdef PER_PIXEL\n\n\n    //     vec2 nn=(vn-0.5)*2.0;\n    //     float ll=length( nn );\n    //     // col.r=0.0;\n    //     // col.b=0.0;\n    //     // col.a=1.0;\n\n    //     // if(ll>0.49 && ll<0.51) col=vec4(0.0,1.0,0.0,1.0);\n    //     // if(ll>0. ) col=vec4(0.0,1.0,0.0,1.0);\n    //     // col=vec4(vn,0.0,1.0);\n\n\n    //     float dd=(vn.x-0.5)*(vn.x-0.5) + (vn.y-0.5)*(vn.y-0.5);\n    //     dd*=4.0;\n\n    //     if(dd>0.94)\n    //     {\n    //     col=vec4(0.0,1.0,0.0,1.0);\n    //         // nn*=0.5;\n    //         // nn+=0.5;\n    //         // nn*=2.0;\n    //         // vn=nn;\n\n    //         // // dd=1.0;\n    //     }\n    //     // else dd=0.0;\n\n    //     // col=vec4(vec3(dd),1.0);\n\n    //     // if(dd>0.95) col=vec4(1.0,0.0,0.0,1.0);\n\n    //     // vec2 test=(vec2(1.0,1.0)-0.5)*2.0;\n    //     // col=vec4(0.0,0.0,length(test),1.0);\n\n    // #endif\n\n\n\n    outColor = col;\n\n}",matcap_vert:"\nIN vec3 vPosition;\nIN vec2 attrTexCoord;\nIN vec3 attrVertNormal;\nIN float attrVertIndex;\nIN vec3 attrTangent;\nIN vec3 attrBiTangent;\n\n#ifdef HAS_NORMAL_TEXTURE\n\n   OUT vec3 vBiTangent;\n   OUT vec3 vTangent;\n#endif\n\nOUT vec2 texCoord;\nOUT vec3 norm;\nUNI mat4 projMatrix;\nUNI mat4 modelMatrix;\nUNI mat4 viewMatrix;\n\nOUT vec3 vNorm;\nOUT vec3 e;\n\nUNI vec2 texOffset;\nUNI vec2 texRepeat;\n\n\n#ifndef INSTANCING\n    UNI mat4 normalMatrix;\n#endif\n\n\n{{MODULES_HEAD}}\n\n#ifdef CALC_SSNORMALS\n    // from https://www.enkisoftware.com/devlogpost-20150131-1-Normal_generation_in_the_pixel_shader\n    OUT vec3 eye_relative_pos;\n#endif\n\nUNI vec3 camPos;\n\n\n// mat3 transposeMat3(mat3 m)\n// {\n//     return mat3(m[0][0], m[1][0], m[2][0],\n//         m[0][1], m[1][1], m[2][1],\n//         m[0][2], m[1][2], m[2][2]);\n// }\n\n// mat3 inverseMat3(mat3 m)\n// {\n//     float a00 = m[0][0], a01 = m[0][1], a02 = m[0][2];\n//     float a10 = m[1][0], a11 = m[1][1], a12 = m[1][2];\n//     float a20 = m[2][0], a21 = m[2][1], a22 = m[2][2];\n\n//     float b01 = a22 * a11 - a12 * a21;\n//     float b11 = -a22 * a10 + a12 * a20;\n//     float b21 = a21 * a10 - a11 * a20;\n\n//     float det = a00 * b01 + a01 * b11 + a02 * b21;\n\n//     return mat3(b01, (-a22 * a01 + a02 * a21), (a12 * a01 - a02 * a11),\n//         b11, (a22 * a00 - a02 * a20), (-a12 * a00 + a02 * a10),\n//         b21, (-a21 * a00 + a01 * a20), (a11 * a00 - a01 * a10)) / det;\n// }\n\nvoid main()\n{\n    texCoord=texRepeat*vec2(attrTexCoord.x,attrTexCoord.y)+texOffset;\n\n\n    norm=attrVertNormal;\n    mat4 mMatrix=modelMatrix;\n    mat4 mvMatrix;\n    vec3 tangent=attrTangent;\n    vec3 bitangent=attrBiTangent;\n\n    #ifdef HAS_NORMAL_TEXTURE\n        vTangent=attrTangent;\n        vBiTangent=attrBiTangent;\n    #endif\n\n    vec4 pos = vec4( vPosition, 1. );\n\n    {{MODULE_VERTEX_POSITION}}\n\n\n    mvMatrix= viewMatrix * mMatrix;\n\n    #ifdef INSTANCING\n        mat4 normalMatrix=mvMatrix;//inverse(transpose(mvMatrix));\n        // mat4 normalMatrix = mat4(transposeMat3(inverseMat3(mat3(mMatrix))));\n\n    #endif\n\n\n    mat3 wmMatrix=mat3(mMatrix);\n\n    e = normalize( vec3( mvMatrix * pos )  );\n    vec3 n = normalize( mat3(normalMatrix*viewMatrix) * (norm) );\n\n    #ifdef PER_PIXEL\n        vNorm=n;\n    #endif\n    #ifndef PER_PIXEL\n        //matcap\n        vec3 r = reflect( e, n );\n\n        // float m = 2. * sqrt(\n        //     pow(r.x, 2.0)+\n        //     pow(r.y, 2.0)+\n        //     pow(r.z + 1.0, 2.0)\n        // );\n\n        float m = 2.58284271247461903 * sqrt(length(r));\n\n        vNorm.xy = r.xy / m + 0.5;\n\n    #endif\n\n\n\n    #ifdef DO_PROJECT_COORDS_XY\n       texCoord=(projMatrix * mvMatrix*pos).xy*0.1;\n    #endif\n\n    #ifdef DO_PROJECT_COORDS_YZ\n       texCoord=(projMatrix * mvMatrix*pos).yz*0.1;\n    #endif\n\n    #ifdef DO_PROJECT_COORDS_XZ\n        texCoord=(projMatrix * mvMatrix*pos).xz*0.1;\n    #endif\n\n    #ifdef CALC_SSNORMALS\n        eye_relative_pos = (mvMatrix * pos ).xyz - camPos;\n    #endif\n\n\n\n   gl_Position = projMatrix * mvMatrix * pos;\n\n}",};
const
    render=op.inTrigger("render"),
    textureMatcap=op.inTexture('MatCap'),
    textureDiffuse=op.inTexture('Diffuse'),
    textureNormal=op.inTexture('Normal'),
    textureSpec=op.inTexture('Specular'),
    textureSpecMatCap=op.inTexture('Specular MatCap'),
    textureAo=op.inTexture('AO Texture'),
    textureOpacity=op.inTexture("Opacity Texture"),
    r=op.inValueSlider('r',1),
    g=op.inValueSlider('g',1),
    b=op.inValueSlider('b',1),
    pOpacity=op.inValueSlider("Opacity",1),
    aoIntensity=op.inValueSlider("AO Intensity",1.0),
    repeatX=op.inValue("Repeat X",1),
    repeatY=op.inValue("Repeat Y",1),
    offsetX=op.inValue("Offset X",0),
    offsetY=op.inValue("Offset Y",0),
    calcTangents = op.inValueBool("calc normal tangents",true),
    projectCoords=op.inValueSelect('projectCoords',['no','xy','yz','xz'],'no'),
    ssNormals=op.inValueBool("Screen Space Normals"),
    next=op.outTrigger("trigger"),
    shaderOut=op.outObject("Shader");

r.setUiAttribs({colorPick:true});

const alphaMaskSource=op.inSwitch("Alpha Mask Source",["Luminance","R","G","B","A"],"Luminance");
alphaMaskSource.setUiAttribs({ greyout:true });

const texCoordAlpha=op.inValueBool("Opacity TexCoords Transform",false);
const discardTransPxl=op.inValueBool("Discard Transparent Pixels");

op.setPortGroup("Texture Opacity",[alphaMaskSource, texCoordAlpha, discardTransPxl]);
op.setPortGroup("Texture maps",[textureDiffuse,textureNormal,textureSpec,textureSpecMatCap,textureAo, textureOpacity]);
op.setPortGroup("Color",[r,g,b,pOpacity]);

const cgl=op.patch.cgl;
const shader=new CGL.Shader(cgl,'MatCapMaterialNew');
var uniOpacity=new CGL.Uniform(shader,'f','opacity',pOpacity);

shader.setModules(['MODULE_VERTEX_POSITION','MODULE_COLOR','MODULE_BEGIN_FRAG']);
shader.setSource(attachments.matcap_vert,attachments.matcap_frag);
shaderOut.set(shader);

var textureMatcapUniform=new CGL.Uniform(shader,'t','texMatcap',0);
var textureDiffuseUniform=null;
var textureNormalUniform=null;
var textureSpecUniform=null;
var textureSpecMatCapUniform=null;
var textureAoUniform=null;
const offsetUniform=new CGL.Uniform(shader,'2f','texOffset',offsetX,offsetY);
const repeatUniform=new CGL.Uniform(shader,'2f','texRepeat',repeatX,repeatY);

var aoIntensityUniform=new CGL.Uniform(shader,'f','aoIntensity',aoIntensity);
b.uniform=new CGL.Uniform(shader,'f','b',b);
g.uniform=new CGL.Uniform(shader,'f','g',g);
r.uniform=new CGL.Uniform(shader,'f','r',r);


calcTangents.onChange=updateDefines;
updateDefines();

function updateDefines()
{
    if(calcTangents.get()) shader.define('CALC_TANGENT');
    else shader.removeDefine('CALC_TANGENT');
}

ssNormals.onChange=function()
{
    if(ssNormals.get())
    {
        if(cgl.glVersion<2)
        {
            cgl.gl.getExtension('OES_standard_derivatives');
            shader.enableExtension('GL_OES_standard_derivatives');
        }

        shader.define('CALC_SSNORMALS');
    }
    else shader.removeDefine('CALC_SSNORMALS');
};

projectCoords.onChange=function()
{
    shader.toggleDefine('DO_PROJECT_COORDS_XY',projectCoords.get()=='xy');
    shader.toggleDefine('DO_PROJECT_COORDS_YZ',projectCoords.get()=='yz');
    shader.toggleDefine('DO_PROJECT_COORDS_XZ',projectCoords.get()=='xz');
};

textureMatcap.onChange=updateMatcap;

function updateMatcap()
{
    if(!cgl.defaultMatcapTex)
    {
        var pixels=new Uint8Array(256*4);
        for(var x=0;x<16;x++)
        {
            for(var y=0;y<16;y++)
            {
                var c=y*16;
                c*=Math.min(1,(x+y/3)/8);
                pixels[(x+y*16)*4+0]=pixels[(x+y*16)*4+1]=pixels[(x+y*16)*4+2]=c;
                pixels[(x+y*16)*4+3]=255;
            }
        }

        cgl.defaultMatcapTex=new CGL.Texture(cgl);
        cgl.defaultMatcapTex.initFromData(pixels,16,16,CGL.Texture.FILTER_LINEAR, CGL.Texture.WRAP_REPEAT);
    }

    // if(textureMatcap.get())
    // {
    //     if(textureMatcapUniform!==null)return;
    //     shader.removeUniform('texMatcap');
    // }
    // else
    // {
    //     // textureMatcap.set(cgl.defaultMatcapTex);

    //     shader.removeUniform('texMatcap');
    //     textureMatcapUniform=new CGL.Uniform(shader,'t','texMatcap',0);
    // }


}

textureDiffuse.onChange=function()
{
    if(textureDiffuse.get())
    {
        if(textureDiffuseUniform!==null)return;
        shader.define('HAS_DIFFUSE_TEXTURE');
        shader.removeUniform('texDiffuse');
        textureDiffuseUniform=new CGL.Uniform(shader,'t','texDiffuse',1);
    }
    else
    {
        shader.removeDefine('HAS_DIFFUSE_TEXTURE');
        shader.removeUniform('texDiffuse');
        textureDiffuseUniform=null;
    }
};

textureNormal.onChange=function()
{
    if(textureNormal.get())
    {
        if(textureNormalUniform!==null)return;
        shader.define('HAS_NORMAL_TEXTURE');
        shader.removeUniform('texNormal');
        textureNormalUniform=new CGL.Uniform(shader,'t','texNormal',2);
    }
    else
    {
        shader.removeDefine('HAS_NORMAL_TEXTURE');
        shader.removeUniform('texNormal');
        textureNormalUniform=null;
    }
};

textureAo.onChange=function()
{
    if(textureAo.get())
    {
        if(textureAoUniform!==null)return;
        shader.define('HAS_AO_TEXTURE');
        shader.removeUniform('texAo');
        textureAoUniform=new CGL.Uniform(shader,'t','texAo',5);
    }
    else
    {
        shader.removeDefine('HAS_AO_TEXTURE');
        shader.removeUniform('texAo');
        textureAoUniform=null;
    }
};

textureSpec.onChange=textureSpecMatCap.onChange=function()
{
    if(textureSpec.get() && textureSpecMatCap.get())
    {
        if(textureSpecUniform!==null)return;
        shader.define('USE_SPECULAR_TEXTURE');
        shader.removeUniform('texSpec');
        shader.removeUniform('texSpecMatCap');
        textureSpecUniform=new CGL.Uniform(shader,'t','texSpec',3);
        textureSpecMatCapUniform=new CGL.Uniform(shader,'t','texSpecMatCap',4);
    }
    else
    {
        shader.removeDefine('USE_SPECULAR_TEXTURE');
        shader.removeUniform('texSpec');
        shader.removeUniform('texSpecMatCap');
        textureSpecUniform=null;
        textureSpecMatCapUniform=null;
    }
};

// TEX OPACITY

function updateAlphaMaskMethod()
{
    if(alphaMaskSource.get()=='Alpha Channel') shader.define('ALPHA_MASK_ALPHA');
    else shader.removeDefine('ALPHA_MASK_ALPHA');

    if(alphaMaskSource.get()=='Luminance') shader.define('ALPHA_MASK_LUMI');
    else shader.removeDefine('ALPHA_MASK_LUMI');

    if(alphaMaskSource.get()=='R') shader.define('ALPHA_MASK_R');
    else shader.removeDefine('ALPHA_MASK_R');

    if(alphaMaskSource.get()=='G') shader.define('ALPHA_MASK_G');
    else shader.removeDefine('ALPHA_MASK_G');

    if(alphaMaskSource.get()=='B') shader.define('ALPHA_MASK_B');
    else shader.removeDefine('ALPHA_MASK_B');
}
alphaMaskSource.onChange=updateAlphaMaskMethod;
textureOpacity.onChange=updateOpacity;

var textureOpacityUniform = null;

function updateOpacity()
{

    if(textureOpacity.get())
    {
        if(textureOpacityUniform!==null)return;
        shader.removeUniform('texOpacity');
        shader.define('HAS_TEXTURE_OPACITY');
        if(!textureOpacityUniform) textureOpacityUniform=new CGL.Uniform(shader,'t','texOpacity',6);

        alphaMaskSource.setUiAttribs({greyout:false});
        discardTransPxl.setUiAttribs({greyout:false});
        texCoordAlpha.setUiAttribs({greyout:false});

    }
    else
    {
        shader.removeUniform('texOpacity');
        shader.removeDefine('HAS_TEXTURE_OPACITY');
        textureOpacityUniform=null;

        alphaMaskSource.setUiAttribs({greyout:true});
        discardTransPxl.setUiAttribs({greyout:true});
        texCoordAlpha.setUiAttribs({greyout:true});
    }
    updateAlphaMaskMethod();
};

discardTransPxl.onChange=function()
{
    if(discardTransPxl.get()) shader.define('DISCARDTRANS');
    else shader.removeDefine('DISCARDTRANS');
};

texCoordAlpha.onChange=function()
{
    if(texCoordAlpha.get()) shader.define('TRANSFORMALPHATEXCOORDS');
    else shader.removeDefine('TRANSFORMALPHATEXCOORDS');
};

op.onDelete=function()
{
    // if(cgl.defaultMatcapTex)
    // {
    //     cgl.defaultMatcapTex.delete();
    //     cgl.defaultMatcapTex=null;
    // }
};

op.preRender=function()
{
    shader.bind();
};

render.onTriggered=function()
{
    if(!cgl.defaultMatcapTex) updateMatcap();
    shader.popTextures();


    const tex=textureMatcap.get() || cgl.defaultMatcapTex;
    shader.pushTexture(textureMatcapUniform,tex.tex);

    if(textureDiffuse.get() && textureDiffuseUniform) shader.pushTexture(textureDiffuseUniform,textureDiffuse.get().tex);
    if(textureNormal.get() && textureNormalUniform) shader.pushTexture(textureNormalUniform,textureNormal.get().tex);
    if(textureSpec.get() && textureSpecUniform) shader.pushTexture(textureSpecUniform,textureSpec.get().tex);
    if(textureSpecMatCap.get() && textureSpecMatCapUniform) shader.pushTexture(textureSpecMatCapUniform,textureSpecMatCap.get().tex);
    if(textureAo.get() && textureAoUniform)  der.pushTexture(textureAoUniform,textureAo.get().tex);
    if(textureOpacity.get() && textureOpacityUniform) shader.pushTexture(textureOpacityUniform, textureOpacity.get().tex);


    cgl.pushShader(shader);
    next.trigger();
    cgl.popShader();
};



};

Ops.Gl.Shader.MatCapMaterialNew_v2.prototype = new CABLES.Op();
CABLES.OPS["264b5f8d-82dd-44c6-ae03-741a702e904a"]={f:Ops.Gl.Shader.MatCapMaterialNew_v2,objName:"Ops.Gl.Shader.MatCapMaterialNew_v2"};




// **************************************************************
// 
// Ops.Sequence
// 
// **************************************************************

Ops.Sequence = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const exe=op.inTrigger("exe");
const exes=[];
const triggers=[];
const num=16;
exe.onTriggered=triggerAll;

function triggerAll()
{
    for(var i=0;i<triggers.length;i++) triggers[i].trigger();
}

for(var i=0;i<num;i++)
{
    triggers.push( op.outTrigger("trigger "+i));

    if(i<num-1)
    {
        var newExe=op.inTrigger("exe "+i);
        newExe.onTriggered=triggerAll;
        exes.push( newExe );
    }
}

};

Ops.Sequence.prototype = new CABLES.Op();
CABLES.OPS["a466bc1f-06e9-4595-8849-bffb9fe22f99"]={f:Ops.Sequence,objName:"Ops.Sequence"};




// **************************************************************
// 
// Ops.Gl.Matrix.Transform
// 
// **************************************************************

Ops.Gl.Matrix.Transform = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    render=op.inTrigger("render"),
    posX=op.inValue("posX",0),
    posY=op.inValue("posY",0),
    posZ=op.inValue("posZ",0),
    scale=op.inValue("scale",1),
    rotX=op.inValue("rotX",0),
    rotY=op.inValue("rotY",0),
    rotZ=op.inValue("rotZ",0),
    trigger=op.outTrigger("trigger");

op.setPortGroup('Rotation',[rotX,rotY,rotZ]);
op.setPortGroup('Position',[posX,posY,posZ]);
op.setPortGroup('Scale',[scale]);
op.setUiAxisPorts(posX,posY,posZ);

const cgl=op.patch.cgl;
var vPos=vec3.create();
var vScale=vec3.create();
var transMatrix = mat4.create();
mat4.identity(transMatrix);

var
    doScale=false,
    doTranslate=false,
    translationChanged=true,
    scaleChanged=true,
    rotChanged=true;

rotX.onChange=rotY.onChange=rotZ.onChange=setRotChanged;
posX.onChange=posY.onChange=posZ.onChange=setTranslateChanged;
scale.onChange=setScaleChanged;

render.onTriggered=function()
{
    // if(!CGL.TextureEffect.checkOpNotInTextureEffect(op)) return;

    var updateMatrix=false;
    if(translationChanged)
    {
        updateTranslation();
        updateMatrix=true;
    }
    if(scaleChanged)
    {
        updateScale();
        updateMatrix=true;
    }
    if(rotChanged) updateMatrix=true;

    if(updateMatrix) doUpdateMatrix();

    cgl.pushModelMatrix();
    mat4.multiply(cgl.mMatrix,cgl.mMatrix,transMatrix);

    trigger.trigger();
    cgl.popModelMatrix();

    if(CABLES.UI && CABLES.UI.showCanvasTransforms) gui.setTransform(op.id,posX.get(),posY.get(),posZ.get());

    if(CABLES.UI && gui.patch().isCurrentOp(op))
        gui.setTransformGizmo(
            {
                posX:posX,
                posY:posY,
                posZ:posZ,
            });
};

op.transform3d=function()
{
    return { pos:[posX,posY,posZ] };
};

function doUpdateMatrix()
{
    mat4.identity(transMatrix);
    if(doTranslate)mat4.translate(transMatrix,transMatrix, vPos);

    if(rotX.get()!==0)mat4.rotateX(transMatrix,transMatrix, rotX.get()*CGL.DEG2RAD);
    if(rotY.get()!==0)mat4.rotateY(transMatrix,transMatrix, rotY.get()*CGL.DEG2RAD);
    if(rotZ.get()!==0)mat4.rotateZ(transMatrix,transMatrix, rotZ.get()*CGL.DEG2RAD);

    if(doScale)mat4.scale(transMatrix,transMatrix, vScale);
    rotChanged=false;
}

function updateTranslation()
{
    doTranslate=false;
    if(posX.get()!==0.0 || posY.get()!==0.0 || posZ.get()!==0.0) doTranslate=true;
    vec3.set(vPos, posX.get(),posY.get(),posZ.get());
    translationChanged=false;
}

function updateScale()
{
    // doScale=false;
    // if(scale.get()!==0.0)
    doScale=true;
    vec3.set(vScale, scale.get(),scale.get(),scale.get());
    scaleChanged=false;
}

function setTranslateChanged()
{
    translationChanged=true;
}

function setScaleChanged()
{
    scaleChanged=true;
}

function setRotChanged()
{
    rotChanged=true;
}

doUpdateMatrix();




};

Ops.Gl.Matrix.Transform.prototype = new CABLES.Op();
CABLES.OPS["650baeb1-db2d-4781-9af6-ab4e9d4277be"]={f:Ops.Gl.Matrix.Transform,objName:"Ops.Gl.Matrix.Transform"};




// **************************************************************
// 
// Ops.Gl.Matrix.Translate
// 
// **************************************************************

Ops.Gl.Matrix.Translate = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const render=op.inTrigger("render");
const trigger=op.outTrigger("trigger")
const x=op.inValue("x");
const y=op.inValue("y");
const z=op.inValue("z");

const cgl=op.patch.cgl;

var vec=vec3.create();

render.onTriggered=function()
{
    vec3.set(vec, x.get(),y.get(),z.get());
    cgl.pushModelMatrix();
    mat4.translate(cgl.mMatrix,cgl.mMatrix, vec);
    trigger.trigger();
    cgl.popModelMatrix();
};


};

Ops.Gl.Matrix.Translate.prototype = new CABLES.Op();
CABLES.OPS["1f89ba0e-e7eb-46d7-8c66-7814b7c528b9"]={f:Ops.Gl.Matrix.Translate,objName:"Ops.Gl.Matrix.Translate"};




// **************************************************************
// 
// Ops.Anim.Timer_v2
// 
// **************************************************************

Ops.Anim.Timer_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    inSpeed=op.inValue("Speed",1),
    playPause=op.inValueBool("Play",true),
    reset=op.inTriggerButton("Reset"),
    inSyncTimeline=op.inValueBool("Sync to timeline",false),
    outTime=op.outValue("Time");

op.setPortGroup("Controls",[playPause,reset,inSpeed]);

const timer=new CABLES.Timer();
var lastTime=null;
var time=0;
var syncTimeline=false;

playPause.onChange=setState;
setState();

function setState()
{
    if(playPause.get())
    {
        timer.play();
        op.patch.addOnAnimFrame(op);
    }
    else
    {
        timer.pause();
        op.patch.removeOnAnimFrame(op);
    }
}

reset.onTriggered=doReset;

function doReset()
{
    time=0;
    lastTime=null;
    timer.setTime(0);
    outTime.set(0);
}

inSyncTimeline.onChange=function()
{
    syncTimeline=inSyncTimeline.get();
    playPause.setUiAttribs({greyout:syncTimeline});
    reset.setUiAttribs({greyout:syncTimeline});
};

op.onAnimFrame=function(tt)
{
    if(timer.isPlaying())
    {

        if(CABLES.overwriteTime!==undefined)
        {
            outTime.set(CABLES.overwriteTime*inSpeed.get());

        } else

        if(syncTimeline)
        {
            outTime.set(tt*inSpeed.get());
        }
        else
        {
            timer.update();
            var timerVal=timer.get();



            if(lastTime===null)
            {
                lastTime=timerVal;
                return;
            }

            var t=Math.abs(timerVal-lastTime);
            lastTime=timerVal;




            time+=t*inSpeed.get();
            if(time!=time)time=0;
            outTime.set(time);
        }
    }
};




};

Ops.Anim.Timer_v2.prototype = new CABLES.Op();
CABLES.OPS["aac7f721-208f-411a-adb3-79adae2e471a"]={f:Ops.Anim.Timer_v2,objName:"Ops.Anim.Timer_v2"};




// **************************************************************
// 
// Ops.Gl.ShaderEffects.PerlinAreaDeform_v3
// 
// **************************************************************

Ops.Gl.ShaderEffects.PerlinAreaDeform_v3 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={perlindeform_vert:"\nUNI bool MOD_smooth;\nUNI float MOD_x,MOD_y,MOD_z;\nUNI float MOD_strength;\nUNI float MOD_size;\nUNI float MOD_scale;\nUNI float MOD_mScale;\nUNI float MOD_scrollx;\nUNI float MOD_scrolly;\nUNI float MOD_scrollz;\nUNI float MOD_fallOff;\n\nvec3 MOD_newTangent,MOD_newBiTangent;\n\n\n\n#ifndef PERLINDEFORM\n#define PERLINDEFORM\nfloat Interpolation_C2( float x ) { return x * x * x * (x * (x * 6.0 - 15.0) + 10.0); }   //  6x^5-15x^4+10x^3\t( Quintic Curve.  As used by Perlin in Improved Noise.  http://mrl.nyu.edu/~perlin/paper445.pdf )\nvec2 Interpolation_C2( vec2 x ) { return x * x * x * (x * (x * 6.0 - 15.0) + 10.0); }\nvec3 Interpolation_C2( vec3 x ) { return x * x * x * (x * (x * 6.0 - 15.0) + 10.0); }\nvec4 Interpolation_C2( vec4 x ) { return x * x * x * (x * (x * 6.0 - 15.0) + 10.0); }\nvec4 Interpolation_C2_InterpAndDeriv( vec2 x ) { return x.xyxy * x.xyxy * ( x.xyxy * ( x.xyxy * ( x.xyxy * vec2( 6.0, 0.0 ).xxyy + vec2( -15.0, 30.0 ).xxyy ) + vec2( 10.0, -60.0 ).xxyy ) + vec2( 0.0, 30.0 ).xxyy ); }\nvec3 Interpolation_C2_Deriv( vec3 x ) { return x * x * (x * (x * 30.0 - 60.0) + 30.0); }\n\n\nvoid FAST32_hash_3D( \tvec3 gridcell,\n                        out vec4 lowz_hash_0,\n                        out vec4 lowz_hash_1,\n                        out vec4 lowz_hash_2,\n                        out vec4 highz_hash_0,\n                        out vec4 highz_hash_1,\n                        out vec4 highz_hash_2\t)\t\t//\tgenerates 3 random numbers for each of the 8 cell corners\n{\n    //    gridcell is assumed to be an integer coordinate\n\n    //\tTODO: \tthese constants need tweaked to find the best possible noise.\n    //\t\t\tprobably requires some kind of brute force computational searching or something....\n    const vec2 OFFSET = vec2( 50.0, 161.0 );\n    const float DOMAIN = 69.0;\n    const vec3 SOMELARGEFLOATS = vec3( 635.298681, 682.357502, 668.926525 );\n    const vec3 ZINC = vec3( 48.500388, 65.294118, 63.934599 );\n\n    //\ttruncate the domain\n    gridcell.xyz = gridcell.xyz - floor(gridcell.xyz * ( 1.0 / DOMAIN )) * DOMAIN;\n    vec3 gridcell_inc1 = step( gridcell, vec3( DOMAIN - 1.5 ) ) * ( gridcell + 1.0 );\n\n    //\tcalculate the noise\n    vec4 P = vec4( gridcell.xy, gridcell_inc1.xy ) + OFFSET.xyxy;\n    P *= P;\n    P = P.xzxz * P.yyww;\n    vec3 lowz_mod = vec3( 1.0 / ( SOMELARGEFLOATS.xyz + gridcell.zzz * ZINC.xyz ) );\n    vec3 highz_mod = vec3( 1.0 / ( SOMELARGEFLOATS.xyz + gridcell_inc1.zzz * ZINC.xyz ) );\n    lowz_hash_0 = fract( P * lowz_mod.xxxx );\n    highz_hash_0 = fract( P * highz_mod.xxxx );\n    lowz_hash_1 = fract( P * lowz_mod.yyyy );\n    highz_hash_1 = fract( P * highz_mod.yyyy );\n    lowz_hash_2 = fract( P * lowz_mod.zzzz );\n    highz_hash_2 = fract( P * highz_mod.zzzz );\n}\n\n//\n//\tPerlin Noise 3D  ( gradient noise )\n//\tReturn value range of -1.0->1.0\n//\thttp://briansharpe.files.wordpress.com/2011/11/perlinsample.jpg\n//\nfloat Perlin3D( vec3 P )\n{\n    //\testablish our grid cell and unit position\n    vec3 Pi = floor(P);\n    vec3 Pf = P - Pi;\n    vec3 Pf_min1 = Pf - 1.0;\n\n#if 1\n    //\n    //\tclassic noise.\n    //\trequires 3 random values per point.  with an efficent hash function will run faster than improved noise\n    //\n\n    //\tcalculate the hash.\n    //\t( various hashing methods listed in order of speed )\n    vec4 hashx0, hashy0, hashz0, hashx1, hashy1, hashz1;\n    FAST32_hash_3D( Pi, hashx0, hashy0, hashz0, hashx1, hashy1, hashz1 );\n    //SGPP_hash_3D( Pi, hashx0, hashy0, hashz0, hashx1, hashy1, hashz1 );\n\n    //\tcalculate the gradients\n    vec4 grad_x0 = hashx0 - 0.49999;\n    vec4 grad_y0 = hashy0 - 0.49999;\n    vec4 grad_z0 = hashz0 - 0.49999;\n    vec4 grad_x1 = hashx1 - 0.49999;\n    vec4 grad_y1 = hashy1 - 0.49999;\n    vec4 grad_z1 = hashz1 - 0.49999;\n    vec4 grad_results_0 = inversesqrt( grad_x0 * grad_x0 + grad_y0 * grad_y0 + grad_z0 * grad_z0 ) * ( vec2( Pf.x, Pf_min1.x ).xyxy * grad_x0 + vec2( Pf.y, Pf_min1.y ).xxyy * grad_y0 + Pf.zzzz * grad_z0 );\n    vec4 grad_results_1 = inversesqrt( grad_x1 * grad_x1 + grad_y1 * grad_y1 + grad_z1 * grad_z1 ) * ( vec2( Pf.x, Pf_min1.x ).xyxy * grad_x1 + vec2( Pf.y, Pf_min1.y ).xxyy * grad_y1 + Pf_min1.zzzz * grad_z1 );\n\n#if 1\n    //\tClassic Perlin Interpolation\n    vec3 blend = Interpolation_C2( Pf );\n    vec4 res0 = mix( grad_results_0, grad_results_1, blend.z );\n    vec4 blend2 = vec4( blend.xy, vec2( 1.0 - blend.xy ) );\n    float final = dot( res0, blend2.zxzx * blend2.wwyy );\n    final *= 1.1547005383792515290182975610039;\t\t//\t(optionally) scale things to a strict -1.0->1.0 range    *= 1.0/sqrt(0.75)\n    return final;\n#else\n    //\tClassic Perlin Surflet\n    //\thttp://briansharpe.wordpress.com/2012/03/09/modifications-to-classic-perlin-noise/\n    Pf *= Pf;\n    Pf_min1 *= Pf_min1;\n    vec4 vecs_len_sq = vec4( Pf.x, Pf_min1.x, Pf.x, Pf_min1.x ) + vec4( Pf.yy, Pf_min1.yy );\n    float final = dot( Falloff_Xsq_C2( min( vec4( 1.0 ), vecs_len_sq + Pf.zzzz ) ), grad_results_0 ) + dot( Falloff_Xsq_C2( min( vec4( 1.0 ), vecs_len_sq + Pf_min1.zzzz ) ), grad_results_1 );\n    final *= 2.3703703703703703703703703703704;\t\t//\t(optionally) scale things to a strict -1.0->1.0 range    *= 1.0/cube(0.75)\n    return final;\n#endif\n\n#else\n    //\n    //\timproved noise.\n    //\trequires 1 random value per point.  Will run faster than classic noise if a slow hashing function is used\n    //\n\n    //\tcalculate the hash.\n    //\t( various hashing methods listed in order of speed )\n    vec4 hash_lowz, hash_highz;\n    FAST32_hash_3D( Pi, hash_lowz, hash_highz );\n    //BBS_hash_3D( Pi, hash_lowz, hash_highz );\n    //SGPP_hash_3D( Pi, hash_lowz, hash_highz );\n\n    //\n    //\t\"improved\" noise using 8 corner gradients.  Faster than the 12 mid-edge point method.\n    //\tKen mentions using diagonals like this can cause \"clumping\", but we'll live with that.\n    //\t[1,1,1]  [-1,1,1]  [1,-1,1]  [-1,-1,1]\n    //\t[1,1,-1] [-1,1,-1] [1,-1,-1] [-1,-1,-1]\n    //\n    hash_lowz -= 0.5;\n    vec4 grad_results_0_0 = vec2( Pf.x, Pf_min1.x ).xyxy * sign( hash_lowz );\n    hash_lowz = abs( hash_lowz ) - 0.25;\n    vec4 grad_results_0_1 = vec2( Pf.y, Pf_min1.y ).xxyy * sign( hash_lowz );\n    vec4 grad_results_0_2 = Pf.zzzz * sign( abs( hash_lowz ) - 0.125 );\n    vec4 grad_results_0 = grad_results_0_0 + grad_results_0_1 + grad_results_0_2;\n\n    hash_highz -= 0.5;\n    vec4 grad_results_1_0 = vec2( Pf.x, Pf_min1.x ).xyxy * sign( hash_highz );\n    hash_highz = abs( hash_highz ) - 0.25;\n    vec4 grad_results_1_1 = vec2( Pf.y, Pf_min1.y ).xxyy * sign( hash_highz );\n    vec4 grad_results_1_2 = Pf_min1.zzzz * sign( abs( hash_highz ) - 0.125 );\n    vec4 grad_results_1 = grad_results_1_0 + grad_results_1_1 + grad_results_1_2;\n\n    //\tblend the gradients and return\n    vec3 blend = Interpolation_C2( Pf );\n    vec4 res0 = mix( grad_results_0, grad_results_1, blend.z );\n    vec4 blend2 = vec4( blend.xy, vec2( 1.0 - blend.xy ) );\n    return dot( res0, blend2.zxzx * blend2.wwyy ) * (2.0 / 3.0);\t//\t(optionally) mult by (2.0/3.0) to scale to a strict -1.0->1.0 range\n#endif\n}\n\n#endif\n\nvec3 MOD_deform(vec3 pos,vec3 norm)\n{\n    vec3 modelPos=pos;\n    vec3 forcePos=vec3(MOD_x,MOD_y,MOD_z);\n\n    vec3 vecToOrigin=modelPos-forcePos;\n    float dist=abs(length(vecToOrigin));\n    // float distAlpha = (MOD_size - dist) / MOD_size;\n\n    if(dist*MOD_mScale<MOD_size*MOD_mScale)\n    {\n        vec3 ppos=vec3(pos*MOD_scale*MOD_mScale);\n        ppos.x+=MOD_scrollx;\n        ppos.y+=MOD_scrolly;\n        ppos.z+=MOD_scrollz;\n\n        float p=(Perlin3D(ppos))*MOD_strength;\n\n        float dist=distance(vec3(MOD_x,MOD_y,MOD_z),modelPos);\n        float fallOff=1.0-smoothstep(MOD_fallOff*MOD_size,MOD_size,dist);\n\n        vec3 pnorm=norm;//normalize(pos.xyz);\n\n        #ifdef MOD_METH_MULNORM\n            pos.x+=p*pnorm.x*fallOff;\n            pos.y+=p*pnorm.y*fallOff;\n            pos.z+=p*pnorm.z*fallOff;\n        #endif\n\n        #ifdef MOD_METH_ADD_XYZ\n            pos.x+=p*fallOff;\n            pos.y+=p*fallOff;\n            pos.z+=p*fallOff;\n        #endif\n\n        #ifdef MOD_METH_ADD_Z\n            pos.z+=p*fallOff;\n        #endif\n    }\n\n    return pos;\n}\n\n// LOOK AT THIS./....\n//https://github.com/spite/perlin-experiments/blob/master/chrome.html\n\n\nvec3 MOD_calcNormal(vec3 pos,vec3 norm,vec3 tangent,vec3 bitangent)\n{\n    //http://diary.conewars.com/vertex-displacement-shader/\n    vec4 position=vec4(MOD_deform(pos,norm),1.0);\n\n    vec3 positionAndTangent = MOD_deform( pos + tangent * 0.1,norm );\n    vec3 positionAndBitangent = MOD_deform( pos + bitangent * 0.1,norm );\n\n    MOD_newTangent = ( positionAndTangent - position.xyz ); // leaves just 'tangent'\n    MOD_newBiTangent = ( positionAndBitangent - position.xyz ); // leaves just 'bitangent'\n\n    vec3 newNormal = cross( MOD_newTangent.xyz, MOD_newBiTangent.xyz );\n    return normalize(newNormal.xyz);\n\n}\n\n",perlindeform_body_vert:"\n#ifndef MOD_WORLDSPACE\n    pos.xyz=MOD_deform(pos.xyz,norm.xyz);\n\n    #ifdef MOD_CALC_NORMALS\n        norm=MOD_calcNormal(pos.xyz,norm.xyz,tangent,bitangent);\n    #endif\n#endif\n\n#ifdef MOD_WORLDSPACE\n    pos.xyz=MOD_deform( (mMatrix*pos).xyz ,norm.xyz);\n\n    #ifdef MOD_CALC_NORMALS\n        norm=MOD_calcNormal( (mMatrix*pos).xyz,norm.xyz,tangent,bitangent);\n    #endif\n#endif\n\n#ifdef MOD_CALC_NORMALS\n    tangent=MOD_newTangent;\n    bitangent=MOD_newBiTangent;\n#endif",};
const
    render=op.inTrigger("render"),
    next=op.outTrigger("trigger"),
    inScale=op.inValueFloat("Scale",1),
    inSize=op.inValueFloat("Size",1),
    inStrength=op.inValueFloat("Strength",1),
    inCalcNormals=op.inValueBool("Calc Normals",true),
    inFalloff=op.inValueSlider("Falloff",0.5),
    output=op.inValueSelect("Output",['Mul Normal','Add XYZ','Add Z'],'Add XYZ'),
    x=op.inValueFloat("x"),
    y=op.inValueFloat("y"),
    z=op.inValueFloat("z"),
    scrollx=op.inValueFloat("Scroll X"),
    scrolly=op.inValueFloat("Scroll Y"),
    scrollz=op.inValueFloat("Scroll Z");

const cgl=op.patch.cgl;
inCalcNormals.onChange=updateCalcNormals;
var inWorldSpace=op.inValueBool("WorldSpace");
var shader=null;
var moduleVert=null;
output.onChange=updateOutput;
render.onLinkChanged=removeModule;

var mscaleUni=null;
inWorldSpace.onChange=updateWorldspace;

function updateCalcNormals()
{
    if(!shader)return;
    shader.toggleDefine(moduleVert.prefix+"CALC_NORMALS",inCalcNormals.get());
}

function removeModule()
{
    if(shader && moduleVert) shader.removeModule(moduleVert);
    shader=null;
}

function updateOutput()
{
    if(!shader)return;

    shader.toggleDefine(moduleVert.prefix+"METH_ADD_XYZ",output.get()=='Add XYZ');
    shader.toggleDefine(moduleVert.prefix+"METH_ADD_Z",output.get()=='Add Z');
    shader.toggleDefine(moduleVert.prefix+"METH_MULNORM",output.get()=='Mul Normal');
}

function updateWorldspace()
{
    if(!shader)return;
    if(inWorldSpace.get()) shader.define(moduleVert.prefix+"WORLDSPACE");
        else shader.removeDefine(moduleVert.prefix+"WORLDSPACE");
}

function getScaling(mat)
{
    var m31 = mat[8];
    var m32 = mat[9];
    var m33 = mat[10];
    return Math.hypot(m31, m32, m33);
}

render.onTriggered=function()
{
    if(!cgl.getShader())
    {
        next.trigger();
        return;
    }

    var modelScale=getScaling(cgl.mMatrix);
    if(mscaleUni)mscaleUni.setValue(modelScale);

    if(CABLES.UI)
    {
        cgl.pushModelMatrix();

        if(CABLES.UI && (gui.patch().isCurrentOp(op) ||  CABLES.UI.renderHelper))
        {
            cgl.pushModelMatrix();
            mat4.translate(cgl.mMatrix,cgl.mMatrix,[x.get(),y.get(),z.get()]);
            CABLES.GL_MARKER.drawSphere(op,inSize.get());
            cgl.popModelMatrix();
        }

        if(gui.patch().isCurrentOp(op))
            gui.setTransformGizmo(
                {
                    posX:x,
                    posY:y,
                    posZ:z
                });

        cgl.popModelMatrix();
    }

    if(cgl.getShader()!=shader)
    {
        if(shader) removeModule();
        shader=cgl.getShader();

        moduleVert=shader.addModule(
            {
                title:op.objName,
                name:'MODULE_VERTEX_POSITION',
                srcHeadVert:attachments.perlindeform_vert,
                srcBodyVert:attachments.perlindeform_body_vert
            });

        inSize.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'size',inSize);
        inStrength.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'strength',inStrength);
        inScale.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'scale',inScale);

        scrollx.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'scrollx',scrollx);
        scrolly.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'scrolly',scrolly);
        scrollz.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'scrollz',scrollz);

        x.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'x',x);
        y.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'y',y);
        z.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'z',z);
        inFalloff.uniform=new CGL.Uniform(shader,'f',moduleVert.prefix+'fallOff',inFalloff);

        mscaleUni=new CGL.Uniform(shader,'f',moduleVert.prefix+'mScale',1);

        updateOutput();
        updateWorldspace();
        updateCalcNormals();
    }

    if(!shader)return;

    next.trigger();
};















};

Ops.Gl.ShaderEffects.PerlinAreaDeform_v3.prototype = new CABLES.Op();
CABLES.OPS["e4432ebd-d67c-46e2-b302-619d4f97daab"]={f:Ops.Gl.ShaderEffects.PerlinAreaDeform_v3,objName:"Ops.Gl.ShaderEffects.PerlinAreaDeform_v3"};




// **************************************************************
// 
// Ops.Gl.Meshes.Rectangle_v2
// 
// **************************************************************

Ops.Gl.Meshes.Rectangle_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    render=op.inTrigger("render"),
    trigger=op.outTrigger('trigger'),
    width=op.inValue("width",1),
    height=op.inValue("height",1),
    pivotX=op.inSwitch("pivot x",["left","center","right"]),
    pivotY=op.inSwitch("pivot y",["top","center","bottom"]),
    nColumns=op.inValueInt("num columns",1),
    nRows=op.inValueInt("num rows",1),
    axis=op.inSwitch("axis",["xy","xz"],"xy"),
    active=op.inValueBool('Active',true),
    geomOut=op.outObject("geometry");

geomOut.ignoreValueSerialize=true;

var cgl=op.patch.cgl;
axis.set('xy');
pivotX.set('center');
pivotY.set('center');

op.setPortGroup('Pivot',[pivotX,pivotY]);
op.setPortGroup('Size',[width,height]);
op.setPortGroup('Structure',[nColumns,nRows]);
op.toWorkPortsNeedToBeLinked(render);

var geom=new CGL.Geometry('rectangle');
var mesh=null;
var needsRebuild=false;

axis.onChange=
    pivotX.onChange=
    pivotY.onChange=
    width.onChange=
    height.onChange=
    nRows.onChange=
    nColumns.onChange=rebuildLater;
rebuild();

function rebuildLater()
{
    needsRebuild=true;
}

op.preRender=
render.onTriggered=function()
{
    if(!CGL.TextureEffect.checkOpNotInTextureEffect(op)) return;
    if(needsRebuild)rebuild();
    if(active.get() && mesh) mesh.render(cgl.getShader());
    trigger.trigger();
};

function rebuild()
{
    var w=width.get();
    var h=parseFloat(height.get());
    var x=0;
    var y=0;

    if(typeof w=='string')w=parseFloat(w);
    if(typeof h=='string')h=parseFloat(h);

    if(pivotX.get()=='center') x=0;
    else if(pivotX.get()=='right') x=-w/2;
    else if(pivotX.get()=='left') x=+w/2;

    if(pivotY.get()=='center') y=0;
    else if(pivotY.get()=='top') y=-h/2;
    else if(pivotY.get()=='bottom') y=+h/2;

    var verts=[];
    var tc=[];
    var norms=[];
    var tangents=[];
    var biTangents=[];
    var indices=[];

    var numRows=Math.round(nRows.get());
    var numColumns=Math.round(nColumns.get());

    var stepColumn=w/numColumns;
    var stepRow=h/numRows;

    var c,r,a;
    a=axis.get();
    for(r=0;r<=numRows;r++)
    {
        for(c=0;c<=numColumns;c++)
        {
            verts.push( c*stepColumn - width.get()/2+x );
            if(a=='xz') verts.push( 0.0 );
            verts.push( r*stepRow - height.get()/2+y );
            if(a=='xy') verts.push( 0.0 );

            tc.push( c/numColumns );
            tc.push( 1.0-r/numRows );

            if(a=='xz')
            {
                norms.push(0,1,0);
                tangents.push(1,0,0);
                biTangents.push(0,0,1);
            }
            else if(a=='xy')
            {
                norms.push(0,0,1);
                tangents.push(-1,0,0);
                biTangents.push(0,-1,0);
            }
        }
    }

    for(c=0;c<numColumns;c++)
    {
        for(r=0;r<numRows;r++)
        {
            var ind = c+(numColumns+1)*r;
            var v1=ind;
            var v2=ind+1;
            var v3=ind+numColumns+1;
            var v4=ind+1+numColumns+1;

            indices.push(v1);
            indices.push(v3);
            indices.push(v2);

            indices.push(v2);
            indices.push(v3);
            indices.push(v4);
        }
    }

    geom.clear();
    geom.vertices=verts;
    geom.texCoords=tc;
    geom.verticesIndices=indices;
    geom.vertexNormals=norms;
    geom.tangents=tangents;
    geom.biTangents=biTangents;

    if(numColumns*numRows>64000)geom.unIndex();

    if(!mesh) mesh=new CGL.Mesh(cgl,geom);
    else mesh.setGeom(geom);

    geomOut.set(null);
    geomOut.set(geom);
    needsRebuild=false;
}

op.onDelete=function()
{
    if(mesh)mesh.dispose();
}




};

Ops.Gl.Meshes.Rectangle_v2.prototype = new CABLES.Op();
CABLES.OPS["fc5718d6-11a5-496e-8f16-1c78b1a2824c"]={f:Ops.Gl.Meshes.Rectangle_v2,objName:"Ops.Gl.Meshes.Rectangle_v2"};




// **************************************************************
// 
// Ops.Math.Multiply
// 
// **************************************************************

Ops.Math.Multiply = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const number1=op.inValueFloat("number1");
const number2=op.inValueFloat("number2");
const result=op.outValue("result");

number1.set(1);
number2.set(2);

number1.onChange=update;
number2.onChange=update;
update();

function update()
{
    const n1=number1.get();
    const n2=number2.get();

    if(isNaN(n1))n1=0;
    if(isNaN(n2))n2=0;

    result.set( n1*n2 );
}



};

Ops.Math.Multiply.prototype = new CABLES.Op();
CABLES.OPS["1bbdae06-fbb2-489b-9bcc-36c9d65bd441"]={f:Ops.Math.Multiply,objName:"Ops.Math.Multiply"};




// **************************************************************
// 
// Ops.Gl.Phong.PhongMaterial_v4
// 
// **************************************************************

Ops.Gl.Phong.PhongMaterial_v4 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={simosphong_frag:"\n// https://www.mathematik.uni-marburg.de/~thormae/lectures/graphics1/code/WebGLShaderLightMat/ShaderLightMat.html\nstruct Material {\n\tvec3 diffuse;\n\tfloat shininess;\n\tfloat specularCoefficient;\n};\n\nstruct Light {\n    vec3 position;\n    vec3 color;\n    vec3 specular;\n    int type;\n    vec3 lightProperties; // x - intensity, y - radius, z - falloff\n    vec3 spotProperties; // x - spotExponent, y - cosConeAngle, z - cosConeAngleInner\n    float intensity;\n    float radius;\n    float falloff;\n    float cosConeAngle;\n    float cosConeAngleInner;\n    float spotExponent;\n    vec3 conePointAt;\n};\n\n#ifdef HAS_TEXTURES\n    UNI vec4 inTextureIntensities;\n    #ifdef HAS_TEXTURE_DIFFUSE\n        UNI sampler2D texDiffuse;\n    #endif\n\n    #ifdef HAS_TEXTURE_SPECULAR\n        UNI sampler2D texSpecular;\n    #endif\n\n    #ifdef HAS_TEXTURE_NORMAL\n        UNI sampler2D texNormal;\n    #endif\n\n    #ifdef HAS_TEXTURE_AO\n        UNI sampler2D texAO;\n    #endif\n\n    #ifdef HAS_TEXTURE_EMISSIVE\n        UNI sampler2D texEmissive;\n    #endif\n    #ifdef HAS_TEXTURE_ALPHA\n        UNI sampler2D texAlpha;\n    #endif\n#endif\n\nUNI vec3 camPos;\nUNI mat4 viewMatrix;\nUNI Light lights[MAX_LIGHTS];\nUNI vec4 inDiffuseColor;\nUNI vec4 inMaterialProperties;\n\n\n#ifdef ENABLE_FRESNEL\n    UNI vec4 inFresnel;\n    UNI vec2 inFresnelWidthExponent;\n#endif\n\n\nIN vec2 texCoord;\nIN vec3 normInterpolated;\nIN vec3 fragPos;\nIN mat3 TBN_Matrix;\nIN vec4 cameraSpace_pos;\nIN vec3 v_viewDirection;\n\n\n/* CONSTANTS */\n#define PI 3.1415926535897932384626433832795\n#define NONE -1\n#define AMBIENT 0\n#define POINT 1\n#define DIRECTIONAL 2\n#define SPOT 3\n#define ALBEDO x;\n#define ROUGHNESS y;\n#define SHININESS z;\n#define SPECULAR_AMT w;\n#define NORMAL x;\n#define AO y;\n#define SPECULAR z;\n#define EMISSIVE w;\nconst float TWO_PI = 2.*PI;\nconst float EIGHT_PI = 8.*PI;\n\n{{MODULES_HEAD}}\n\nfloat when_gt(float x, float y) { return max(sign(x - y), 0.0); } // comparator function\nfloat when_eq(float x, float y) { return 1. - abs(sign(x - y)); } // comparator function\nfloat when_neq(float x, float y) { return abs(sign(x - y)); } // comparator function\n\n#ifdef CONSERVE_ENERGY\n    // http://www.rorydriscoll.com/2009/01/25/energy-conservation-in-games/\n    // http://www.farbrausch.de/~fg/articles/phong.pdf\n    float EnergyConservation(float shininess) {\n        #ifdef SPECULAR_PHONG\n            return (shininess + 2.)/TWO_PI;\n        #endif\n        #ifdef SPECULAR_BLINN\n            return (shininess + 8.)/EIGHT_PI;\n        #endif\n\n        #ifdef SPECULAR_SCHLICK\n            return (shininess + 8.)/EIGHT_PI;\n        #endif\n\n        #ifdef SPECULAR_GAUSS\n            return (shininess + 8.)/EIGHT_PI;\n        #endif\n    }\n#endif\n\n#ifdef ENABLE_OREN_NAYAR_DIFFUSE\n    float CalculateOrenNayar(vec3 lightDirection, vec3 viewDirection, vec3 normal) {\n        float LdotV = dot(lightDirection, viewDirection);\n        float NdotL = dot(lightDirection, normal);\n        float NdotV = dot(normal, viewDirection);\n\n        float albedo = inMaterialProperties.ALBEDO;\n        albedo *= 1.8;\n        float s = LdotV - NdotL * NdotV;\n        float t = mix(1., max(NdotL, NdotV), step(0., s));\n\n        float roughness = inMaterialProperties.ROUGHNESS;\n        float sigma2 = roughness * roughness;\n        float A = 1. + sigma2 * (albedo / (sigma2 + 0.13) + 0.5 / (sigma2 + 0.33));\n        float B = 0.45 * sigma2 / (sigma2 + 0.09);\n\n        float factor = albedo * max(0., NdotL) * (A + B * s / t) / PI;\n\n        return factor;\n\n    }\n#endif\n\n#ifdef ENABLE_FRESNEL\n    float CalculateFresnel(vec3 direction, vec3 normal)\n    {\n\n        vec3 nDirection = normalize( direction );\n        vec3 nNormal = normalize( mat3(viewMatrix) * normal );\n        vec3 halfDirection = normalize( nNormal + nDirection );\n\n        float cosine = dot( halfDirection, nDirection );\n        float product = max( cosine, 0.0 );\n        float factor = pow(product, inFresnelWidthExponent.y);\n\n        return 5. * factor;\n\n    }\n#endif\n\n\nfloat CalculateFalloff(Light light, float distance) {\n    float denom = distance / light.radius + 1.0;\n    float attenuation = 1.0 / (denom*denom);\n    float t = (attenuation - light.falloff) / (1.0 - light.falloff);\n    return max(t, 0.0);\n}\n\nvec4 CalculateDiffuseColor(Material material, Light light, vec3 lightDirection, vec3 viewDirection, vec3 normal) {\n    #ifndef ENABLE_OREN_NAYAR_DIFFUSE\n        float lambertian = clamp(dot(lightDirection, normal), 0., 1.);\n    #endif\n\n    #ifdef ENABLE_OREN_NAYAR_DIFFUSE\n        float lambertian = CalculateOrenNayar(lightDirection, viewDirection, normal);\n    #endif\n\n    vec3 diffuseColor = lambertian*material.diffuse*light.color;\n    return vec4(diffuseColor, lambertian);\n}\n\nvec3 CalculateSpecularColor(Material material, Light light, vec3 lightDirection, vec3 viewDirection, vec3 normal, float lambertian) {\n    vec3 specularColor = vec3(0.);\n\n    #ifdef SPECULAR_PHONG\n        vec3 reflectDirection = reflect(-lightDirection, normal);\n        float specularAngle = max(dot(reflectDirection, viewDirection), 0.);\n        float specularFactor = pow(specularAngle, max(0., material.shininess));\n    specularColor = lambertian * specularFactor * material.specularCoefficient * light.specular;\n    #endif\n\n    #ifdef SPECULAR_BLINN\n        vec3 halfDirection = normalize(lightDirection + viewDirection);\n        float specularAngle = max(dot(halfDirection, normal), 0.);\n        float specularFactor = pow(specularAngle, max(0., material.shininess));\n        specularColor = lambertian * specularFactor * material.specularCoefficient * light.specular;\n    #endif\n\n    #ifdef SPECULAR_SCHLICK\n        vec3 halfDirection = normalize(lightDirection + viewDirection);\n        float specularAngle = dot(halfDirection, normal);\n        float shininess = max(0., material.shininess);\n        float specularFactor = specularAngle / (shininess - shininess*specularAngle + specularAngle);\n        specularColor = lambertian * specularFactor * material.specularCoefficient * light.specular;\n    #endif\n\n    #ifdef SPECULAR_GAUSS\n        vec3 halfDirection = normalize(lightDirection + viewDirection);\n        float specularAngle = acos(max(dot(halfDirection, normal), 0.));\n        float exponent = specularAngle * material.shininess * 0.17;\n        exponent = -(exponent*exponent);\n        float specularFactor = exp(exponent);\n\n        specularColor = lambertian*specularFactor * material.specularCoefficient * light.specular;\n    #endif\n\n    #ifdef CONSERVE_ENERGY\n        float conserveEnergyFactor = EnergyConservation(material.shininess);\n        specularColor = conserveEnergyFactor * specularColor;\n    #endif\n\n    return specularColor;\n}\n\nfloat CalculateSpotLightEffect(Light light, vec3 lightDirection) {\n    vec3 spotLightDirection = normalize(light.position-light.conePointAt);\n    float spotAngle = dot(-lightDirection, spotLightDirection);\n    float epsilon = light.cosConeAngle - light.cosConeAngleInner;\n\n    float spotIntensity = clamp((spotAngle - light.cosConeAngle)/epsilon, 0.0, 1.0);\n    spotIntensity = pow(spotIntensity, max(0.01, light.spotExponent));\n\n    return max(0., spotIntensity);\n}\n\nvoid main() {\n    {{MODULE_BEGIN_FRAG}}\n\n    vec3 normal = normalize(normInterpolated);\n    vec2 uv = texCoord;\n\n    #ifdef DOUBLE_SIDED\n        if(!gl_FrontFacing) normal = normal*-1.0;\n    #endif\n\n    Material material;\n    material.diffuse = inDiffuseColor.rgb;\n    material.shininess = inMaterialProperties.SHININESS;\n    material.specularCoefficient = inMaterialProperties.SPECULAR_AMT;\n\n    float alpha = inDiffuseColor.a;\n\n    #ifdef HAS_TEXTURES\n        #ifdef HAS_TEXTURE_DIFFUSE\n            material.diffuse = texture(texDiffuse, texCoord).rgb;\n\n            #ifdef COLORIZE_TEXTURE\n                material.diffuse *= inDiffuseColor.rgb;\n            #endif\n        #endif\n\n        #ifdef HAS_TEXTURE_NORMAL\n            normal = texture(texNormal, texCoord).rgb;\n            normal = normalize(normal * 2. - 1.);\n            float normalIntensity = inTextureIntensities.NORMAL;\n            normal = normalize(mix(vec3(0., 0., 1.), normal, 2. * normalIntensity));\n            normal =normalize(TBN_Matrix * normal);\n        #endif\n    #endif\n\n    vec3 color = vec3(0.);\n    vec4 col = vec4(0.);\n\n    for (int i = 0; i < MAX_LIGHTS; i++) {\n        Light light = lights[i];\n        light.intensity = light.lightProperties.x;\n\n        if (light.type == NONE) continue;\n\n        else if (light.type == AMBIENT) {\n            color = light.intensity*light.color;\n        }\n        else {\n\n            // lightProperties; // x - intensity, y - radius, z - falloff\n            light.radius = light.lightProperties.y;\n            light.falloff = light.lightProperties.z;\n\n            // spotProperties; // x - spotExponent, y - cosConeAngle, z - cosConeAngleInner\n            light.spotExponent = light.spotProperties.x;\n            light.cosConeAngle = light.spotProperties.y;\n            light.cosConeAngleInner = light.spotProperties.z;\n\n             // when light is not directional, we subtract fragpos, if it is we just use position\n            vec3 lightDirection = normalize(light.position - when_neq(float(light.type), float(DIRECTIONAL)) * fragPos);\n            vec3 viewDirection = normalize(v_viewDirection);\n\n\n            #ifdef HAS_TEXTURES\n                #ifdef HAS_TEXTURE_AO\n                    float aoIntensity = inTextureIntensities.AO;\n                    light.color *= mix(vec3(1.), texture(texAO, texCoord).rgb, aoIntensity);\n                #endif\n\n                #ifdef HAS_TEXTURE_SPECULAR\n                    float specularIntensity = inTextureIntensities.SPECULAR;\n                    light.specular *= mix(1., texture(texSpecular, texCoord).r, specularIntensity);\n                #endif\n            #endif\n\n            vec4 diffuseColorLambert = CalculateDiffuseColor(material, light, lightDirection, viewDirection, normal);\n            vec3 diffuseColor = diffuseColorLambert.rgb;\n            float lambertian = diffuseColorLambert.w;\n\n            // attenuation\n            float distanceLightFrag = length(light.position - fragPos);\n            float attenuation = CalculateFalloff(light, distanceLightFrag);\n            attenuation *= when_gt(lambertian, 0.);\n\n            // when_eq returns 1 when light type == DIRECTIONAL, 0 else, we take the max -> dirLight gets attenuation of 1.\n            attenuation = max(attenuation, when_eq(float(light.type), float(DIRECTIONAL)));\n\n            vec3 specularColor = CalculateSpecularColor(material, light, lightDirection, viewDirection, normal, lambertian);\n\n            color = light.intensity * (diffuseColor + specularColor);\n\n            color *= attenuation;\n\n            if (light.type == SPOT) {\n                float spotIntensity = CalculateSpotLightEffect(light, lightDirection);\n                color *= spotIntensity;\n            }\n\n        }\n\n        #ifdef ENABLE_FRESNEL\n            color += inFresnel.rgb * (CalculateFresnel(vec3(cameraSpace_pos), normal) * inFresnel.w * inFresnelWidthExponent.x);\n        #endif\n\n        col += vec4(clamp(color, 0.0, 1.0), alpha);\n    }\n\n    col.a = alpha;\n\n    #ifdef HAS_TEXTURE_ALPHA\n        #ifdef TRANSFORMALPHATEXCOORDS\n            uv=vec2(texCoord.s,1.0-texCoord.t);\n        #endif\n        #ifdef ALPHA_MASK_ALPHA\n            col.a*=texture(texAlpha,uv).a;\n        #endif\n        #ifdef ALPHA_MASK_LUMI\n            col.a*= dot(vec3(0.2126,0.7152,0.0722), texture(texAlpha,uv).rgb);\n        #endif\n        #ifdef ALPHA_MASK_R\n            col.a*=texture(texAlpha,uv).r;\n        #endif\n        #ifdef ALPHA_MASK_G\n            col.a*=texture(texAlpha,uv).g;\n        #endif\n        #ifdef ALPHA_MASK_B\n            col.a*=texture(texAlpha,uv).b;\n        #endif\n    #endif\n\n    #ifdef HAS_TEXTURE_EMISSIVE\n        float emissiveIntensity = inTextureIntensities.EMISSIVE;\n        col.rgb += emissiveIntensity * material.diffuse * texture(texEmissive, texCoord).r;\n    #endif\n\n\n    {{MODULE_COLOR}}\n\n    #ifdef DISCARDTRANS\n        if(col.a<0.2) discard;\n    #endif\n\n    // outColor = vec4(1., 0., 0., 1.);\n    outColor = clamp(col, 0., 1.);\n    //outColor = vec4()\n}",simosphong_vert:"\n{{MODULES_HEAD}}\n\n#define NONE -1\n#define AMBIENT 0\n#define POINT 1\n#define DIRECTIONAL 2\n#define SPOT 3\n\n#define TEX_REPEAT_X x;\n#define TEX_REPEAT_Y y;\n#define TEX_OFFSET_X z;\n#define TEX_OFFSET_Y w;\n\nIN vec3 vPosition;\nIN vec2 attrTexCoord;\nIN vec3 attrVertNormal;\nIN float attrVertIndex;\nIN vec3 attrTangent;\nIN vec3 attrBiTangent;\n\nOUT vec2 texCoord;\nOUT vec3 norm;\nOUT vec3 normInterpolated;\nOUT vec3 tangent;\nOUT vec3 bitangent;\nOUT vec3 fragPos;\n\n\nOUT mat3 TBN_Matrix; // tangent bitangent normal space transform matrix\nOUT vec4 cameraSpace_pos;\nOUT vec3 v_viewDirection;\n\n#ifdef HAS_TEXTURES\n    UNI vec4 inTextureRepeatOffset;\n#endif\n\nstruct Light {\n    vec3 position;\n    int type;\n};\n\nUNI vec3 camPos;\nUNI mat4 projMatrix;\nUNI mat4 viewMatrix;\nUNI mat4 modelMatrix;\nOUT mat3 normalMatrix;\n// UNI mat4 normalMatrix;\n\n\nmat3 transposeMat3(mat3 m)\n{\n    return mat3(m[0][0], m[1][0], m[2][0],\n        m[0][1], m[1][1], m[2][1],\n        m[0][2], m[1][2], m[2][2]);\n}\n\nmat3 inverseMat3(mat3 m)\n{\n    float a00 = m[0][0], a01 = m[0][1], a02 = m[0][2];\n    float a10 = m[1][0], a11 = m[1][1], a12 = m[1][2];\n    float a20 = m[2][0], a21 = m[2][1], a22 = m[2][2];\n\n    float b01 = a22 * a11 - a12 * a21;\n    float b11 = -a22 * a10 + a12 * a20;\n    float b21 = a21 * a10 - a11 * a20;\n\n    float det = a00 * b01 + a01 * b11 + a02 * b21;\n\n    return mat3(b01, (-a22 * a01 + a02 * a21), (a12 * a01 - a02 * a11),\n        b11, (a22 * a00 - a02 * a20), (-a12 * a00 + a02 * a10),\n        b21, (-a21 * a00 + a01 * a20), (a11 * a00 - a01 * a10)) / det;\n}\n\nvoid main()\n{\n    mat4 mMatrix=modelMatrix;\n    vec4 pos=vec4(vPosition,  1.0);\n    mat4 mvMatrix;\n    texCoord=attrTexCoord;\n    norm=attrVertNormal;\n\n    tangent = attrTangent;\n    bitangent = attrBiTangent;\n\n    {{MODULE_VERTEX_POSITION}}\n\n    normalMatrix = transposeMat3(inverseMat3(mat3(mMatrix)));\n    mvMatrix = (viewMatrix * mMatrix);\n\n\n\n    cameraSpace_pos = mvMatrix * pos;\n\n    #ifdef HAS_TEXTURES\n        float repeatX = inTextureRepeatOffset.TEX_REPEAT_X;\n        float offsetX = inTextureRepeatOffset.TEX_OFFSET_X;\n        float repeatY = inTextureRepeatOffset.TEX_REPEAT_Y;\n        float offsetY = inTextureRepeatOffset.TEX_OFFSET_Y;\n\n        texCoord.x *= repeatX;\n        texCoord.x += offsetX;\n        texCoord.y *= repeatY;\n        texCoord.y += offsetY;\n    #endif\n\n   normInterpolated = vec3(normalMatrix*norm);\n\n        vec3 normCameraSpace = normalize((vec4(normInterpolated, 0.0)).xyz);\n        vec3 tangCameraSpace = normalize((mMatrix * vec4(attrTangent, 0.0)).xyz);\n        vec3 bitangCameraSpace = normalize((mMatrix * vec4(attrBiTangent, 0.0)).xyz);\n\n        // re orthogonalization for smoother normals\n        tangCameraSpace = normalize(tangCameraSpace - dot(tangCameraSpace, normCameraSpace) * normCameraSpace);\n        bitangCameraSpace = cross(normCameraSpace, tangCameraSpace);\n\n        TBN_Matrix = mat3(tangCameraSpace, bitangCameraSpace, normCameraSpace);\n\n\n    fragPos = vec3((mMatrix) * pos);\n    v_viewDirection = normalize(camPos - fragPos);\n\n    gl_Position = projMatrix * mvMatrix * pos;\n}",};
function Light(config) {
     this.type = config.type || "point";
     this.color = config.color || [1, 1, 1];
     this.specular = config.specular || [0, 0, 0];
     this.position = config.position || null;
     this.intensity = config.intensity || 1;
     this.radius = config.radius || 1;
     this.falloff = config.falloff || 1;
     this.spotExponent = config.spotExponent || 1;
     this.cosConeAngleInner = Math.cos(CGL.DEG2RAD*config.coneAngleInner) || 0; // spot light
     this.cosConeAngle = config.cosConeAngle || 0;
     this.conePointAt = config.conePointAt || [0, 0, 0];
     return this;
}


const cgl = op.patch.cgl;

const inTrigger = op.inTrigger("Trigger In");


// * DIFFUSE *
const inDiffuseR = op.inFloat("R", Math.random());
const inDiffuseG = op.inFloat("G", Math.random());
const inDiffuseB = op.inFloat("B", Math.random());
const inDiffuseA = op.inFloatSlider("A", 1);
const diffuseColors = [inDiffuseR, inDiffuseG, inDiffuseB, inDiffuseA];
op.setPortGroup("Diffuse Color", diffuseColors);

const inToggleOrenNayar = op.inBool("Enable", false);
const inAlbedo = op.inFloatSlider("Albedo", 0.707);
const inRoughness = op.inFloatSlider("Roughness", 0.835);

inToggleOrenNayar.setUiAttribs({ hidePort: true });
inAlbedo.setUiAttribs({ greyout: true });
inRoughness.setUiAttribs({ greyout: true });
inDiffuseR.setUiAttribs({ colorPick: true });
op.setPortGroup("Oren-Nayar Diffuse",[inToggleOrenNayar, inAlbedo, inRoughness]);


inToggleOrenNayar.onChange = function() {
    if (inToggleOrenNayar.get()) {
        shader.define("ENABLE_OREN_NAYAR_DIFFUSE");
        inAlbedo.setUiAttribs({ greyout: false });
        inRoughness.setUiAttribs({ greyout: false });
    } else {
        shader.removeDefine("ENABLE_OREN_NAYAR_DIFFUSE");
        inAlbedo.setUiAttribs({ greyout: true });
        inRoughness.setUiAttribs({ greyout: true });
    }
}

// * FRESNEL *
const inToggleFresnel=op.inValueBool("Active", false);
inToggleFresnel.setUiAttribs({ hidePort: true });
const inFresnel=op.inValueSlider("Fresnel Intensity", 0.7);
const inFresnelWidth = op.inFloat("Fresnel Width", 1);
const inFresnelExponent = op.inFloat("Fresnel Exponent", 6);
const inFresnelR = op.inFloat("Fresnel R", 1);
const inFresnelG = op.inFloat("Fresnel G", 1);
const inFresnelB = op.inFloat("Fresnel B", 1);
inFresnelR.setUiAttribs({ colorPick: true });

const fresnelArr = [inFresnel, inFresnelWidth, inFresnelExponent, inFresnelR, inFresnelG, inFresnelB];
fresnelArr.forEach(function(port) { port.setUiAttribs({ greyout: true })});
op.setPortGroup("Fresnel", fresnelArr.concat([inToggleFresnel]));

inToggleFresnel.onChange = function() {
    if (inToggleFresnel.get()) {
        shader.define("ENABLE_FRESNEL");
        fresnelArr.forEach(function(port) { port.setUiAttribs({ greyout: false }); })
    } else {
        shader.removeDefine("ENABLE_FRESNEL");
        fresnelArr.forEach(function(port) { port.setUiAttribs({ greyout: true }); })
    }
}

// * SPECULAR *
const inShininess = op.inFloat("Shininess", 4);
const inSpecularCoefficient = op.inFloatSlider("Specular Amount", 1);
const inSpecularMode = op.inSwitch("Specular Model", ["Blinn", "Schlick", "Phong", "Gauss"], "Blinn");

inSpecularMode.setUiAttribs({ hidePort: true });
const specularColors = [inShininess, inSpecularCoefficient, inSpecularMode];
op.setPortGroup("Specular", specularColors);



// * LIGHT *
const inEnergyConservation = op.inValueBool("Energy Conservation", false);
const inToggleDoubleSided = op.inBool("Double Sided Material", false);

inEnergyConservation.setUiAttribs({ hidePort: true });
inToggleDoubleSided.setUiAttribs({ hidePort: true });

const lightProps = [inEnergyConservation, inToggleDoubleSided];
op.setPortGroup("Light Options", lightProps);

// TEXTURES
const inDiffuseTexture = op.inTexture("Diffuse Texture");
const inSpecularTexture = op.inTexture("Specular Texture");
const inNormalTexture = op.inTexture("Normal Map");
const inAoTexture = op.inTexture("AO Texture");
const inEmissiveTexture = op.inTexture("Emissive Texture");
const inAlphaTexture = op.inTexture("Opacity Texture");
op.setPortGroup("Textures",[inDiffuseTexture, inSpecularTexture, inNormalTexture, inAoTexture, inEmissiveTexture, inAlphaTexture]);

// TEXTURE TRANSFORMS
const inColorizeTexture = op.inBool("Colorize Texture",false);
const inDiffuseRepeatX = op.inFloat("Diffuse Repeat X", 1);
const inDiffuseRepeatY = op.inFloat("Diffuse Repeat Y", 1);
const inTextureOffsetX = op.inFloat("Texture Offset X", 0);
const inTextureOffsetY = op.inFloat("Texture Offset Y", 0);
const inSpecularIntensity = op.inFloatSlider("Specular Intensity", 1);
const inNormalIntensity = op.inFloatSlider("Normal Map Intensity", 0.5);
const inAoIntensity = op.inFloatSlider("AO Intensity", 1);
const inEmissiveIntensity = op.inFloat("Emissive Intensity", 1);

inColorizeTexture.setUiAttribs({ hidePort: true });
op.setPortGroup("Texture Transforms",[inNormalIntensity, inAoIntensity, inSpecularIntensity, inEmissiveIntensity, inColorizeTexture, inDiffuseRepeatY, inDiffuseRepeatX, inTextureOffsetY, inTextureOffsetX]);

const alphaMaskSource=op.inSwitch("Alpha Mask Source",["Luminance","R","G","B","A"],"Luminance");
alphaMaskSource.setUiAttribs({ greyout:true });

const texCoordAlpha=op.inValueBool("Opacity TexCoords Transform",false);
const discardTransPxl=op.inValueBool("Discard Transparent Pixels");

texCoordAlpha.setUiAttribs({ hidePort: true });
discardTransPxl.setUiAttribs({ hidePort: true });

op.setPortGroup("Opacity Texture",[alphaMaskSource, texCoordAlpha, discardTransPxl]);



const outTrigger = op.outTrigger("Trigger Out");
const shaderOut = op.outObject("Shader");
shaderOut.ignoreValueSerialize = true;


const shader = new CGL.Shader(cgl,"simosphong");
shader.setModules(['MODULE_VERTEX_POSITION', 'MODULE_COLOR', 'MODULE_BEGIN_FRAG']);
shader.setSource(attachments.simosphong_vert, attachments.simosphong_frag);

shaderOut.set(shader);

let diffuseTextureUniform = null;
let specularTextureUniform = null;
let normalTextureUniform = null;
let aoTextureUniform = null;
let emissiveTextureUniform = null;
let alphaTextureUniform = null;


inColorizeTexture.onChange = function() {
    shader.toggleDefine("COLORIZE_TEXTURE", inColorizeTexture.get());
}
function updateDiffuseTexture() {
    if (inDiffuseTexture.get()) {
            if(!shader.hasDefine('HAS_TEXTURE_DIFFUSE')) {
                shader.define('HAS_TEXTURE_DIFFUSE');
                if (!diffuseTextureUniform) diffuseTextureUniform = new CGL.Uniform(shader, 't', 'texDiffuse', 0);
            }
    } else {
                shader.removeUniform('texDiffuse');
                shader.removeDefine('HAS_TEXTURE_DIFFUSE');
                diffuseTextureUniform = null;
            }
}

function updateSpecularTexture() {
    if (inSpecularTexture.get()) {
        if(!shader.hasDefine('HAS_TEXTURE_SPECULAR')) {
            shader.define('HAS_TEXTURE_SPECULAR');
            if (!specularTextureUniform) specularTextureUniform = new CGL.Uniform(shader, 't', 'texSpecular', 1);
        }
    } else {
        shader.removeUniform('texSpecular');
        shader.removeDefine('HAS_TEXTURE_SPECULAR');
        specularTextureUniform = null;
        }
}

function updateNormalTexture() {
    if (inNormalTexture.get()) {
        if(!shader.hasDefine('HAS_TEXTURE_NORMAL')) {
            shader.define('HAS_TEXTURE_NORMAL');
            if (!normalTextureUniform) normalTextureUniform = new CGL.Uniform(shader, 't', 'texNormal', 2);
        }
    } else {
        shader.removeUniform('texNormal');
        shader.removeDefine('HAS_TEXTURE_NORMAL');
        normalTextureUniform = null;
        }
}

function updateAoTexture() {
    if (inAoTexture.get()) {
        if(!shader.hasDefine('HAS_TEXTURE_AO')) {
            shader.define('HAS_TEXTURE_AO');
            if (!aoTextureUniform) aoTextureUniform = new CGL.Uniform(shader, 't', 'texAO', 3);
        }
    } else {
        shader.removeUniform('texAO');
        shader.removeDefine('HAS_TEXTURE_AO');
        aoTextureUniform = null;
        }
}

function updateEmissiveTexture() {
    if (inEmissiveTexture.get()) {
        if(!shader.hasDefine('HAS_TEXTURE_EMISSIVE')) {
            shader.define('HAS_TEXTURE_EMISSIVE');
            if (!emissiveTextureUniform) emissiveTextureUniform = new CGL.Uniform(shader, 't', 'texEmissive', 4);
        }
    } else {
        shader.removeUniform('texEmissive');
        shader.removeDefine('HAS_TEXTURE_EMISSIVE');
        emissiveTextureUniform = null;
    }
}

// TEX OPACITY

function updateAlphaMaskMethod()
{
    if(alphaMaskSource.get()=='Alpha Channel') shader.define('ALPHA_MASK_ALPHA');
        else shader.removeDefine('ALPHA_MASK_ALPHA');

    if(alphaMaskSource.get()=='Luminance') shader.define('ALPHA_MASK_LUMI');
        else shader.removeDefine('ALPHA_MASK_LUMI');

    if(alphaMaskSource.get()=='R') shader.define('ALPHA_MASK_R');
        else shader.removeDefine('ALPHA_MASK_R');

    if(alphaMaskSource.get()=='G') shader.define('ALPHA_MASK_G');
        else shader.removeDefine('ALPHA_MASK_G');

    if(alphaMaskSource.get()=='B') shader.define('ALPHA_MASK_B');
        else shader.removeDefine('ALPHA_MASK_B');
}
alphaMaskSource.onChange=updateAlphaMaskMethod;

function updateAlphaTexture()
{

    if(inAlphaTexture.get())
    {
        if(alphaTextureUniform !== null) return;
        shader.removeUniform('texAlpha');
        shader.define('HAS_TEXTURE_ALPHA');
        if(!alphaTextureUniform) alphaTextureUniform = new CGL.Uniform(shader,'t','texAlpha', 5);

        alphaMaskSource.setUiAttribs({greyout:false});
        discardTransPxl.setUiAttribs({greyout:false});
        texCoordAlpha.setUiAttribs({greyout:false});

    }
    else
    {
        shader.removeUniform('texAlpha');
        shader.removeDefine('HAS_TEXTURE_ALPHA');
        alphaTextureUniform = null;

        alphaMaskSource.setUiAttribs({greyout:true});
        discardTransPxl.setUiAttribs({greyout:true});
        texCoordAlpha.setUiAttribs({greyout:true});
    }
    updateAlphaMaskMethod();
};

discardTransPxl.onChange=function()
{
    if(discardTransPxl.get()) shader.define('DISCARDTRANS');
        else shader.removeDefine('DISCARDTRANS');
};


texCoordAlpha.onChange=function()
{
    if(texCoordAlpha.get()) shader.define('TRANSFORMALPHATEXCOORDS');
        else shader.removeDefine('TRANSFORMALPHATEXCOORDS');
};


inDiffuseTexture.onChange = updateDiffuseTexture;
inSpecularTexture.onChange = updateSpecularTexture;
inNormalTexture.onChange = updateNormalTexture;
inAoTexture.onChange = updateAoTexture;
inEmissiveTexture.onChange = updateEmissiveTexture;
inAlphaTexture.onChange = updateAlphaTexture;

const MAX_UNIFORM_FRAGMENTS = cgl.gl.getParameter(cgl.gl.MAX_FRAGMENT_UNIFORM_VECTORS);
const MAX_LIGHTS = MAX_UNIFORM_FRAGMENTS === 64 ? 6 : 16;

shader.define('MAX_LIGHTS', MAX_LIGHTS.toString());
shader.define("SPECULAR_PHONG");

const LIGHT_TYPES = {
    none: -1,
    ambient: 0,
    point: 1,
    directional: 2,
    spot: 3,
};

inSpecularMode.onChange = function() {
    if (inSpecularMode.get() === "Phong") {
        shader.define("SPECULAR_PHONG");
        shader.removeDefine("SPECULAR_BLINN");
        shader.removeDefine("SPECULAR_GAUSS");
        shader.removeDefine("SPECULAR_SCHLICK");
    } else if (inSpecularMode.get() === "Blinn") {
        shader.define("SPECULAR_BLINN");
        shader.removeDefine("SPECULAR_PHONG");
        shader.removeDefine("SPECULAR_GAUSS");
        shader.removeDefine("SPECULAR_SCHLICK");
    } else if (inSpecularMode.get() === "Gauss") {
        shader.define("SPECULAR_GAUSS");
        shader.removeDefine("SPECULAR_BLINN");
        shader.removeDefine("SPECULAR_PHONG");
        shader.removeDefine("SPECULAR_SCHLICK");
    } else if (inSpecularMode.get() === "Schlick") {
        shader.define("SPECULAR_SCHLICK");
        shader.removeDefine("SPECULAR_BLINN");
        shader.removeDefine("SPECULAR_PHONG");
        shader.removeDefine("SPECULAR_GAUSS");
    }
}

inEnergyConservation.onChange = function() {
    shader.toggleDefine("CONSERVE_ENERGY", inEnergyConservation.get());
}

inToggleDoubleSided.onChange = function () {
    shader.toggleDefine("DOUBLE_SIDED", inToggleDoubleSided.get());
}

// * INIT UNIFORMS *
const initialUniforms = [
    new CGL.Uniform(shader, "4f", "inMaterialProperties", inAlbedo, inRoughness, inShininess, inSpecularCoefficient),
    new CGL.Uniform(shader, "4f", "inDiffuseColor", inDiffuseR, inDiffuseG, inDiffuseB, inDiffuseA),
    new CGL.Uniform(shader, "4f", "inTextureIntensities", inNormalIntensity, inAoIntensity, inSpecularIntensity, inEmissiveIntensity),
    new CGL.Uniform(shader, "4f", "inTextureRepeatOffset", inDiffuseRepeatX, inDiffuseRepeatY, inTextureOffsetX, inTextureOffsetY),
    new CGL.Uniform(shader, "4f", "inFresnel", inFresnelR, inFresnelG, inFresnelB, inFresnel),
    new CGL.Uniform(shader, "2f", "inFresnelWidthExponent", inFresnelWidth, inFresnelExponent),

];

const lightUniforms = [];

const initialLight = new Light({
    type: "point",
    color: [0.8, 0.8, 0.8],
    specular: [1, 1, 1],
    position: [0, 2, 2.75],
    intensity: 1,
    radius: 15,
    falloff: 0.2,
    cosConeAngleInner: null,
    spotExponent: null,
    coneAngle: null,
    conePointAt: null,
});

for (let i = 0; i < MAX_LIGHTS; i += 1) {
    const lightProperties = [
        i === 0 ? initialLight.intensity : 0,
        i === 0 ? initialLight.radius : 0,
        i === 0 ? initialLight.falloff : 0
        ];
    const lightPropertiesUniform = new CGL.Uniform(shader, "3f", "lights" + "[" + i + "]" + ".lightProperties", lightProperties);
    const spotProperties = [null, null, null];
    const spotPropertiesUniform = new CGL.Uniform(shader, "3f", "lights" + "[" + i + "]" + ".spotProperties", spotProperties);

    lightUniforms.push({
        color: new CGL.Uniform(shader, "3f", "lights" + "[" + i + "]" + ".color", i === 0 ? initialLight.color : [0, 0, 0]),

        specular: new CGL.Uniform(shader, "3f", "lights" + "[" + i + "]" + ".specular", i === 0 ? initialLight.specular : [1, 1, 1]),

        position: new CGL.Uniform(shader, "3f", "lights" + "[" + i + "]" + ".position", i === 0 ? initialLight.position : [0, 0, 0]),

        type: new CGL.Uniform(shader, "i", "lights" + "[" + i + "]" + ".type", i === 0 ? LIGHT_TYPES.point : LIGHT_TYPES.none),

        lightProperties: lightPropertiesUniform,
        intensity: true,
        radius: true,
        falloff: true,

        /* SPOT LIGHT */
        spotProperties: spotPropertiesUniform,
        spotExponent: true,
        cosConeAngle: true,
        cosConeAngleInner: true,
        conePointAt: new CGL.Uniform(shader, "3f", "lights" + "[" + i + "]" + ".conePointAt", null)
    });
};

const render = function() {
    if (!shader) {
        op.log("NO SHADER");
        return;
    }
    if (cgl.shadowPass) {
        outTrigger.trigger();
    } else {
        cgl.pushShader(shader);
        shader.popTextures();

        if (inDiffuseTexture.get()) shader.pushTexture(diffuseTextureUniform, inDiffuseTexture.get().tex);
        if (inSpecularTexture.get()) shader.pushTexture(specularTextureUniform, inSpecularTexture.get().tex);
        if (inNormalTexture.get()) shader.pushTexture(normalTextureUniform, inNormalTexture.get().tex);
        if (inAoTexture.get()) shader.pushTexture(aoTextureUniform, inAoTexture.get().tex);
        if (inEmissiveTexture.get()) shader.pushTexture(emissiveTextureUniform, inEmissiveTexture.get().tex);
        if (inAlphaTexture.get()) shader.pushTexture(alphaTextureUniform, inAlphaTexture.get().tex);

        outTrigger.trigger();
        cgl.popShader();
    }
}

op.preRender = function() {
    shader.bind();
    render();
}

/* transform for default light */
const inverseViewMat = mat4.create();
const vecTemp = vec3.create();
const camPos = vec3.create();

inTrigger.onTriggered = function() {
    if (cgl.frameStore.lightStack) {
        if (cgl.frameStore.lightStack.length === 0) {
            op.setUiError("deflight","Default light is enabled. Please add lights to your patch to make this warning disappear.",0);
            // if there is no lights in the stack, we set the material back to its initialLight
            for (let i = 0; i < lightUniforms.length; i += 1) {
                if (i === 0) {
                    const keys = Object.keys(initialLight);
                    for (let j = 0; j < keys.length; j += 1) {
                        const key = keys[j];
                        if (key === "type") {
                            lightUniforms[i][key].setValue(LIGHT_TYPES[initialLight[key]]);
                        } else {
                            if (lightUniforms[i][key]) {
                                if (key === "radius" || key === "intensity" || key === "falloff") {
                                    lightUniforms[i].lightProperties.setValue([initialLight.intensity, initialLight.radius, initialLight.falloff]);
                                }
                                else if (key === "spotExponent" || key === "cosConeAngle" || key === "cosConeAngleInner") {
                                        lightUniforms[i].spotProperties.setValue([null, null, null]);
                                }
                                else if (key === "position") {
                                    /* transform for default light */
                                    mat4.invert(inverseViewMat, cgl.vMatrix);
                                    vec3.transformMat4(camPos, vecTemp, inverseViewMat);
                                    lightUniforms[i].position.setValue(camPos);

                                }
                                else {
                                    lightUniforms[i][key].setValue(initialLight[key]);
                                }
                            }
                        }
                    }
                } else {
                    lightUniforms[i].type.setValue(LIGHT_TYPES.none);
                }
            }
            render();
        } else {
            // we have lights in the stack
            op.setUiError("deflight",null);
            for (let i = 0; i < MAX_LIGHTS; i += 1) {
                const light = cgl.frameStore.lightStack[i];
                if (!light) {
                    lightUniforms[i].type.setValue(LIGHT_TYPES.none);
                    continue;
                }

                const keys = Object.keys(light);
                for (let j = 0; j < keys.length; j += 1) {
                    const key = keys[j];
                    if (key === "type") {
                        lightUniforms[i][key].setValue(LIGHT_TYPES[light[key]]);
                    }
                    else {
                        if (lightUniforms[i][key]) {
                            if (key === "radius" || key === "intensity" || key === "falloff") {
                                lightUniforms[i].lightProperties.setValue([light.intensity, light.radius, light.falloff]);
                            }
                            else if (key === "spotExponent" || key === "cosConeAngle" || key === "cosConeAngleInner") {
                                lightUniforms[i].spotProperties.setValue([light.spotExponent, light.cosConeAngle, light.cosConeAngleInner]);
                            }
                            else lightUniforms[i][key].setValue(light[key]);
                        }
                    }
                }
            }
        render();
        }
    } else {
        cgl.frameStore.lightStack = [];
    }
}

updateDiffuseTexture();
updateSpecularTexture();
updateNormalTexture();
updateAoTexture();
updateAlphaTexture();



};

Ops.Gl.Phong.PhongMaterial_v4.prototype = new CABLES.Op();
CABLES.OPS["7059501e-31a6-46cd-8939-4d4b82229086"]={f:Ops.Gl.Phong.PhongMaterial_v4,objName:"Ops.Gl.Phong.PhongMaterial_v4"};




// **************************************************************
// 
// Ops.Gl.CubeMap.CubeMapMaterial_v2
// 
// **************************************************************

Ops.Gl.CubeMap.CubeMapMaterial_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={cubemap_frag:"{{MODULES_HEAD}}\n\n#define PI 3.14159265358\n#define PI_TWO 2.*PI\n#define RECIPROCAL_PI 1./PI\n#define RECIPROCAL_PI2 RECIPROCAL_PI/2.\n\nIN vec3 vCoords;\nIN vec3 v_normal;\nIN vec3 v_eyeCoords;\nIN vec3 v_pos;\nIN mat3 normalMatrix;\nIN vec3 texCoords;\nIN vec3 reflectionTexCoords;\nIN vec3 normInterpolated;\nIN vec3 fragPos;\n\nUNI vec3 camPos;\nUNI float inRotation;\nUNI vec3 inColor;\n\n#ifdef TEX_FORMAT_CUBEMAP\n    UNI samplerCube skybox;\n    #define SAMPLETEX textureLod\n#endif\n#ifndef TEX_FORMAT_CUBEMAP\n    #define TEX_FORMAT_EQUIRECT\n    UNI sampler2D skybox;\n    #define SAMPLETEX sampleEquirect\n#endif\n\nUNI mat4 modelMatrix;\nUNI mat4 inverseViewMatrix;\nUNI float miplevel;\n\n#ifdef TEX_FORMAT_EQUIRECT\n    const vec2 invAtan = vec2(0.1591, 0.3183);\n    vec4 sampleEquirect(sampler2D tex,vec3 direction,float lod)\n    {\n        vec3 newDirection = normalize(direction);\n\t\tvec2 sampleUV;\n\t\tsampleUV.x = -1. * (atan( direction.z, direction.x ) * RECIPROCAL_PI2 + 0.75);\n\t\tsampleUV.y = asin( clamp(direction.y, -1., 1.) ) * RECIPROCAL_PI + 0.5;\n\n        return textureLod(tex,sampleUV,lod);\n    }\n#endif\n\nvoid main()\n{\n    float rot = inRotation * PI_TWO;\n    float sa = sin(rot);\n    float ca = cos(rot);\n    mat2 matRotation = mat2(ca,sa,-sa,ca);\n\n    {{MODULE_BEGIN_FRAG}}\n\n    vec3 normal = normalize(normInterpolated);\n\n    vec4 col = vec4(1.0,1.0,1.0,1.0);\n    vec3 viewDirection = normalize((camPos - fragPos));\n\n    #ifdef DO_REFLECTION\n        vec3 envMapNormal = normal;\n        vec3 reflectDirection = reflect(-viewDirection, normal);\n\n        if (!gl_FrontFacing) {\n            reflectDirection.yz *= -1.;\n        } else {\n          // reflectDirection.x *= -1.;\n        }\n\n        #ifdef FLIP_X\n            reflectDirection.x *= -1.;\n        #endif\n        #ifdef FLIP_Y\n            reflectDirection.y *= -1.;\n        #endif\n        #ifdef FLIP_Z\n            reflectDirection.z *= -1.;\n        #endif\n\n        reflectDirection.xz *= matRotation;\n        col = SAMPLETEX(skybox, reflectDirection,1. + miplevel*10.0);\n    #endif\n\n    #ifndef DO_REFLECTION\n        if (!gl_FrontFacing) normal.x *= -1.;\n\n        #ifdef FLIP_X\n            normal.x *= -1.;\n        #endif\n        #ifdef FLIP_Y\n            normal.y *= -1.;\n        #endif\n        #ifdef FLIP_Z\n            normal.z *= -1.;\n        #endif\n\n        normal.xz *= matRotation;\n\n        col = SAMPLETEX(skybox, normal, miplevel * 10.0);\n    #endif\n\n    #ifdef COLORIZE\n        col.rgb *= inColor;\n    #endif\n    {{MODULE_COLOR}}\n\n    outColor=col;\n}\n",cubemap_vert:"\n{{MODULES_HEAD}}\n\nUNI mat4 projMatrix;\nUNI mat4 modelMatrix;\nUNI mat4 viewMatrix;\n\nOUT vec3 v_eyeCoords;\nOUT vec3 v_normal;\nOUT vec3 v_pos;\nOUT mat3 normalMatrix;\nOUT vec3 normInterpolated;\nOUT vec3 norm;\nOUT vec3 texCoords;\nOUT vec3 reflectionTexCoords;\nOUT vec3 fragPos;\nOUT vec4 modelPos;\nIN vec3 vPosition;\nIN vec3 attrVertNormal;\n\nOUT mat4 mvMatrix;\nOUT vec2 texCoord;\nmat3 transposeMat3(mat3 m)\n{\n    return mat3(m[0][0], m[1][0], m[2][0],\n        m[0][1], m[1][1], m[2][1],\n        m[0][2], m[1][2], m[2][2]);\n}\n\nmat3 inverseMat3(mat3 m)\n{\n    float a00 = m[0][0], a01 = m[0][1], a02 = m[0][2];\n    float a10 = m[1][0], a11 = m[1][1], a12 = m[1][2];\n    float a20 = m[2][0], a21 = m[2][1], a22 = m[2][2];\n\n    float b01 = a22 * a11 - a12 * a21;\n    float b11 = -a22 * a10 + a12 * a20;\n    float b21 = a21 * a10 - a11 * a20;\n\n    float det = a00 * b01 + a01 * b11 + a02 * b21;\n\n    return mat3(b01, (-a22 * a01 + a02 * a21), (a12 * a01 - a02 * a11),\n        b11, (a22 * a00 - a02 * a20), (-a12 * a00 + a02 * a10),\n        b21, (-a21 * a00 + a01 * a20), (a11 * a00 - a01 * a10)) / det;\n}\n\nvoid main()\n{\n    vec4 pos = vec4( vPosition, 1. );\n    mat4 mMatrix=modelMatrix;\n\n\n    norm=attrVertNormal;\n\n\n    {{MODULE_VERTEX_POSITION}}\n\n    mat3 mMatrixMat3 = mat3(mMatrix);\n    normalMatrix = transposeMat3(inverseMat3(mMatrixMat3));\n    normInterpolated = vec3(normalMatrix*norm);\n\n\n\n    mvMatrix=viewMatrix*mMatrix;\n    modelPos=mMatrix*pos;\n\n    fragPos = vec3((mMatrix) * pos);\n    gl_Position = projMatrix * mvMatrix * pos;\n\n}",};
//https://jmonkeyengine.github.io/wiki/jme3/advanced/pbr_part3.html
//https://learnopengl.com/PBR/IBL/Diffuse-irradiance

const render = op.inTrigger('render');
const inCubemap = op.inObject("Cubemap");
const inUseReflection = op.inValueBool("Use Reflection", false);
const inMiplevel = op.inValueSlider("Blur",0.0);
op.setPortGroup("Appearance", [inMiplevel, inUseReflection]);
const inRotation = op.inFloat("Rotation", 0);
const inFlipX = op.inBool("Flip X", false);
const inFlipY = op.inBool("Flip Y", false);
const inFlipZ = op.inBool("Flip Z", false);

op.setPortGroup("Transforms", [inRotation, inFlipX, inFlipY, inFlipZ]);
const inColorize = op.inBool("Colorize", false);
const inR = op.inFloatSlider("R", Math.random());
const inG = op.inFloatSlider("G", Math.random());
const inB = op.inFloatSlider("B", Math.random());
inR.setUiAttribs({ colorPick: true });

op.setPortGroup("Color", [inColorize, inR, inG, inB]);

inUseReflection.onChange = inCubemap.onChange =
inFlipX.onChange = inFlipY.onChange = inFlipZ.onChange = updateMapping;
inColorize.onChange = function() {
    shader.toggleDefine("COLORIZE", inColorize.get());
    inR.setUiAttribs({ greyout: !inColorize.get() });
    inG.setUiAttribs({ greyout: !inColorize.get() });
    inB.setUiAttribs({ greyout: !inColorize.get() });

}
const trigger=op.outTrigger('trigger');

const cgl=op.patch.cgl;
const srcVert=attachments.cubemap_vert;
const srcFrag=attachments.cubemap_frag;


const shader=new CGL.Shader(cgl,'cubemap material');
shader.setModules(['MODULE_VERTEX_POSITION','MODULE_COLOR','MODULE_BEGIN_FRAG']);



shader.setSource(srcVert,srcFrag);
inMiplevel.uniform=new CGL.Uniform(shader,'f','miplevel', inMiplevel);
const inRotationUniform = new CGL.Uniform(shader, 'f', 'inRotation', inRotation);
const inColorUniform = new CGL.Uniform(shader, "3f", "inColor", inR, inG, inB);
render.onTriggered=doRender;
updateMapping();


function doRender()
{
    cgl.pushShader(shader);

    if(inCubemap.get())
    {
        if(inCubemap.get().cubemap) cgl.setTexture(0, inCubemap.get().cubemap,cgl.gl.TEXTURE_CUBE_MAP);
        else cgl.setTexture(0, inCubemap.get().tex);
    }
    else cgl.setTexture(0, CGL.Texture.getTempTexture(cgl).tex);

    trigger.trigger();
    cgl.popShader();
}

function updateMapping()
{
    shader.toggleDefine("FLIP_X", inFlipX.get());
    shader.toggleDefine("FLIP_Y", inFlipY.get());
    shader.toggleDefine("FLIP_Z", inFlipZ.get());
    shader.toggleDefine("DO_REFLECTION", inUseReflection.get());

    if(inCubemap.get() && inCubemap.get().cubemap)
    {
        shader.define("TEX_FORMAT_CUBEMAP");
        shader.removeDefine("TEX_FORMAT_EQUIRECT");
    }
    else
    {
        shader.removeDefine("TEX_FORMAT_CUBEMAP");
        shader.define("TEX_FORMAT_EQUIRECT");
    }
}




};

Ops.Gl.CubeMap.CubeMapMaterial_v2.prototype = new CABLES.Op();
CABLES.OPS["d1fce807-a626-433f-ba61-3f59149fe46e"]={f:Ops.Gl.CubeMap.CubeMapMaterial_v2,objName:"Ops.Gl.CubeMap.CubeMapMaterial_v2"};




// **************************************************************
// 
// Ops.Gl.Phong.PointLight_v4
// 
// **************************************************************

Ops.Gl.Phong.PointLight_v4 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};

function Light(config) {
     this.type = config.type || "point";
     this.color = config.color || [1, 1, 1];
     this.specular = config.specular || [0, 0, 0];
     this.position = config.position || null;
     this.intensity = config.intensity || 1;
     this.radius = config.radius || 1;
     this.falloff = config.falloff || 1;
     this.spotExponent = config.spotExponent || 1;
     this.cosConeAngle = Math.cos(CGL.DEG2RAD * this.coneAngle);
     this.conePointAt = config.conePointAt || [0, 0, 0];
     return this;
}

// * OP START *
const inTrigger = op.inTrigger("Trigger In");
const inIntensity = op.inFloat("Intensity", 2);
const inRadius = op.inFloat("Radius", 15);

const inPosX = op.inFloat("X", 0);
const inPosY = op.inFloat("Y", 1);
const inPosZ = op.inFloat("Z", 0.75);

const positionIn = [inPosX, inPosY, inPosZ];
op.setPortGroup("Position", positionIn);

const inR = op.inFloat("R", 0.8);
const inG = op.inFloat("G", 0.8);
const inB = op.inFloat("B", 0.8);

inR.setUiAttribs({ colorPick: true });
const colorIn = [inR, inG, inB];
op.setPortGroup("Color", colorIn);

const inSpecularR = op.inFloat("Specular R", 1);
const inSpecularG = op.inFloat("Specular G", 1);
const inSpecularB = op.inFloat("Specular B", 1);

inSpecularR.setUiAttribs({ colorPick: true });
const colorSpecularIn = [inSpecularR, inSpecularG, inSpecularB];
op.setPortGroup("Specular Color", colorSpecularIn);


const inFalloff = op.inFloatSlider("Falloff", 0.5);

const attribIns = [inIntensity, inRadius];
op.setPortGroup("Light Attributes", attribIns);

const outTrigger = op.outTrigger("Trigger Out");

const inLight = {
  position: [inPosX, inPosY, inPosZ],
  color: [inR, inG, inB],
  specular: [inSpecularR, inSpecularG, inSpecularB],
  intensity: inIntensity,
  radius: inRadius,
  falloff: inFalloff,
};

const cgl = op.patch.cgl;

const light = new Light({
    type: "point",
    position: [0, 1, 2].map(function(i){ return positionIn[i].get() }),
    color: [0 , 1, 2].map(function(i) { return colorIn[i].get() }),
    specular: [0 , 1, 2].map(function(i) { return colorSpecularIn[i].get() }),
    intensity: inIntensity.get(),
    radius: inRadius.get(),
    falloff: inFalloff.get(),
});

Object.keys(inLight).forEach(function(key) {
    if (inLight[key].length) {
        for (let i = 0; i < inLight[key].length; i += 1) {
            inLight[key][i].onChange = function() {
                light[key][i] = inLight[key][i].get();
            }
        }
    } else {
        inLight[key].onChange = function() {
            light[key] = inLight[key].get();
        }
    }
});


const sc=vec3.create();
const result = vec3.create();
const position = vec3.create();
const transVec = vec3.create();

inTrigger.onTriggered = function() {
    if (!cgl.frameStore.lightStack) cgl.frameStore.lightStack = [];

    vec3.set(transVec, inPosX.get(), inPosY.get(), inPosZ.get());
    vec3.transformMat4(position, transVec, cgl.mMatrix);
    light.position = position;

    // mat4.getScaling(sc,cgl.mMatrix);
    // light.radius=inRadius.get()*sc[0];

    if(CABLES.UI && gui.patch().isCurrentOp(op)) {
        gui.setTransformGizmo({
            posX: inPosX,
            posY: inPosY,
            posZ: inPosZ,
        });

        cgl.pushModelMatrix();
        mat4.translate(cgl.mMatrix,cgl.mMatrix, transVec);
        CABLES.GL_MARKER.drawSphere(op, inRadius.get());
        cgl.popModelMatrix();
    }

    cgl.frameStore.lightStack.push(light);
    outTrigger.trigger();
    cgl.frameStore.lightStack.pop();
}




};

Ops.Gl.Phong.PointLight_v4.prototype = new CABLES.Op();
CABLES.OPS["8d7dc3dc-b71d-4ba8-830f-83dca2638359"]={f:Ops.Gl.Phong.PointLight_v4,objName:"Ops.Gl.Phong.PointLight_v4"};




// **************************************************************
// 
// Ops.Ui.PatchInput
// 
// **************************************************************

Ops.Ui.PatchInput = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};

op.getPatchOp=function()
{
    for(var i in op.patch.ops)
    {
        if(op.patch.ops[i].patchId)
        {
            if(op.patch.ops[i].patchId.get()==op.uiAttribs.subPatch)
            {
                return op.patch.ops[i];
            }
        }
    }
};


};

Ops.Ui.PatchInput.prototype = new CABLES.Op();
CABLES.OPS["e3f68bc3-892a-4c78-9974-aca25c27025d"]={f:Ops.Ui.PatchInput,objName:"Ops.Ui.PatchInput"};




// **************************************************************
// 
// Ops.Ui.SubPatch
// 
// **************************************************************

Ops.Ui.SubPatch = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
op.dyn=op.addInPort(new CABLES.Port(op,"create port",CABLES.OP_PORT_TYPE_DYNAMIC));
op.dynOut=op.addOutPort(new CABLES.Port(op,"create port out",CABLES.OP_PORT_TYPE_DYNAMIC));

var dataStr=op.addInPort(new CABLES.Port(op,"dataStr",CABLES.OP_PORT_TYPE_VALUE,{ display:'readonly' }));
op.patchId=op.addInPort(new CABLES.Port(op,"patchId",CABLES.OP_PORT_TYPE_VALUE,{ display:'readonly' }));



dataStr.setUiAttribs({hideParam:true});
op.patchId.setUiAttribs({hideParam:true});

var data={"ports":[],"portsOut":[]};

// Ops.Ui.Patch.maxPatchId=CABLES.generateUUID();

op.patchId.onChange=function()
{
    // console.log("subpatch changed...");
    // clean up old subpatch if empty
    var oldPatchOps=op.patch.getSubPatchOps(oldPatchId);

    // console.log("subpatch has childs ",oldPatchOps.length);

    if(oldPatchOps.length==2)
    {
        for(var i=0;i<oldPatchOps.length;i++)
        {
            // console.log("delete ",oldPatchOps[i]);
            op.patch.deleteOp(oldPatchOps[i].id);
        }
    }
    else
    {
        // console.log("old subpatch has ops.,...");
    }
};

var oldPatchId=CABLES.generateUUID();
op.patchId.set(oldPatchId);

op.onLoaded=function()
{
    // op.patchId.set(CABLES.generateUUID());
};

op.onLoadedValueSet=function()
{
    data=JSON.parse(dataStr.get());
    if(!data)
    {
        data={"ports":[],"portsOut":[]};
    }
    setupPorts();
};

function loadData()
{
}

getSubPatchInputOp();
getSubPatchOutputOp();

var dataLoaded=false;
dataStr.onChange=function()
{
    if(dataLoaded)return;

    if(!dataStr.get())return;
    try
    {
        // console.log('parse subpatch data');
        loadData();
    }
    catch(e)
    {
        // op.log('cannot load subpatch data...');
        console.log(e);
    }
};

function saveData()
{
    dataStr.set(JSON.stringify(data));
}

function addPortListener(newPort,newPortInPatch)
{
    //console.log('newPort',newPort.name);

    newPort.addEventListener("onUiAttrChange",function(attribs)
    {
        console.log("onUiAttrChange!!!");

        if(attribs.title)
        {
            var i=0;
            for(i=0;i<data.portsOut.length;i++)
                if(data.portsOut[i].name==newPort.name)
                    data.portsOut[i].title=attribs.title;

            for(i=0;i<data.ports.length;i++)
                if(data.ports[i].name==newPort.name)
                    data.ports[i].title=attribs.title;

            saveData();
        }

    });


    if(newPort.direction==CABLES.PORT_DIR_IN)
    {
        if(newPort.type==CABLES.OP_PORT_TYPE_FUNCTION)
        {
            newPort.onTriggered=function()
            {
                if(newPortInPatch.isLinked())
                    newPortInPatch.trigger();
            };
        }
        else
        {
            newPort.onChange=function()
            {
                newPortInPatch.set(newPort.get());
            };
        }
    }
}

function setupPorts()
{
    if(!op.patchId.get())return;
    var ports=data.ports||[];
    var portsOut=data.portsOut||[];
    var i=0;

    for(i=0;i<ports.length;i++)
    {
        if(!op.getPortByName(ports[i].name))
        {
            // console.log("ports[i].name",ports[i].name);

            var newPort=op.addInPort(new CABLES.Port(op,ports[i].name,ports[i].type));
            var patchInputOp=getSubPatchInputOp();





            // console.log(patchInputOp);

            var newPortInPatch=patchInputOp.addOutPort(new CABLES.Port(patchInputOp,ports[i].name,ports[i].type));

// console.log('newPortInPatch',newPortInPatch);


            newPort.ignoreValueSerialize=true;
            newPort.setUiAttribs({"editableTitle":true});
            if(ports[i].title)
            {
                newPort.setUiAttribs({"title":ports[i].title});
                newPortInPatch.setUiAttribs({"title":ports[i].title});
            }
            addPortListener(newPort,newPortInPatch);

        }
    }

    for(i=0;i<portsOut.length;i++)
    {
        if(!op.getPortByName(portsOut[i].name))
        {
            var newPortOut=op.addOutPort(new CABLES.Port(op,portsOut[i].name,portsOut[i].type));
            var patchOutputOp=getSubPatchOutputOp();
            var newPortOutPatch=patchOutputOp.addInPort(new CABLES.Port(patchOutputOp,portsOut[i].name,portsOut[i].type));

            newPortOut.ignoreValueSerialize=true;
            newPortOut.setUiAttribs({"editableTitle":true});

            if(portsOut[i].title)
            {
                newPortOut.setUiAttribs({"title":portsOut[i].title});
                newPortOutPatch.setUiAttribs({"title":portsOut[i].title});
            }


            // addPortListener(newPortOut,newPortOutPatch);
            addPortListener(newPortOutPatch,newPortOut);

        }
    }

    dataLoaded=true;

}



op.dyn.onLinkChanged=function()
{
    if(op.dyn.isLinked())
    {
        var otherPort=op.dyn.links[0].getOtherPort(op.dyn);
        op.dyn.removeLinks();
        otherPort.removeLinkTo(op.dyn);


        var newName="in"+data.ports.length+" "+otherPort.parent.name+" "+otherPort.name;

        data.ports.push({"name":newName,"type":otherPort.type});

        setupPorts();

        var l=gui.scene().link(
            otherPort.parent,
            otherPort.getName(),
            op,
            newName
            );

        // console.log('-----+===== ',otherPort.getName(),otherPort.get() );
        // l._setValue();
        // l.setValue(otherPort.get());

        dataLoaded=true;
        saveData();
    }
    else
    {
        setTimeout(function()
        {
            op.dyn.removeLinks();
            gui.patch().removeDeadLinks();
        },100);
    }
};

op.dynOut.onLinkChanged=function()
{
    if(op.dynOut.isLinked())
    {
        var otherPort=op.dynOut.links[0].getOtherPort(op.dynOut);
        op.dynOut.removeLinks();
        otherPort.removeLinkTo(op.dynOut);
        var newName="out"+data.ports.length+" "+otherPort.parent.name+" "+otherPort.name;

        data.portsOut.push({"name":newName,"type":otherPort.type});

        setupPorts();

        gui.scene().link(
            otherPort.parent,
            otherPort.getName(),
            op,
            newName
            );

        dataLoaded=true;
        saveData();
    }
    else
    {
        setTimeout(function()
        {
            op.dynOut.removeLinks();
            gui.patch().removeDeadLinks();
        },100);


        op.log('dynOut unlinked...');
    }
    gui.patch().removeDeadLinks();
};



function getSubPatchOutputOp()
{
    var patchOutputOP=op.patch.getSubPatchOp(op.patchId.get(),'Ops.Ui.PatchOutput');

    if(!patchOutputOP)
    {
        // console.log("Creating output for ",op.patchId.get());
        op.patch.addOp('Ops.Ui.PatchOutput',{'subPatch':op.patchId.get()} );
        patchOutputOP=op.patch.getSubPatchOp(op.patchId.get(),'Ops.Ui.PatchOutput');

        if(!patchOutputOP) console.warn('no patchinput2!');
    }
    return patchOutputOP;

}

function getSubPatchInputOp()
{
    var patchInputOP=op.patch.getSubPatchOp(op.patchId.get(),'Ops.Ui.PatchInput');

    if(!patchInputOP)
    {
        op.patch.addOp('Ops.Ui.PatchInput',{'subPatch':op.patchId.get()} );
        patchInputOP=op.patch.getSubPatchOp(op.patchId.get(),'Ops.Ui.PatchInput');
        if(!patchInputOP) console.warn('no patchinput2!');
    }


    return patchInputOP;
}

op.addSubLink=function(p,p2)
{
    var num=data.ports.length;

    var sublPortname="in"+(num-1)+" "+p2.parent.name+" "+p2.name;
    console.log('sublink! ',sublPortname);

    if(p.direction==CABLES.PORT_DIR_IN)
    {
        var l=gui.scene().link(
            p.parent,
            p.getName(),
            getSubPatchInputOp(),
            sublPortname
            );

        // console.log('- ----=====EEE ',p.getName(),p.get() );
        // console.log('- ----=====EEE ',l.getOtherPort(p).getName() ,l.getOtherPort(p).get() );
    }
    else
    {
        var l=gui.scene().link(
            p.parent,
            p.getName(),
            getSubPatchOutputOp(),
            "out"+(num)+" "+p2.parent.name+" "+p2.name
            );
    }

    var bounds=gui.patch().getSubPatchBounds(op.patchId.get());

    getSubPatchInputOp().uiAttr(
        {
            "translate":
            {
                "x":bounds.minx,
                "y":bounds.miny-100
            }
        });

    getSubPatchOutputOp().uiAttr(
        {
            "translate":
            {
                "x":bounds.minx,
                "y":bounds.maxy+100
            }
        });
    saveData();
    return sublPortname;
};



op.onDelete=function()
{
    for (var i = op.patch.ops.length-1; i >=0 ; i--)
    {
        if(op.patch.ops[i].uiAttribs && op.patch.ops[i].uiAttribs.subPatch==op.patchId.get())
        {
            // console.log(op.patch.ops[i].objName);
            op.patch.deleteOp(op.patch.ops[i].id);
        }
    }



};


};

Ops.Ui.SubPatch.prototype = new CABLES.Op();
CABLES.OPS["84d9a6f0-ed7a-466d-b386-225ed9e89c60"]={f:Ops.Ui.SubPatch,objName:"Ops.Ui.SubPatch"};




// **************************************************************
// 
// Ops.Json3d.Mesh3d
// 
// **************************************************************

Ops.Json3d.Mesh3d = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    exe=op.inTrigger("Render"),
    filename=op.inUrl("file",'3d json'),
    meshIndex=op.inValueInt("Mesh Index",0),
    inNormals=op.inSwitch("Calculate Normals",["no","smooth","flat"],"no"),
    inResize=op.inBool("Resize",true),
    inSize=op.inValue("New Size",1),
    centerPivot=op.inValueBool("Center",true),
    merge=op.inValueBool("Merge All",false),
    next=op.outTrigger("trigger"),
    draw=op.inValueBool("Draw",true),
    geometryOut=op.outObject("Geometry"),
    outScale=op.outValue("Scaling",1.0),
    outName=op.outString("Mesh Name");

op.setPortGroup("Geometry",[centerPivot,merge,inNormals,inSize,inResize]);

const cgl=op.patch.cgl;
var scene=new CABLES.Variable();

var origVerts=[];
var geom=null;
var data=null;
var mesh=null;
var meshes=[];
var currentIndex=-1;
var bounds={};
var needSetMesh=true;
var hasError=false;

op.preRender=
    exe.onTriggered=render;

filename.onChange=
    inNormals.onChange=reload;

centerPivot.onChange=
    inSize.onChange=
    meshIndex.onChange=
    merge.onChange=setMeshLater;

inResize.onChange=
    merge.onChange=updateResizeUi;


function getMeshName(idx)
{

    if(data && data.meshes && data.meshes[idx] && data.meshes[idx].name)return data.meshes[idx].name;

    if(data && data.rootnode && data.rootnode.children && data.rootnode.children.length>idx-1)
    {
        for(var i=0;i<data.rootnode.children.length;i++)
        {
            if(data.rootnode.children[i].meshes && data.rootnode.children[i].meshes.length==1 && data.rootnode.children[i].meshes[0]==idx) return data.rootnode.children[i].name;
        }

    }


    return "unknown";
}

function updateResizeUi()
{
    inSize.setUiAttribs({greyout:!inResize.get()});
    meshIndex.setUiAttribs({greyout:merge.get()});
    setMeshLater();
}

function calcNormals()
{
    if(!geom)
    {
        console.log('calc normals: no geom!');
        return;
    }

    if(inNormals.get()=='smooth')geom.calculateNormals();
    else if(inNormals.get()=='flat')
    {
        geom.unIndex();
        geom.calculateNormals();
    }
}

function render()
{
    if(needSetMesh) setMesh();

    if(draw.get())
    {
        if(mesh) mesh.render(cgl.getShader());
        next.trigger();
    }
}

function setMeshLater()
{
    needSetMesh=true;
}

function updateScale()
{
    if(!geom) return;

    if(inResize.get())
    {
        var scale=inSize.get()/bounds.maxAxis;
        for(var i=0;i<geom.vertices.length;i++)geom.vertices[i]*=scale;
        outScale.set(scale);
    }
    else
    {
        outScale.set(1);
    }
}

function updateInfo(geom)
{
    if(!CABLES.UI)return;

    var nfo='<div class="panel">';

    if(data)
    {
        nfo += 'Mesh '+(currentIndex+1)+' of '+data.meshes.length+'<br/>';
        nfo += '<br/>';
    }

    if(geom)
    {
        nfo += (geom.verticesIndices||[]).length/3+' faces <br/>';
        nfo += (geom.vertices||[]).length/3+' vertices <br/>';
        nfo += (geom.texCoords||[]).length/2+' texturecoords <br/>';
        nfo += (geom.vertexNormals||[]).length/3+' normals <br/>';
        nfo += (geom.tangents||[]).length/3+' tangents <br/>';
        nfo += (geom.biTangents||[]).length/3+' bitangents <br/>';
    }

    nfo+="</div>";

    op.uiAttr({info:nfo});
}

function setMesh()
{
    if(mesh)
    {
        mesh.dispose();
        mesh=null;
    }

    var index=Math.floor(meshIndex.get());

    if(!data || index!=index || !CABLES.UTILS.isNumeric(index) || index<0 || index>=data.meshes.length)
    {
        op.uiAttr({'warning':'mesh not found - index out of range / or no file selected '});
        meshes[index]=null;
        hasError=true;
        outName.set("");
        return;
    }
    else
    {
        if(hasError)
        {
            op.uiAttr({'warning':null});
            hasError=false;
        }
    }

    currentIndex=index;

    geom=new CGL.Geometry();

    if(merge.get())
    {
        for(var i=0;i<data.meshes.length;i++)
        {
            var jsonGeom=data.meshes[i];
            if(jsonGeom)
            {
                var geomNew=CGL.Geometry.json2geom(jsonGeom);
                geom.merge(geomNew);
            }
        }
        outName.set(getMeshName(""));
    }
    else
    {
        var jsonGeom=data.meshes[index];

        outName.set(getMeshName(index));

        if(!jsonGeom)
        {
            mesh=null;
            op.uiAttr({warning:'mesh not found'});
            return;
        }

        geom=CGL.Geometry.json2geom(jsonGeom);
    }

    if(centerPivot.get())geom.center();

    bounds=geom.getBounds();
    updateScale();
    updateInfo(geom);

    if(inNormals.get()!='no')calcNormals();
    geometryOut.set(null);
    geometryOut.set(geom);

    if(mesh)mesh.dispose();

    mesh=new CGL.Mesh(cgl,geom);
    needSetMesh=false;
    meshes[index]=mesh;

    op.uiAttr({'warning':null});
}

function reload()
{
    if(!filename.get())return;
    currentIndex=-1;

    // console.log("reload",filename.get());

    function doLoad()
    {
        CABLES.ajax(
            op.patch.getFilePath(filename.get()),
            function(err,_data,xhr)
            {
                if(err)
                {
                    if(CABLES.UI)op.uiAttr({'error':'could not load file...'});

                    console.error('ajax error:',err);
                    op.patch.loading.finished(loadingId);
                    return;
                }
                else
                {
                    if(CABLES.UI)op.uiAttr({'error':null});
                }

                try
                {
                    data=JSON.parse(_data);
                }
                catch(ex)
                {
                    if(CABLES.UI)op.uiAttr({'error':'could not load file...'});
                    op.patch.loading.finished(loadingId);
                    return;
                }

                needSetMesh=true;
                op.patch.loading.finished(loadingId);
                if(CABLES.UI) gui.jobs().finish('loading3d'+loadingId);
            });
    }

    var loadingId=op.patch.loading.start('json3dMesh',filename.get());

    if(CABLES.UI) gui.jobs().start({id:'loading3d'+loadingId,title:'loading 3d data'},doLoad);
    else doLoad();
}


};

Ops.Json3d.Mesh3d.prototype = new CABLES.Op();
CABLES.OPS["cdf55940-7726-4ab3-854c-1a576434da46"]={f:Ops.Json3d.Mesh3d,objName:"Ops.Json3d.Mesh3d"};




// **************************************************************
// 
// Ops.WebAudio.AudioPlayer
// 
// **************************************************************

Ops.WebAudio.AudioPlayer = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
var self = this;
var patch=this.patch;
// todo: audio object: firefox does not support .loop=true
//
// myAudio = new Audio('someSound.ogg');
// myAudio.addEventListener('ended', function() {
//     this.currentTime = 0;
//     this.play();
// }, false);
// myAudio.play();



this.file=op.inFile("file","audio");
var play=op.addInPort(new CABLES.Port(this,"play",CABLES.OP_PORT_TYPE_VALUE,{ display:'bool' }));
var autoPlay=op.addInPort(new CABLES.Port(this,"Autoplay",CABLES.OP_PORT_TYPE_VALUE,{ display:'bool' }));

var volume=this.addInPort(new CABLES.Port(this,"volume",CABLES.OP_PORT_TYPE_VALUE,{ display:'range' }));
var synchronizedPlayer=this.addInPort(new CABLES.Port(this,"Synchronized Player",CABLES.OP_PORT_TYPE_VALUE,{ display:'bool' }));

this.audioOut=this.addOutPort(new CABLES.Port(this, "audio out",CABLES.OP_PORT_TYPE_OBJECT));
var outPlaying=this.addOutPort(new CABLES.Port(this, "playing",CABLES.OP_PORT_TYPE_VALUE));
var outEnded=this.addOutPort(new CABLES.Port(this, "ended",CABLES.OP_PORT_TYPE_FUNCTION));


var doLoop=op.addInPort(new CABLES.Port(this,"Loop",CABLES.OP_PORT_TYPE_VALUE,{ display:'bool' }));

autoPlay.set(true);
volume.set(1.0);

outPlaying.ignoreValueSerialize=true;
outEnded.ignoreValueSerialize=true;

window.AudioContext = window.AudioContext||window.webkitAudioContext;
if(!window.audioContext) window.audioContext = new AudioContext();

if(!window.audioContext) {
    if(this.patch.config.onError) this.patch.config.onError('sorry, could not initialize WebAudio. Please check if your Browser supports WebAudio');
}

this.filter = audioContext.createGain();
self.audio=null;
var buffer=null;
var playing=false;
outPlaying.set(false);


play.onChange=function()
{

    if(!self.audio)
    {
        op.uiAttr({'error':'No audio file selected'});
        return;
    }
    else op.uiAttr({'error':null});


    if(play.get())
    {
        playing=true;
        var prom = self.audio.play();
        if (prom instanceof Promise)
            prom.then(null,function(e){});
    }
    else
    {
        playing=false;
        self.audio.pause();
    }
    outPlaying.set(playing);
};



this.onDelete=function()
{
    if(self.audio) self.audio.pause();
};


doLoop.onChange=function()
{
    if(self.audio) self.audio.loop=doLoop.get();
    else if(self.media) self.media.loop=doLoop.get();
};

function seek()
{
    // if(!window.gui && CGL.getLoadingStatus()>=1.0)
    // {
    //     console.log('seek canceled',CGL.getLoadingStatus());
    //     return;
    // }

    if(!synchronizedPlayer.get())
    {
        if(!self.audio)return;

        var prom;
        if(self.patch.timer.isPlaying() && self.audio.paused) prom = self.audio.play();
        else if(!self.patch.timer.isPlaying() && !self.audio.paused) prom = self.audio.pause();

        if (prom instanceof Promise)
            prom.then(null, function (e) {});

        self.audio.currentTime=self.patch.timer.getTime();
    }
    else
    {
        if(buffer===null)return;

        var t=self.patch.timer.getTime();
        if(!isFinite(t))
        {
            return;
            // console.log('not finite time...',t);
            // t=0.0;
        }

        playing=false;

        // console.log('seek.....',self.patch.timer.isPlaying());

        if(self.patch.timer.isPlaying() )
        {
            console.log('play!');
            outPlaying.set(true);

            self.media.start(t);
            playing=true;
        }
    }

}

function playPause()
{
    if(!self.audio)return;

    var prom;
    if(self.patch.timer.isPlaying()) prom = self.audio.play();
    else prom = self.audio.pause();
    if (prom instanceof Promise)
        prom.then(null, function (e) {});
}

function updateVolume()
{
    // self.filter.gain.value=(volume.get() || 0)*op.patch.config.masterVolume;
    self.filter.gain.setValueAtTime((volume.get() || 0) * op.patch.config.masterVolume, window.audioContext.currentTime);
}

volume.onChange=updateVolume;
op.onMasterVolumeChanged=updateVolume;

var firstTime=true;
var loadingFilename='';
this.file.onChange=function()
{
    if(!self.file.get())return;
    loadingFilename=op.patch.getFilePath(self.file.get());

    var loadingId=patch.loading.start('audioplayer',self.file.get());


    if(!synchronizedPlayer.get())
    {
        if(self.audio)
        {
            self.audio.pause();
            outPlaying.set(false);
        }
        self.audio = new Audio();

// console.log('load audio',self.file.val);

        self.audio.crossOrigin = "anonymous";
        self.audio.src = op.patch.getFilePath(self.file.get());
        self.audio.loop = doLoop.get();
        self.audio.crossOrigin = "anonymous";

        var canplaythrough=function()
        {
            if(autoPlay.get() || play.get()){
              var prom = self.audio.play();
              if (prom instanceof Promise)
                prom.then(null,function(e){});
            }
            outPlaying.set(true);
            patch.loading.finished(loadingId);
            self.audio.removeEventListener('canplaythrough',canplaythrough, false);
        };

        self.audio.addEventListener('canplaythrough',canplaythrough, false);

        self.audio.addEventListener('ended',function()
        {
            // console.log('audio player ended...');
            outPlaying.set(false);
            playing=false;
            outEnded.trigger();
        }, false);


        self.media = audioContext.createMediaElementSource(self.audio);
        self.media.connect(self.filter);
        self.audioOut.val = self.filter;
    }
    else
    {
        self.media = audioContext.createBufferSource();
        self.media.loop=doLoop.get();

        var request = new XMLHttpRequest();

        request.open( 'GET', op.patch.getFilePath(self.file.get()), true );
        request.responseType = 'arraybuffer';

        request.onload = function()
        {
            var audioData = request.response;

            audioContext.decodeAudioData( audioData, function(res)
            {
                buffer=res;
                // console.log('sound load complete');
                self.media.buffer = res;
                self.media.connect(self.filter);
                self.audioOut.val = self.filter;
                self.media.loop=doLoop.get();

                patch.loading.finished(loadingId);

                // if(!window.gui)
                // {
                //     self.media.start(0);
                //     playing=true;
                // }
            } );

        };

        request.send();

        self.patch.timer.onPlayPause(seek);
        self.patch.timer.onTimeChange(seek);

    }

};


};

Ops.WebAudio.AudioPlayer.prototype = new CABLES.Op();
CABLES.OPS["46f34657-e42b-421b-a632-0e76d190e9ff"]={f:Ops.WebAudio.AudioPlayer,objName:"Ops.WebAudio.AudioPlayer"};




// **************************************************************
// 
// Ops.WebAudio.Output
// 
// **************************************************************

Ops.WebAudio.Output = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
op.requirements=[CABLES.Requirements.WEBAUDIO];

var audioCtx = CABLES.WEBAUDIO.createAudioContext(op);

// constants
var VOLUME_DEFAULT = 1.0;
var VOLUME_MIN = 0;
var VOLUME_MAX = 1;

// vars
var gainNode = audioCtx.createGain();
var destinationNode = audioCtx.destination;
gainNode.connect(destinationNode);
var masterVolume = 1;

// inputs
var audioInPort = CABLES.WEBAUDIO.createAudioInPort(op, "Audio In", gainNode);
var volumePort = op.inValueSlider("Volume", VOLUME_DEFAULT);
var mutePort = op.inValueBool("Mute", false);

// functions
// sets the volume, multiplied by master volume
function setVolume() {
    var volume = volumePort.get() * masterVolume;
    if(volume >= VOLUME_MIN && volume <= VOLUME_MAX) {
        // gainNode.gain.value = volume;
        gainNode.gain.setValueAtTime(volume, audioCtx.currentTime);
    } else {
        // gainNode.gain.value = VOLUME_DEFAULT * masterVolume;
        gainNode.gain.setValueAtTime(VOLUME_DEFAULT * masterVolume, audioCtx.currentTime);
    }
}

function mute(b) {
    if(b) {
        // gainNode.gain.value = 0;
        gainNode.gain.setValueAtTime(0, audioCtx.currentTime);
    } else {
        setVolume();
    }
}

// change listeners
mutePort.onChange = function() {
    mute(mutePort.get());
};

volumePort.onChange = function() {
    if(mutePort.get()) {
        return;
    }
    setVolume();
};

op.onMasterVolumeChanged = function(v) {
    masterVolume = v;
    setVolume();
};




};

Ops.WebAudio.Output.prototype = new CABLES.Op();
CABLES.OPS["53fdbf4a-bc8d-4c5d-a698-f34fdeb53827"]={f:Ops.WebAudio.Output,objName:"Ops.WebAudio.Output"};




// **************************************************************
// 
// Ops.Math.MapRange
// 
// **************************************************************

Ops.Math.MapRange = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};

const result=op.outValue("result");
var v=op.inValueFloat("value");
var old_min=op.inValueFloat("old min");
var old_max=op.inValueFloat("old max");
var new_min=op.inValueFloat("new min");
var new_max=op.inValueFloat("new max");
var easing=op.inValueSelect("Easing",["Linear","Smoothstep","Smootherstep"],"Linear");

op.setPortGroup("Input Range",[old_min,old_max]);
op.setPortGroup("Output Range",[new_min,new_max]);

var ease=0;
var r=0;

easing.onChange=function()
{
    if(easing.get()=="Smoothstep") ease=1;
        else if(easing.get()=="Smootherstep") ease=2;
            else ease=0;
};


function exec()
{
    if(v.get()>=Math.max( old_max.get(),old_min.get() ))
    {
        result.set(new_max.get());
        return;
    }
    else
    if(v.get()<=Math.min( old_max.get(),old_min.get() ))
    {
        result.set(new_min.get());
        return;
    }

    var nMin=new_min.get();
    var nMax=new_max.get();
    var oMin=old_min.get();
    var oMax=old_max.get();
    var x=v.get();

    var reverseInput = false;
    var oldMin = Math.min( oMin, oMax );
    var oldMax = Math.max( oMin, oMax );
    if(oldMin!= oMin) reverseInput = true;

    var reverseOutput = false;
    var newMin = Math.min( nMin, nMax );
    var newMax = Math.max( nMin, nMax );
    if(newMin != nMin) reverseOutput = true;

    var portion=0;

    if(reverseInput) portion = (oldMax-x)*(newMax-newMin)/(oldMax-oldMin);
        else portion = (x-oldMin)*(newMax-newMin)/(oldMax-oldMin);

    if(reverseOutput) r=newMax - portion;
        else r=portion + newMin;

    if(ease===0)
    {
        result.set(r);
    }
    else
    if(ease==1)
    {
        x = Math.max(0, Math.min(1, (r-nMin)/(nMax-nMin)));
        result.set( nMin+x*x*(3 - 2*x)* (nMax-nMin) ); // smoothstep
    }
    else
    if(ease==2)
    {
        x = Math.max(0, Math.min(1, (r-nMin)/(nMax-nMin)));
        result.set( nMin+x*x*x*(x*(x*6 - 15) + 10) * (nMax-nMin) ) ; // smootherstep
    }

}

v.set(0);
old_min.set(0);
old_max.set(1);
new_min.set(-1);
new_max.set(1);


v.onChange=exec;
old_min.onChange=exec;
old_max.onChange=exec;
new_min.onChange=exec;
new_max.onChange=exec;

result.set(0);

exec();

};

Ops.Math.MapRange.prototype = new CABLES.Op();
CABLES.OPS["2617b407-60a0-4ff6-b4a7-18136cfa7817"]={f:Ops.Math.MapRange,objName:"Ops.Math.MapRange"};




// **************************************************************
// 
// Ops.Math.Clamp
// 
// **************************************************************

Ops.Math.Clamp = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const val=op.inValueFloat("val",0.5);
const min=op.inValueFloat("min",0);
const max=op.inValueFloat("max",1);
const ignore=op.inValueBool("ignore outside values");
const result=op.outValue("result");

val.onChange=min.onChange=max.onChange=clamp;

function clamp()
{
    if(ignore.get())
    {
        if(val.get()>max.get()) return;
        if(val.get()<min.get()) return;
    }
    result.set( Math.min(Math.max(val.get(), min.get()), max.get()));
}



};

Ops.Math.Clamp.prototype = new CABLES.Op();
CABLES.OPS["cda1a98e-5e16-40bd-9b18-a67e9eaad5a1"]={f:Ops.Math.Clamp,objName:"Ops.Math.Clamp"};




// **************************************************************
// 
// Ops.Trigger.TriggerOnce
// 
// **************************************************************

Ops.Trigger.TriggerOnce = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    exe=op.inTriggerButton("Exec"),
    reset=op.inTriggerButton("Reset"),
    next=op.outTrigger("Next");
var outTriggered=op.outValue("Was Triggered");

var triggered=false;

op.toWorkPortsNeedToBeLinked(exe);

reset.onTriggered=function()
{
    triggered=false;
    outTriggered.set(triggered);
};

exe.onTriggered=function()
{
    if(triggered)return;

    triggered=true;
    next.trigger();
    outTriggered.set(triggered);

};

};

Ops.Trigger.TriggerOnce.prototype = new CABLES.Op();
CABLES.OPS["cf3544e4-e392-432b-89fd-fcfb5c974388"]={f:Ops.Trigger.TriggerOnce,objName:"Ops.Trigger.TriggerOnce"};




// **************************************************************
// 
// Ops.Gl.Geometry.TransformGeometry
// 
// **************************************************************

Ops.Gl.Geometry.TransformGeometry = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    geometry=op.inObject("Geometry"),
    transX=op.inValue("Translate X"),
    transY=op.inValue("Translate Y"),
    transZ=op.inValue("Translate Z"),
    scaleX=op.inValueSlider("Scale X",1),
    scaleY=op.inValueSlider("Scale Y",1),
    scaleZ=op.inValueSlider("Scale Z",1),
    rotX=op.inValue("Rotation X"),
    rotY=op.inValue("Rotation Y"),
    rotZ=op.inValue("Rotation Z"),
    outGeom=op.outObject("Result");

transX.onChange=
    transY.onChange=
    transZ.onChange=
    scaleX.onChange=
    scaleY.onChange=
    scaleZ.onChange=
    rotX.onChange=
    rotY.onChange=
    rotZ.onChange=
    geometry.onChange=update;


function update()
{
    var oldGeom=geometry.get();
    var i=0;

    if(oldGeom)
    {
        var geom=oldGeom.copy();
        var rotVec=vec3.create();
        var emptyVec=vec3.create();
        var transVec=vec3.create();
        var centerVec=vec3.create();

        for(i=0;i<geom.vertices.length;i+=3)
        {
            geom.vertices[i+0]*=scaleX.get();
            geom.vertices[i+1]*=scaleY.get();
            geom.vertices[i+2]*=scaleZ.get();

            geom.vertices[i+0]+=transX.get();
            geom.vertices[i+1]+=transY.get();
            geom.vertices[i+2]+=transZ.get();
        }

        for(i=0;i<geom.vertices.length;i+=3)
        {
            vec3.set(rotVec,
                geom.vertices[i+0],
                geom.vertices[i+1],
                geom.vertices[i+2]);

            vec3.rotateX(rotVec,rotVec,transVec,rotX.get()*CGL.DEG2RAD);
            vec3.rotateY(rotVec,rotVec,transVec,rotY.get()*CGL.DEG2RAD);
            vec3.rotateZ(rotVec,rotVec,transVec,rotZ.get()*CGL.DEG2RAD);

            geom.vertices[i+0]=rotVec[0];
            geom.vertices[i+1]=rotVec[1];
            geom.vertices[i+2]=rotVec[2];
        }

        outGeom.set(null);
        outGeom.set(geom);
    }
    else
    {
        outGeom.set(null);
    }
}

};

Ops.Gl.Geometry.TransformGeometry.prototype = new CABLES.Op();
CABLES.OPS["9678fee2-5436-499c-b94d-2603cdbeb380"]={f:Ops.Gl.Geometry.TransformGeometry,objName:"Ops.Gl.Geometry.TransformGeometry"};




// **************************************************************
// 
// Ops.Ui.PatchOutput
// 
// **************************************************************

Ops.Ui.PatchOutput = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};

// empty

};

Ops.Ui.PatchOutput.prototype = new CABLES.Op();
CABLES.OPS["851b44cb-5667-4140-9800-5aeb7031f1d7"]={f:Ops.Ui.PatchOutput,objName:"Ops.Ui.PatchOutput"};




// **************************************************************
// 
// Ops.Gl.Texture
// 
// **************************************************************

Ops.Gl.Texture = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
var filename=op.inFile("file","image");
var tfilter=op.inSwitch("filter",['nearest','linear','mipmap']);
var wrap=op.inValueSelect("wrap",['repeat','mirrored repeat','clamp to edge'],"clamp to edge");
var flip=op.inValueBool("flip",false);
var unpackAlpha=op.inValueBool("unpackPreMultipliedAlpha",false);
var aniso=op.inSwitch("Anisotropic",[0,1,2,4,8,16],0);

var textureOut=op.outTexture("texture");
var width=op.outValue("width");
var height=op.outValue("height");
var loading=op.outValue("loading");
var ratio=op.outValue("Aspect Ratio");

op.setPortGroup("Size",[width,height]);

unpackAlpha.hidePort();

op.toWorkPortsNeedToBeLinked(textureOut);

const cgl=op.patch.cgl;
var cgl_filter=0;
var cgl_wrap=0;
var cgl_aniso=0;

filename.onChange=flip.onChange=function(){reloadSoon();};

aniso.onChange=tfilter.onChange=onFilterChange;
wrap.onChange=onWrapChange;
unpackAlpha.onChange=function(){ reloadSoon(); };

var timedLoader=0;

tfilter.set('mipmap');
wrap.set('repeat');

textureOut.set(CGL.Texture.getEmptyTexture(cgl));

var setTempTexture=function()
{
    var t=CGL.Texture.getTempTexture(cgl);
    textureOut.set(t);
};

var loadingId=null;
var tex=null;
function reloadSoon(nocache)
{
    clearTimeout(timedLoader);
    timedLoader=setTimeout(function()
    {
        realReload(nocache);
    },30);
}

function realReload(nocache)
{
    if(!loadingId)loadingId=cgl.patch.loading.start('textureOp',filename.get());

    var url=op.patch.getFilePath(String(filename.get()));
    if(nocache)url+='?rnd='+CABLES.generateUUID();

    if((filename.get() && filename.get().length>1))
    {
        loading.set(true);




        if(tex)tex.delete();
        tex=CGL.Texture.load(cgl,url,
            function(err)
            {
                if(err)
                {
                    setTempTexture();
                    op.uiAttr({'error':'could not load texture "'+filename.get()+'"'});
                    cgl.patch.loading.finished(loadingId);
                    return;
                }
                else op.uiAttr({'error':null});
                textureOut.set(tex);
                width.set(tex.width);
                height.set(tex.height);
                ratio.set(tex.width/tex.height);

                if(!tex.isPowerOfTwo()) op.uiAttr(
                    {
                        hint:'texture dimensions not power of two! - texture filtering will not work.',
                        warning:null
                    });
                    else op.uiAttr(
                        {
                            hint:null,
                            warning:null
                        });

                textureOut.set(null);
                textureOut.set(tex);
                // tex.printInfo();

            },{
                anisotropic:cgl_aniso,
                wrap:cgl_wrap,
                flip:flip.get(),
                unpackAlpha:unpackAlpha.get(),
                filter:cgl_filter
            });

        textureOut.set(null);
        textureOut.set(tex);

        if(!textureOut.get() && nocache)
        {
        }

        cgl.patch.loading.finished(loadingId);
    }
    else
    {
        cgl.patch.loading.finished(loadingId);
        setTempTexture();
    }
}

function onFilterChange()
{
    if(tfilter.get()=='nearest') cgl_filter=CGL.Texture.FILTER_NEAREST;
    else if(tfilter.get()=='linear') cgl_filter=CGL.Texture.FILTER_LINEAR;
    else if(tfilter.get()=='mipmap') cgl_filter=CGL.Texture.FILTER_MIPMAP;
    else if(tfilter.get()=='Anisotropic') cgl_filter=CGL.Texture.FILTER_ANISOTROPIC;

    cgl_aniso=parseFloat(aniso.get());

    reloadSoon();
}

function onWrapChange()
{
    if(wrap.get()=='repeat') cgl_wrap=CGL.Texture.WRAP_REPEAT;
    if(wrap.get()=='mirrored repeat') cgl_wrap=CGL.Texture.WRAP_MIRRORED_REPEAT;
    if(wrap.get()=='clamp to edge') cgl_wrap=CGL.Texture.WRAP_CLAMP_TO_EDGE;

    reloadSoon();
}

op.onFileChanged=function(fn)
{
    if(filename.get() && filename.get().indexOf(fn)>-1)
    {
        textureOut.set(null);
        textureOut.set(CGL.Texture.getTempTexture(cgl));

        realReload(true);
    }
};







};

Ops.Gl.Texture.prototype = new CABLES.Op();
CABLES.OPS["466394d4-6c1a-4e5d-a057-0063ab0f096a"]={f:Ops.Gl.Texture,objName:"Ops.Gl.Texture"};




// **************************************************************
// 
// Ops.Gl.ShaderEffects.GrassWobble
// 
// **************************************************************

Ops.Gl.ShaderEffects.GrassWobble = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};


var render=op.inTrigger('render');
var trigger=op.outTrigger('trigger');


var frequency=op.inValue("frequency",1);
var mul=op.inValue("mul",0.1);


var cgl=op.patch.cgl;

var uniFrequency=null;
var shader=null;
var module=null;
var uniTime;


var srcHeadVert=''
    .endl()+'UNI float MOD_time;'
    .endl()+'UNI float MOD_frequency;'
    .endl()+'UNI float MOD_mul;'

    .endl();

var srcBodyVert=''

    .endl()+'vec3 MOD_pos=(pos).xyz;'
    .endl()+'float rndSrc=mMatrix[3].z+mMatrix[3].x;'
    
    .endl()+'#ifdef INSTANCING'
    .endl()+'   rndSrc=instMat[3].z+instMat[3].x;'
    .endl()+'#endif'

    .endl()+'float MOD_v=abs(pos.y*MOD_mul)*sin( ((MOD_time*MOD_frequency)+rndSrc) ) ;'

    .endl()+'pos.x+=MOD_v;'
    .endl()+'pos.z+=MOD_v;'


    .endl();




var startTime=CABLES.now()/1000.0;

function removeModule()
{
    if(shader && module)
    {
        shader.removeModule(module);
        shader=null;
    }
}

render.onLinkChanged=removeModule;
render.onTriggered=function()
{
    if(cgl.getShader()!=shader)
    {
        if(shader) removeModule();
        shader=cgl.getShader();
        module=shader.addModule(
            {
                title:op.objName,
                name:'MODULE_VERTEX_POSITION',
                srcHeadVert:srcHeadVert,
                srcBodyVert:srcBodyVert
            });

        uniTime=new CGL.Uniform(shader,'f',module.prefix+'time',0);
        uniFrequency=new CGL.Uniform(shader,'f',module.prefix+'frequency',frequency);
        mul.uniform=new CGL.Uniform(shader,'f',module.prefix+'mul',mul);
    }

    uniTime.setValue(CABLES.now()/1000.0-startTime);
    trigger.trigger();
};


};

Ops.Gl.ShaderEffects.GrassWobble.prototype = new CABLES.Op();
CABLES.OPS["a590b203-2b56-4e25-8f49-12c0f6549972"]={f:Ops.Gl.ShaderEffects.GrassWobble,objName:"Ops.Gl.ShaderEffects.GrassWobble"};




// **************************************************************
// 
// Ops.Gl.SurfaceScatterInstanced_v3
// 
// **************************************************************

Ops.Gl.SurfaceScatterInstanced_v3 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    render=op.inTrigger("Render"),
    inDraw=op.inValueBool("draw",true),
    inNum=op.inValueInt("Num",100),
    inGeomSurface=op.inObject("Geom Surface"),
    inGeom=op.inObject("Geometry"),
    inDistribution=op.inValueSelect("Distribution",['Vertex','Triangle Center','Triangle Side','Random Triangle Point'],'Vertex'),
    inVariety=op.inValueSelect("Selection",["Random","Sequential"],"Random"),
    seed=op.inValueFloat("Random Seed"),
    inSizeMin=op.inValueSlider("Size min",1.0),
    inSizeMax=op.inValueSlider("Size max",1.0),
    inDoLimit=op.inValueBool("Limit",false),
    inLimit=op.inValueInt("Limit Num",0),
    inRotateRandom=op.inValueBool("Random Rotate",true),
    outArrPositions=op.outArray("Positions")
    // outArrRotations=op.outArray("Rotation")
    ;

const cgl=op.patch.cgl;
var mod=null;
var mesh=null;
var shader=null;
var uniDoInstancing=null;
var recalc=true;

var matrixArray=new Float32Array(1);
const m=mat4.create();
var qAxis=vec3.create();

op.setPortGroup("Size",[inSizeMin,inSizeMax]);
op.setPortGroup("Distribution",[inDistribution,inVariety,seed]);

inDistribution.onChange=
    seed.onChange=
    inNum.onChange=
    inRotateRandom.onChange=
    inSizeMin.onChange=
    inSizeMax.onChange=
    inVariety.onChange=
    inGeomSurface.onChange=reset;
render.onTriggered=doRender;
render.onLinkChanged=removeModule;

var arrPositions=[];
var arrRotations=[];




function uniqueIndices(oldCount,newCount,randomize)
{
    function fisherYatesShuffle(array)
    {
        Math.randomSeed=seed.get();
        var i = 0;
        var j = 0;
        var temp = null;

        for (i = array.length - 1; i > 0; i -= 1)
        {
            j = Math.floor(Math.seededRandom()* (i + 1));
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    var arr=[];
    arr.length=newCount;

    if(newCount>oldCount)
    {
        arr.length=newCount;
        for(var i=0;i<newCount;i++) arr[i]=i%(oldCount);
    }
    else
    {
        arr.length=oldCount;
        for(var i=0;i<oldCount;i++) arr[i]=i;
    }

    if(randomize)fisherYatesShuffle(arr);
    return arr;
}


function setup()
{
    // if(!mesh)return;
    op.toWorkPortsNeedToBeLinkedReset();

    if(inDraw.get())
    {
        op.toWorkPortsNeedToBeLinked(inGeom);
    }
    var geom=inGeomSurface.get();
    var num=Math.abs(Math.floor(inNum.get()));
    var m=mat4.create();
    var q=quat.create();
    var vm2=vec3.create();
    var qMat=mat4.create();
    var norm=vec3.create();

    if(!geom) return;

    Math.randomSeed=seed.get();

    const DISTMODE_VERTEX=0;
    const DISTMODE_TRIANGLE_CENTER=1;
    const DISTMODE_TRIANGLE_SIDE=2;
    const DISTMODE_TRIANGLE_RANDOM=3;

    var distMode=0;
    if(inDistribution.get()=='Triangle Center')distMode=DISTMODE_TRIANGLE_CENTER;
    else if(inDistribution.get()=='Triangle Side')distMode=DISTMODE_TRIANGLE_SIDE;
    else if(inDistribution.get()=='Random Triangle Point')distMode=DISTMODE_TRIANGLE_RANDOM;

    if(matrixArray.length!=num*16) matrixArray=new Float32Array(num*16);

    var faces=geom.verticesIndices;
    if(!geom.isIndexed())
    {
        faces=[];
        for(var i=0;i<geom.vertices.length/3;i++) faces[i]=i;
    }

    var indices=uniqueIndices(faces.length/3,num,inVariety.get()=="Random");

    arrRotations.length=arrPositions.length=num*3;


    for(var i=0;i<num;i++)
    {
        var index=indices[i];
        var index3=index*3;

        var px=0;
        var py=0;
        var pz=0;

        mat4.identity(m);

        var nx=geom.vertexNormals[faces[index3]*3+0];
        var ny=geom.vertexNormals[faces[index3]*3+1];
        var nz=geom.vertexNormals[faces[index3]*3+2];

        if(distMode==DISTMODE_VERTEX)
        {
            px=geom.vertices[faces[index3]*3+0];
            py=geom.vertices[faces[index3]*3+1];
            pz=geom.vertices[faces[index3]*3+2];
        }
        else if(distMode==DISTMODE_TRIANGLE_CENTER)
        {
            px=(geom.vertices[faces[index3]*3+0]+geom.vertices[faces[index3+1]*3+0]+geom.vertices[faces[index3+2]*3+0])/3;
            py=(geom.vertices[faces[index3]*3+1]+geom.vertices[faces[index3+1]*3+1]+geom.vertices[faces[index3+2]*3+1])/3;
            pz=(geom.vertices[faces[index3]*3+2]+geom.vertices[faces[index3+1]*3+2]+geom.vertices[faces[index3+2]*3+2])/3;

            nx=(geom.vertexNormals[faces[index3]*3+0]+geom.vertexNormals[faces[index3+1]*3+0]+geom.vertexNormals[faces[index3+2]*3+0])/3;
            ny=(geom.vertexNormals[faces[index3]*3+1]+geom.vertexNormals[faces[index3+1]*3+1]+geom.vertexNormals[faces[index3+2]*3+1])/3;
            nz=(geom.vertexNormals[faces[index3]*3+2]+geom.vertexNormals[faces[index3+1]*3+2]+geom.vertexNormals[faces[index3+2]*3+2])/3;

        }
        else if(distMode==DISTMODE_TRIANGLE_SIDE)
        {
            var which=Math.round(Math.seededRandom()*3.0);
            var whichA=which;
            var whichB=which+1;
            if(whichB>2)whichB=0;

            px=( geom.vertices[faces[index3+whichA]*3+0]+geom.vertices[faces[index3+whichB]*3+0] )/2;
            py=( geom.vertices[faces[index3+whichA]*3+1]+geom.vertices[faces[index3+whichB]*3+1] )/2;
            pz=( geom.vertices[faces[index3+whichA]*3+2]+geom.vertices[faces[index3+whichB]*3+2] )/2;

        }
        else if(distMode==DISTMODE_TRIANGLE_RANDOM)
        {
            var r=Math.seededRandom();
            var p1x=CABLES.map(r,0,1,geom.vertices[(faces[index3+0])*3+0],geom.vertices[(faces[index3+1])*3+0]);
            var p1y=CABLES.map(r,0,1,geom.vertices[(faces[index3+0])*3+1],geom.vertices[(faces[index3+1])*3+1]);
            var p1z=CABLES.map(r,0,1,geom.vertices[(faces[index3+0])*3+2],geom.vertices[(faces[index3+1])*3+2]);

            var n1x=CABLES.map(r,0,1,geom.vertexNormals[(faces[index3+0])*3+0],geom.vertexNormals[(faces[index3+1])*3+0]);
            var n1y=CABLES.map(r,0,1,geom.vertexNormals[(faces[index3+0])*3+1],geom.vertexNormals[(faces[index3+1])*3+1]);
            var n1z=CABLES.map(r,0,1,geom.vertexNormals[(faces[index3+0])*3+2],geom.vertexNormals[(faces[index3+1])*3+2]);

            r=Math.seededRandom();
            var p2x=CABLES.map(r,0,1,geom.vertices[(faces[index3+1])*3+0],geom.vertices[(faces[index3+2])*3+0]);
            var p2y=CABLES.map(r,0,1,geom.vertices[(faces[index3+1])*3+1],geom.vertices[(faces[index3+2])*3+1]);
            var p2z=CABLES.map(r,0,1,geom.vertices[(faces[index3+1])*3+2],geom.vertices[(faces[index3+2])*3+2]);

            var n2x=CABLES.map(r,0,1,geom.vertexNormals[(faces[index3+1])*3+0],geom.vertexNormals[(faces[index3+2])*3+0]);
            var n2y=CABLES.map(r,0,1,geom.vertexNormals[(faces[index3+1])*3+1],geom.vertexNormals[(faces[index3+2])*3+1]);
            var n2z=CABLES.map(r,0,1,geom.vertexNormals[(faces[index3+1])*3+2],geom.vertexNormals[(faces[index3+2])*3+2]);

            r=Math.seededRandom();
            var p3x=CABLES.map(r,0,1,geom.vertices[(faces[index3+2])*3+0],geom.vertices[(faces[index3+0])*3+0]);
            var p3y=CABLES.map(r,0,1,geom.vertices[(faces[index3+2])*3+1],geom.vertices[(faces[index3+0])*3+1]);
            var p3z=CABLES.map(r,0,1,geom.vertices[(faces[index3+2])*3+2],geom.vertices[(faces[index3+0])*3+2]);

            var n3x=CABLES.map(r,0,1,geom.vertexNormals[(faces[index3+2])*3+0],geom.vertexNormals[(faces[index3+0])*3+0]);
            var n3y=CABLES.map(r,0,1,geom.vertexNormals[(faces[index3+2])*3+1],geom.vertexNormals[(faces[index3+0])*3+1]);
            var n3z=CABLES.map(r,0,1,geom.vertexNormals[(faces[index3+2])*3+2],geom.vertexNormals[(faces[index3+0])*3+2]);

            px=(p1x+p2x+p3x)/3;
            py=(p1y+p2y+p3y)/3;
            pz=(p1z+p2z+p3z)/3;

            nx=(n1x+n2x+n3x)/3;
            ny=(n1y+n2y+n3y)/3;
            nz=(n1z+n2z+n3z)/3;
        }

        arrPositions[i*3+0]=px;
        arrPositions[i*3+1]=py;
        arrPositions[i*3+2]=pz;
        mat4.translate(m,m,[px,py,pz]);

        // rotate to normal direction
        vec3.set(norm,nx,ny,nz );
        vec3.set(vm2,1,0,0);
        quat.rotationTo(q,vm2,norm);

        mat4.fromQuat(qMat, q);
        mat4.mul(m,m,qMat);


        // random rotate around up axis
        if(inRotateRandom.get())
        {
            var mr=mat4.create();
            var qbase=quat.create();
            quat.rotateX(qbase,qbase,Math.seededRandom()*360*CGL.DEG2RAD);
            mat4.fromQuat(mr,qbase);
            mat4.mul(m,m,mr);
        }

        // rotate -90 degree
        var mr2=mat4.create();
        var qbase2=quat.create();
        quat.rotateZ(qbase2,qbase2,-90*CGL.DEG2RAD);
        mat4.fromQuat(mr2,qbase2);
        mat4.mul(m,m,mr2);

        // scale
        if(inSizeMin.get()!=1.0 || inSizeMax!=1.0)
        {
            var sc=inSizeMin.get()+ ( Math.seededRandom()*(inSizeMax.get()-inSizeMin.get()) );
            mat4.scale(m,m,[sc,sc,sc]);
        }






        // //quaternion to euler, KINDA works, but not really :/
        // var finalq=q;//quat.create();
        // mat4.getRotation(finalq,m);

        // function clamp(v)
        // {
        //     return Math.min(1,Math.max(-1,v) ) ;
        // }

        // var yaw = Math.atan2(2.0*(finalq[1]*finalq[2] + finalq[3]*finalq[0]), finalq[3]*finalq[3] - finalq[0]*finalq[0] - finalq[1]*finalq[1] + finalq[2]*finalq[2]);
        // var pitch = Math.asin( clamp( -2.0*(finalq[0]*finalq[2] - finalq[3]*finalq[1])));
        // var roll = Math.atan2(2.0*(finalq[0]*finalq[1] + finalq[3]*finalq[2]), finalq[3]*finalq[3] + finalq[0]*finalq[0] - finalq[1]*finalq[1] - finalq[2]*finalq[2]);

        // arrRotations[i*3+0]=360-(pitch*CGL.RAD2DEG);
        // arrRotations[i*3+1]=360-(yaw*CGL.RAD2DEG);
        // arrRotations[i*3+2]=(roll*CGL.RAD2DEG);




        // save
        for(var a=0;a<16;a++)
        {
            matrixArray[i*16+a]=m[a];
        }
    }
    // }
    // else
    // {
    //     console.error("geom is not indexed");
    // }

    if(inDraw.get() && mesh)
    {
        if(op.patch.cgl.glVersion>=2)
        {
            mesh.numInstances=num;
            if(num>0)mesh.addAttribute('instMat',matrixArray,16);
        }
        recalc=false;
    }

    // outArrRotations.set(null);
    // outArrRotations.set(arrRotations);

    outArrPositions.set(null);
    outArrPositions.set(arrPositions);
}


var srcHeadVert=''
    .endl()+'UNI float do_instancing;'
    // .endl()+'UNI float MOD_scale;'

    .endl()+'#ifdef INSTANCING'
    .endl()+'   IN mat4 instMat;'
    .endl()+'   OUT mat4 instModelMat;'
    .endl()+'#endif';

var srcBodyVert=''
    .endl()+'#ifdef INSTANCING'
    .endl()+'   if(do_instancing==1.0)'
    .endl()+'   {'
    .endl()+'       mMatrix*=instMat;'
    .endl()+'   }'
    .endl()+'#endif'
    .endl();



inGeom.onChange=function()
{
    const g=inGeom.get()
    if(!g || !g.vertices)
    {
        mesh=null;
        return;
    }
    mesh=new CGL.Mesh(cgl,g);
    reset();
};

function removeModule()
{
    if(shader && mod)
    {
        shader.removeDefine('INSTANCING');
        shader.removeModule(mod);
        shader=null;
    }
}

function reset()
{
    recalc=true;
}

inDraw.onChange=function()
{
    if(!inDraw.get())
    {
        removeModule();
    }
}

function doRender()
{
    if(recalc)setup();
    if(!mesh) return;
    if(!inGeomSurface.get())return;
    if(!inDraw.get())return;
    if(!inGeom.get())return;


    if(op.patch.cgl.glVersion>=2)
    {
        if(cgl.getShader() && cgl.getShader()!=shader)
        {
            if(shader && mod)
            {
                shader.removeModule(mod);
                shader=null;
            }

            shader=cgl.getShader();
            if(!shader.hasDefine('INSTANCING'))
            {
                mod=shader.addModule(
                    {
                        title:op.objName,
                        name: 'MODULE_VERTEX_POSITION',
                        priority:-2,
                        srcHeadVert: srcHeadVert,
                        srcBodyVert: srcBodyVert
                    });

                shader.define('INSTANCING');
                uniDoInstancing=new CGL.Uniform(shader,'f','do_instancing',0);
            }
            else
            {
                uniDoInstancing=shader.getUniform('do_instancing');
            }
            setup();
        }

        uniDoInstancing.setValue(1);

        var limit=inNum.get();

        if(inDoLimit.get())
        {
            limit=inLimit.get();
            limit=Math.min(limit,inNum.get());
        }



        if(limit>=1)
        {
            mesh.numInstances=limit;
            mesh.render(shader);
        }
        uniDoInstancing.setValue(0);

    }
    else
    {
        // fallback - SLOW // should also use webgl1 extensions...

        for(var i=0;i<matrixArray.length;i+=16)
        {
            op.patch.cgl.pushModelMatrix();

            for(var j=0;j<16;j++)
                m[j]=matrixArray[i+j];

            mat4.multiply(cgl.mMatrix,cgl.mMatrix,m);

            mesh.render(cgl.getShader());
            op.patch.cgl.popModelMatrix();
        }
    }
}

};

Ops.Gl.SurfaceScatterInstanced_v3.prototype = new CABLES.Op();
CABLES.OPS["dbb27405-03b5-4b88-8f63-f9a6c639fc52"]={f:Ops.Gl.SurfaceScatterInstanced_v3,objName:"Ops.Gl.SurfaceScatterInstanced_v3"};




// **************************************************************
// 
// Ops.Anim.SineAnim
// 
// **************************************************************

Ops.Anim.SineAnim = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    exe=op.inTrigger("exe"),
    trigOut = op.outTrigger("Trigger out"),
    result=op.outValue("result"),
    mode =op.inSwitch("Mode",['Sine','Cosine'],'Sine'),
    phase=op.inValueFloat("phase",0),
    mul=op.inValueFloat("frequency",1),
    amplitude=op.inValueFloat("amplitude",1);

var selectIndex = 0;
const SINE = 0;
const COSINE = 1;

op.toWorkPortsNeedToBeLinked(exe);

exec();
onModeChange();

function onModeChange()
{
    var modeSelectValue = mode.get();

    if(modeSelectValue === 'Sine') selectIndex = SINE;
        else if(modeSelectValue === 'Cosine') selectIndex = COSINE;

    op.setUiAttrib({"extendTitle":modeSelectValue});
    exec();
}
function exec()
{
    if(selectIndex == SINE) result.set( amplitude.get() * Math.sin( (op.patch.freeTimer.get()*mul.get()) + phase.get() ));
        else result.set( amplitude.get() * Math.cos( (op.patch.freeTimer.get()*mul.get()) + phase.get() ));
    trigOut.trigger();
}
exe.onTriggered=exec;
mode.onChange=onModeChange;

};

Ops.Anim.SineAnim.prototype = new CABLES.Op();
CABLES.OPS["736d3d0e-c920-449e-ade0-f5ca6018fb5c"]={f:Ops.Anim.SineAnim,objName:"Ops.Anim.SineAnim"};




// **************************************************************
// 
// Ops.Math.Divide
// 
// **************************************************************

Ops.Math.Divide = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    number1 = op.inValueFloat("number1",1),
    number2 = op.inValueFloat("number2",2),
    result = op.outValue("result");

number1.onChange=number2.onChange=exec;
exec();

function exec()
{
    result.set( number1.get() / number2.get() );
}



};

Ops.Math.Divide.prototype = new CABLES.Op();
CABLES.OPS["86fcfd8c-038d-4b91-9820-a08114f6b7eb"]={f:Ops.Math.Divide,objName:"Ops.Math.Divide"};




// **************************************************************
// 
// Ops.Math.TriggerRandomNumber_v2
// 
// **************************************************************

Ops.Math.TriggerRandomNumber_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    exe=op.inTriggerButton('Generate'),
    min=op.inValue("min",0),
    max=op.inValue("max",1),
    outTrig = op.outTrigger("next"),
    result=op.outValue("result"),
    inInteger=op.inValueBool("Integer",false);

exe.onTriggered=genRandom;
max.onChange=genRandom;
min.onChange=genRandom;
inInteger.onChange=genRandom;

op.setPortGroup("Value Range",[min,max]);
genRandom();

function genRandom()
{
    var r=(Math.random()*(max.get()-min.get()))+min.get();
    if(inInteger.get())r=Math.floor((Math.random()*((max.get()-min.get()+1)))+min.get());
    result.set(r);
    outTrig.trigger();
}


};

Ops.Math.TriggerRandomNumber_v2.prototype = new CABLES.Op();
CABLES.OPS["26f446cc-9107-4164-8209-5254487fa132"]={f:Ops.Math.TriggerRandomNumber_v2,objName:"Ops.Math.TriggerRandomNumber_v2"};




// **************************************************************
// 
// Ops.Boolean.IfTrueThen_v2
// 
// **************************************************************

Ops.Boolean.IfTrueThen_v2 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    exe=op.inTrigger("exe"),
    boolean=op.inValueBool("boolean",false),
    triggerThen=op.outTrigger("then"),
    triggerElse=op.outTrigger("else");

exe.onTriggered=exec;

function exec()
{
    if(boolean.get()) triggerThen.trigger();
    else triggerElse.trigger();
}



};

Ops.Boolean.IfTrueThen_v2.prototype = new CABLES.Op();
CABLES.OPS["9549e2ed-a544-4d33-a672-05c7854ccf5d"]={f:Ops.Boolean.IfTrueThen_v2,objName:"Ops.Boolean.IfTrueThen_v2"};




// **************************************************************
// 
// Ops.Math.Compare.LessThan
// 
// **************************************************************

Ops.Math.Compare.LessThan = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const number1 = op.inValue("number1");
const number2 = op.inValue("number2");
const result = op.outValue("result");

number1.onChange=exec;
number2.onChange=exec;
exec();

function exec()
{
    result.set( number1.get() < number2.get() );
}



};

Ops.Math.Compare.LessThan.prototype = new CABLES.Op();
CABLES.OPS["04fd113f-ade1-43fb-99fa-f8825f8814c0"]={f:Ops.Math.Compare.LessThan,objName:"Ops.Math.Compare.LessThan"};




// **************************************************************
// 
// Ops.Gl.Meshes.Cube
// 
// **************************************************************

Ops.Gl.Meshes.Cube = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};

/*
next version:

- make rebuildLater functionality
- make mapping mode for unconnected sides: no more face mapping texture problems (then we don't need that bias...)
- maybe checkboxes to disable some sides ?
- tesselation

*/

const
    render=op.inTrigger('render'),
    width=op.inValue('width',1),
    height=op.inValue('height',1),
    lengt=op.inValue('length',1),
    center=op.inValueBool('center',true),
    active=op.inValueBool('Active',true),
    mapping=op.inSwitch("Mapping",['Default','Cube','Cube Biased'],'Default'),
    trigger=op.outTrigger('trigger'),
    geomOut=op.outObject("geometry");

const cgl=op.patch.cgl;

op.setPortGroup("Geometry",[width,height,lengt]);

var geom=null;
var mesh=null;

mapping.onChange=buildMesh;
width.onChange=buildMesh;
height.onChange=buildMesh;
lengt.onChange=buildMesh;
center.onChange=buildMesh;

buildMesh();


render.onTriggered=function()
{
    if(active.get() && mesh) mesh.render(cgl.getShader());
    trigger.trigger();
};

op.preRender=function()
{
    buildMesh();
    mesh.render(cgl.getShader());
};

function buildMesh()
{
    if(!geom)geom=new CGL.Geometry("cubemesh");
    geom.clear();

    var x=width.get();
    var nx=-1*width.get();
    var y=lengt.get();
    var ny=-1*lengt.get();
    var z=height.get();
    var nz=-1*height.get();

    if(!center.get())
    {
        nx=0;
        ny=0;
        nz=0;
    }
    else
    {
        x*=0.5;
        nx*=0.5;
        y*=0.5;
        ny*=0.5;
        z*=0.5;
        nz*=0.5;
    }

    if(mapping.get()=="Cube" || mapping.get()=="Cube Biased")
        geom.vertices = [
            // Front face
            nx, ny,  z,
            x, ny,  z,
            x,  y,  z,
            nx,  y,  z,
            // Back face
            nx, ny, nz,
            x,  ny, nz,
            x,  y, nz,
            nx, y, nz,
            // Top face
            nx,  y, nz,
            x,  y,  nz,
            x,  y,  z,
            nx,  y, z,
            // Bottom face
            nx, ny, nz,
            x, ny, nz,
            x, ny,  z,
            nx, ny,  z,
            // Right face
            x, ny, nz,
            x, ny, z,
            x,  y, z,
            x, y, nz,
            // zeft face
            nx, ny, nz,
            nx, ny,  z,
            nx,  y,  z,
            nx,  y, nz
            ];

    else
        geom.vertices = [
            // Front face
            nx, ny,  z,
            x, ny,  z,
            x,  y,  z,
            nx,  y,  z,
            // Back face
            nx, ny, nz,
            nx,  y, nz,
            x,  y, nz,
            x, ny, nz,
            // Top face
            nx,  y, nz,
            nx,  y,  z,
            x,  y,  z,
            x,  y, nz,
            // Bottom face
            nx, ny, nz,
            x, ny, nz,
            x, ny,  z,
            nx, ny,  z,
            // Right face
            x, ny, nz,
            x,  y, nz,
            x,  y,  z,
            x, ny,  z,
            // zeft face
            nx, ny, nz,
            nx, ny,  z,
            nx,  y,  z,
            nx,  y, nz
            ];

    if(mapping.get()=="Cube" || mapping.get()=="Cube Biased")
    {
        const sx=0.25;
        const sy=1/3;
        var bias=0.0;
        if(mapping.get()=="Cube Biased")bias=0.01;
        geom.setTexCoords( [
              // Front face   Z+
              sx+bias, sy*2-bias,
              sx*2-bias, sy*2-bias,
              sx*2-bias, sy+bias,
              sx+bias, sy+bias,
              // Back face Z-
              sx*4-bias, sy*2-bias,
              sx*3+bias, sy*2-bias,
              sx*3+bias, sy+bias,
              sx*4-bias, sy+bias,
              // Top face
              sx+bias, 0+bias,
              sx*2-bias, 0+bias,
              sx*2-bias, sy*1-bias,
              sx+bias, sy*1-bias,
              // Bottom face
              sx+bias, sy*2+bias,
              sx*2-bias, sy*2+bias,
              sx*2-bias, sy*3-bias,
              sx+bias, sy*3-bias,
              // Right face
              sx*0+bias, sy+bias,
              sx*1-bias, sy+bias,
              sx*1-bias, sy*2-bias,
              sx*0+bias, sy*2-bias,
              // Left face
              sx*2+bias, sy+bias,
              sx*3-bias, sy+bias,
              sx*3-bias, sy*2-bias,
              sx*2+bias, sy*2-bias,
            ]);

    }

    else
        geom.setTexCoords( [
              // Front face
              0.0, 1.0,
              1.0, 1.0,
              1.0, 0.0,
              0.0, 0.0,
              // Back face
              1.0, 1.0,
              1.0, 0.0,
              0.0, 0.0,
              0.0, 1.0,
              // Top face
              0.0, 0.0,
              0.0, 1.0,
              1.0, 1.0,
              1.0, 0.0,
              // Bottom face
              1.0, 0.0,
              0.0, 0.0,
              0.0, 1.0,
              1.0, 1.0,
              // Right face
              1.0, 1.0,
              1.0, 0.0,
              0.0, 0.0,
              0.0, 1.0,
              // Left face
              0.0, 1.0,
              1.0, 1.0,
              1.0, 0.0,
              0.0, 0.0,
            ]);

    geom.vertexNormals = [
        // Front face
         0.0,  0.0,  1.0,
         0.0,  0.0,  1.0,
         0.0,  0.0,  1.0,
         0.0,  0.0,  1.0,

        // Back face
         0.0,  0.0, -1.0,
         0.0,  0.0, -1.0,
         0.0,  0.0, -1.0,
         0.0,  0.0, -1.0,

        // Top face
         0.0,  1.0,  0.0,
         0.0,  1.0,  0.0,
         0.0,  1.0,  0.0,
         0.0,  1.0,  0.0,

        // Bottom face
         0.0, -1.0,  0.0,
         0.0, -1.0,  0.0,
         0.0, -1.0,  0.0,
         0.0, -1.0,  0.0,

        // Right face
         1.0,  0.0,  0.0,
         1.0,  0.0,  0.0,
         1.0,  0.0,  0.0,
         1.0,  0.0,  0.0,

        // Left face
        -1.0,  0.0,  0.0,
        -1.0,  0.0,  0.0,
        -1.0,  0.0,  0.0,
        -1.0,  0.0,  0.0
    ];
    geom.tangents = [
        // front face
        -1,0,0, -1,0,0, -1,0,0, -1,0,0,
        // back face
        1,0,0, 1,0,0, 1,0,0, 1,0,0,
        // top face
        1,0,0, 1,0,0, 1,0,0, 1,0,0,
        // bottom face
        -1,0,0, -1,0,0, -1,0,0, -1,0,0,
        // right face
        0,0,-1, 0,0,-1, 0,0,-1, 0,0,-1,
        // left face
        0,0,1, 0,0,1, 0,0,1, 0,0,1
    ];
    geom.biTangents = [
        // front face
        0,-1,0, 0,-1,0, 0,-1,0, 0,-1,0,
        // back face
        0,1,0, 0,1,0, 0,1,0, 0,1,0,
        // top face
        0,0,-1, 0,0,-1, 0,0,-1, 0,0,-1,
        // bottom face
        0,0,1, 0,0,1, 0,0,1, 0,0,1,
        // right face
        0,1,0, 0,1,0, 0,1,0, 0,1,0,
        // left face
        0,1,0, 0,1,0, 0,1,0, 0,1,0
    ];

    geom.verticesIndices = [
        0, 1, 2,      0, 2, 3,    // Front face
        4, 5, 6,      4, 6, 7,    // Back face
        8, 9, 10,     8, 10, 11,  // Top face
        12, 13, 14,   12, 14, 15, // Bottom face
        16, 17, 18,   16, 18, 19, // Right face
        20, 21, 22,   20, 22, 23  // Left face
    ];

    if(mesh)mesh.dispose();
    mesh=new CGL.Mesh(cgl,geom);
    geomOut.set(null);
    geomOut.set(geom);
}


op.onDelete=function()
{
    if(mesh)mesh.dispose();
};



};

Ops.Gl.Meshes.Cube.prototype = new CABLES.Op();
CABLES.OPS["ff0535e2-603a-4c07-9ce6-e9e0db857dfe"]={f:Ops.Gl.Meshes.Cube,objName:"Ops.Gl.Meshes.Cube"};




// **************************************************************
// 
// Ops.User.julianstein.wasdnav
// 
// **************************************************************

Ops.User.julianstein.wasdnav = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
op.requirements=[CABLES.Requirements.POINTERLOCK];
var render=op.inTrigger('render');
var trigger=op.outTrigger('trigger');

var isLocked=op.outValue("isLocked",false);

var vPos=vec3.create();
var speedx=0,speedy=0,speedz=0;
var movementSpeedFactor = 0.5;

var fly=op.inValueBool("Allow Flying",true);
var moveSpeed = op.inFloat("Speed",1);

var outPosX=op.outValue("posX");
var outPosY=op.outValue("posY");
var outPosZ=op.outValue("posZ");

var outMouseDown=op.outTrigger("Mouse Left");
var outMouseDownRight=op.outTrigger("Mouse Right");

var outDirX=op.outValue("Dir X");
var outDirY=op.outValue("Dir Y");
var outDirZ=op.outValue("Dir Z");

const DEG2RAD=3.14159/180.0;

var rotX=0;
var rotY=0;

var posX=0;
var posY=0;
var posZ=0;

var cgl=op.patch.cgl;

var viewMatrix = mat4.create();

render.onTriggered=function()
{
    calcCameraMovement();
    move();

    if(!fly.get())posY=0.0;

    if(speedx!==0.0 || speedy!==0.0 || speedz!==0)
    {
        outPosX.set(posX);
        outPosY.set(posY);
        outPosZ.set(posZ);
    }

    cgl.pushViewMatrix();

    vec3.set(vPos, -posX,-posY,-posZ);

    mat4.identity(cgl.vMatrix);

    mat4.rotateX( cgl.vMatrix ,cgl.vMatrix,DEG2RAD*rotX);
    mat4.rotateY( cgl.vMatrix ,cgl.vMatrix,DEG2RAD*rotY);

    mat4.translate( cgl.vMatrix ,cgl.vMatrix,vPos);

    trigger.trigger();
    cgl.popViewMatrix();

    // for dir vec
    mat4.identity(viewMatrix);
    mat4.rotateX( viewMatrix ,viewMatrix,DEG2RAD*rotX);
    mat4.rotateY( viewMatrix ,viewMatrix,DEG2RAD*rotY);
    mat4.transpose(viewMatrix,viewMatrix);

    var dir=vec4.create();
    vec4.transformMat4(dir,[0,0,1,1],viewMatrix);

    vec4.normalize(dir,dir);
    outDirX.set(-dir[0]);
    outDirY.set(-dir[1]);
    outDirZ.set(-dir[2]);

};

//--------------

function calcCameraMovement()
{
    var camMovementXComponent = 0.0,
        camMovementYComponent = 0.0,
        camMovementZComponent = 0.0,
        pitchFactor=0,
        yawFactor=0;

    if (pressedW)
    {
        // Control X-Axis movement
        pitchFactor = Math.cos(DEG2RAD*rotX);

        camMovementXComponent += ( movementSpeedFactor * (Math.sin(DEG2RAD*rotY)) ) * pitchFactor;

        // Control Y-Axis movement
        camMovementYComponent += movementSpeedFactor * (Math.sin(DEG2RAD*rotX)) * -1.0;

        // Control Z-Axis movement
        yawFactor = (Math.cos(DEG2RAD*rotX));
        camMovementZComponent += ( movementSpeedFactor * (Math.cos(DEG2RAD*rotY)) * -1.0 ) * yawFactor;
    }

    if (pressedS)
    {
        // Control X-Axis movement
        pitchFactor = Math.cos(DEG2RAD*rotX);
        camMovementXComponent += ( movementSpeedFactor * (Math.sin(DEG2RAD*rotY)) * -1.0) * pitchFactor;

        // Control Y-Axis movement
        camMovementYComponent += movementSpeedFactor * (Math.sin(DEG2RAD*rotX));

        // Control Z-Axis movement
        yawFactor = (Math.cos(DEG2RAD*rotX));
        camMovementZComponent += ( movementSpeedFactor * (Math.cos(DEG2RAD*rotY)) ) * yawFactor;
    }

    if (pressedA)
    {
        // Calculate our Y-Axis rotation in radians once here because we use it twice
        var yRotRad = DEG2RAD*rotY;

        camMovementXComponent += -movementSpeedFactor * (Math.cos(yRotRad));
        camMovementZComponent += -movementSpeedFactor * (Math.sin(yRotRad));
    }

    if (pressedD)
    {
        // Calculate our Y-Axis rotation in radians once here because we use it twice
        var yRotRad = DEG2RAD*rotY;

        camMovementXComponent += movementSpeedFactor * (Math.cos(yRotRad));
        camMovementZComponent += movementSpeedFactor * (Math.sin(yRotRad));
    }

var mulSpeed=0.016;


    speedx = camMovementXComponent*mulSpeed;
    speedy = camMovementYComponent*mulSpeed;
    speedz = camMovementZComponent*mulSpeed;




    if (speedx > movementSpeedFactor) speedx = movementSpeedFactor;
    if (speedx < -movementSpeedFactor) speedx = -movementSpeedFactor;

    if (speedy > movementSpeedFactor) speedy = movementSpeedFactor;
    if (speedy < -movementSpeedFactor) speedy = -movementSpeedFactor;

    if (speedz > movementSpeedFactor) speedz = movementSpeedFactor;
    if (speedz < -movementSpeedFactor) speedz = -movementSpeedFactor;
}

function moveCallback(e)
{
    var mouseSensitivity=0.05;
    rotX+=e.movementY*mouseSensitivity;
    rotY+=e.movementX*mouseSensitivity;

    if (rotX < -90.0) rotX= -90.0;
    if (rotX > 90.0) rotX=90.0;
    if (rotY < -180.0) rotY= rotY+ 360.0;
    if (rotY > 180.0) rotY= rotY - 360.0;
}

var canvas = document.getElementById("glcanvas");

function mouseDown(e)
{
    if(e.which==3) outMouseDownRight.trigger();
        else outMouseDown.trigger();

}


function lockChangeCallback(e)
{
    if (document.pointerLockElement === canvas ||
            document.mozPointerLockElement === canvas ||
            document.webkitPointerLockElement === canvas)
    {
        document.addEventListener("mousedown", mouseDown, false);
        document.addEventListener("mousemove", moveCallback, false);
        document.addEventListener("keydown", keyDown, false);
        document.addEventListener("keyup", keyUp, false);
        isLocked.set(true);

    }
    else
    {
        document.removeEventListener("mousedown", mouseDown, false);
        document.removeEventListener("mousemove", moveCallback, false);
        document.removeEventListener("keydown", keyDown, false);
        document.removeEventListener("keyup", keyUp, false);
        isLocked.set(false);
        pressedW=false;
        pressedA=false;
        pressedS=false;
        pressedD=false;
    }
}

document.addEventListener('pointerlockchange', lockChangeCallback, false);
document.addEventListener('mozpointerlockchange', lockChangeCallback, false);
document.addEventListener('webkitpointerlockchange', lockChangeCallback, false);

document.getElementById('glcanvas').addEventListener('mousedown',function()
{
    document.addEventListener("mousemove", moveCallback, false);
    canvas.requestPointerLock = canvas.requestPointerLock ||
                                canvas.mozRequestPointerLock ||
                                canvas.webkitRequestPointerLock;
    canvas.requestPointerLock();

});

var lastMove=0;
function move()
{
    var timeOffset = window.performance.now()-lastMove;
    timeOffset *= .5* moveSpeed.get();
    posX=posX+speedx * timeOffset;
    posY=posY+speedy * timeOffset;
    posZ=posZ+speedz * timeOffset;


    lastMove = window.performance.now();
}

var pressedW=false;
var pressedA=false;
var pressedS=false;
var pressedD=false;

function keyDown(e)
{
    switch(e.which)
    {
        case 87:
            pressedW=true;
        break;
        case 38:
            pressedW=true;
        break;
        case 65:
            pressedA=true;
        break;
        case 37:
            pressedA=true;
        break;
        case 83:
            pressedS=true;
        break;
        case 40:
            pressedS=true;
        break;
        case 68:
            pressedD=true;
        break;
        case 39:
            pressedD=true;
        break;

        default:

        break;
    }
}

function keyUp(e)
{
    switch(e.which)
     {
        case 87:
            pressedW=false;
        break;
        case 38:
            pressedW=false;
        break;
        case 65:
            pressedA=false;
        break;
        case 37:
            pressedA=false;
        break;
        case 83:
            pressedS=false;
        break;
        case 40:
            pressedS=false;
        break;
        case 68:
            pressedD=false;
        break;
        case 39:
            pressedD=false;
        break;

        default:

        break;
    }
}

};

Ops.User.julianstein.wasdnav.prototype = new CABLES.Op();





// **************************************************************
// 
// Ops.Gl.TextureEffects.ImageCompose
// 
// **************************************************************

Ops.Gl.TextureEffects.ImageCompose = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const render=op.inTrigger("render");
// const useVPSize=op.addInPort(new CABLES.Port(op,"use viewport size",CABLES.OP_PORT_TYPE_VALUE,{ display:'bool' }));
const useVPSize=op.inBool("use viewport size");
const width=op.inValueInt("width");
const height=op.inValueInt("height");

const tfilter=op.inSwitch("filter",['nearest','linear','mipmap'],"linear");
const twrap=op.inValueSelect("wrap",['clamp to edge','repeat','mirrored repeat']);
const fpTexture=op.inValueBool("HDR");

const trigger=op.outTrigger("trigger");
const texOut=op.outTexture("texture_out");

const bgAlpha=op.inValueSlider("Background Alpha",1);
const outRatio=op.outValue("Aspect Ratio");

op.setPortGroup("Texture Size",[useVPSize,width,height]);
op.setPortGroup("Texture Settings",[twrap,tfilter,fpTexture]);



const cgl=op.patch.cgl;
texOut.set(CGL.Texture.getEmptyTexture(cgl));
var effect=null;
var tex=null;

var w=8,h=8;
var prevViewPort=[0,0,0,0];
var reInitEffect=true;

var bgFrag=''
    .endl()+'uniform float a;'
    .endl()+'void main()'
    .endl()+'{'
    .endl()+'   outColor= vec4(0.0,0.0,0.0,a);'
    .endl()+'}';
var bgShader=new CGL.Shader(cgl,'imgcompose bg');
bgShader.setSource(bgShader.getDefaultVertexShader(),bgFrag);
var uniBgAlpha=new CGL.Uniform(bgShader,'f','a',bgAlpha);

var selectedFilter=CGL.Texture.FILTER_LINEAR;
var selectedWrap=CGL.Texture.WRAP_CLAMP_TO_EDGE;

function initEffect()
{
    if(effect)effect.delete();
    if(tex)tex.delete();

    effect=new CGL.TextureEffect(cgl,{"isFloatingPointTexture":fpTexture.get()});

    tex=new CGL.Texture(cgl,
        {
            "name":"image compose",
            "isFloatingPointTexture":fpTexture.get(),
            "filter":selectedFilter,
            "wrap":selectedWrap,
            "width": Math.ceil(width.get()),
            "height": Math.ceil(height.get()),
        });

    effect.setSourceTexture(tex);
    texOut.set(CGL.Texture.getEmptyTexture(cgl));
    // texOut.set(effect.getCurrentSourceTexture());

    // texOut.set(effect.getCurrentSourceTexture());

    reInitEffect=false;

    // op.log("reinit effect");
    // tex.printInfo();
}

fpTexture.onChange=function()
{
    reInitEffect=true;

    // var e1=cgl.gl.getExtension('EXT_color_buffer_float');
    // var e2=cgl.gl.getExtension('EXT_float_blend');

};

function updateResolution()
{
    if(!effect)initEffect();

    if(useVPSize.get())
    {
        w=cgl.getViewPort()[2];
        h=cgl.getViewPort()[3];
    }
    else
    {
        w=Math.ceil(width.get());
        h=Math.ceil(height.get());
    }

    if((w!=tex.width || h!= tex.height) && (w!==0 && h!==0))
    {
        height.set(h);
        width.set(w);
        tex.setSize(w,h);
        outRatio.set(w/h);
        effect.setSourceTexture(tex);
        // texOut.set(null);
        texOut.set(CGL.Texture.getEmptyTexture(cgl));
        texOut.set(tex);
    }

    if(texOut.get())
        if(!texOut.get().isPowerOfTwo() )
        {
            if(!op.uiAttribs.hint)
                op.uiAttr(
                    {
                        hint:'texture dimensions not power of two! - texture filtering will not work.',
                        warning:null
                    });
        }
        else
        if(op.uiAttribs.hint)
        {
            op.uiAttr({hint:null,warning:null}); //todo only when needed...
        }

}


function updateSizePorts()
{
    if(useVPSize.get())
    {
        width.setUiAttribs({greyout:true});
        height.setUiAttribs({greyout:true});
    }
    else
    {
        width.setUiAttribs({greyout:false});
        height.setUiAttribs({greyout:false});
    }
}


useVPSize.onChange=function()
{
    updateSizePorts();
    if(useVPSize.get())
    {
        width.onChange=null;
        height.onChange=null;
    }
    else
    {
        width.onChange=updateResolution;
        height.onChange=updateResolution;
    }
    updateResolution();

};


op.preRender=function()
{
    doRender();
    bgShader.bind();
};


var doRender=function()
{
    if(!effect || reInitEffect)
    {
        initEffect();
    }
    var vp=cgl.getViewPort();
    prevViewPort[0]=vp[0];
    prevViewPort[1]=vp[1];
    prevViewPort[2]=vp[2];
    prevViewPort[3]=vp[3];

    cgl.gl.blendFunc(cgl.gl.SRC_ALPHA, cgl.gl.ONE_MINUS_SRC_ALPHA);

    updateResolution();

    cgl.currentTextureEffect=effect;
    effect.setSourceTexture(tex);

    effect.startEffect();

    // render background color...
    cgl.pushShader(bgShader);
    cgl.currentTextureEffect.bind();
    cgl.setTexture(0, cgl.currentTextureEffect.getCurrentSourceTexture().tex );
    cgl.currentTextureEffect.finish();
    cgl.popShader();

    trigger.trigger();

    texOut.set(effect.getCurrentSourceTexture());
    // texOut.set(effect.getCurrentTargetTexture());


    // if(effect.getCurrentSourceTexture.filter==CGL.Texture.FILTER_MIPMAP)
    // {
    //         this._cgl.gl.bindTexture(this._cgl.gl.TEXTURE_2D, effect.getCurrentSourceTexture.tex);
    //         effect.getCurrentSourceTexture.updateMipMap();
    //     // else
    //     // {
    //     //     this._cgl.gl.bindTexture(this._cgl.gl.TEXTURE_2D, this._textureSource.tex);;
    //     //     this._textureSource.updateMipMap();
    //     // }

    //     this._cgl.gl.bindTexture(this._cgl.gl.TEXTURE_2D, null);
    // }

    effect.endEffect();

    cgl.setViewPort(prevViewPort[0],prevViewPort[1],prevViewPort[2],prevViewPort[3]);


    cgl.gl.blendFunc(cgl.gl.SRC_ALPHA,cgl.gl.ONE_MINUS_SRC_ALPHA);

    cgl.currentTextureEffect=null;
};


function onWrapChange()
{
    if(twrap.get()=='repeat') selectedWrap=CGL.Texture.WRAP_REPEAT;
    if(twrap.get()=='mirrored repeat') selectedWrap=CGL.Texture.WRAP_MIRRORED_REPEAT;
    if(twrap.get()=='clamp to edge') selectedWrap=CGL.Texture.WRAP_CLAMP_TO_EDGE;

    reInitEffect=true;
    updateResolution();
}

twrap.set('repeat');
twrap.onChange=onWrapChange;


function onFilterChange()
{
    if(tfilter.get()=='nearest') selectedFilter=CGL.Texture.FILTER_NEAREST;
    if(tfilter.get()=='linear')  selectedFilter=CGL.Texture.FILTER_LINEAR;
    if(tfilter.get()=='mipmap')  selectedFilter=CGL.Texture.FILTER_MIPMAP;

    reInitEffect=true;
    updateResolution();
    // effect.setSourceTexture(tex);
    // updateResolution();
}

tfilter.set('linear');
tfilter.onChange=onFilterChange;

useVPSize.set(true);
render.onTriggered=doRender;
op.preRender=doRender;


width.set(640);
height.set(360);
onFilterChange();
onWrapChange();
updateSizePorts();

};

Ops.Gl.TextureEffects.ImageCompose.prototype = new CABLES.Op();
CABLES.OPS["5c04608d-1e42-4e36-be00-1be4a81fc309"]={f:Ops.Gl.TextureEffects.ImageCompose,objName:"Ops.Gl.TextureEffects.ImageCompose"};




// **************************************************************
// 
// Ops.Gl.Textures.VideoTexture
// 
// **************************************************************

Ops.Gl.Textures.VideoTexture = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
var filename=op.inFile("file","video");
var play=op.inValueBool("play");
var loop=op.inValueBool("loop");
var autoPlay = op.inValueBool('auto play', false);

var volume=op.inValueSlider("Volume");
var muted = op.inValueBool('mute', true);
var speed=op.inValueFloat("speed",1);

var tfilter=op.inValueSelect("filter",['nearest','linear','mipmap']);
var wrap=op.inValueSelect("wrap",['repeat','mirrored repeat','clamp to edge'],"clamp to edge");

var flip=op.inValueBool("flip",true);
var fps=op.inValueFloat("fps",25);
var time=op.inValueFloat("set time");
var rewind=op.inTriggerButton("rewind");

var inPreload=op.inValueBool("Preload",true);

var textureOut=op.outTexture("texture");
var outDuration=op.outValue("duration");
var outProgress=op.outValue("progress");
var outTime=op.outValue("CurrentTime");
var loading=op.outValue("Loading");
var canPlayThrough = op.outValueBool('Can Play Through', false);

var videoElementPlaying=false;
var embedded=false;
var cgl=op.patch.cgl;
var videoElement=document.createElement('video');
videoElement.setAttribute('playsinline', '');
videoElement.setAttribute('webkit-playsinline', '');
var intervalID=null;
fps.set(25);
volume.set(1);

var cgl_filter=0;
var cgl_wrap=0;

var emptyTexture=CGL.Texture.getEmptyTexture(cgl);

var tex=null;
textureOut.set(tex);
var timeout=null;
var firstTime=true;
textureOut.set(CGL.Texture.getEmptyTexture(cgl));

function reInitTexture()
{
    if(tex)tex.delete();
    tex=new CGL.Texture(cgl,
    {
        wrap:cgl_wrap,
        filter:cgl_filter
    });

}

autoPlay.onChange = function() {
    if(videoElement) {
        if(autoPlay.get()) {
            videoElement.setAttribute('autoplay', '');
        } else {
            videoElement.removeAttribute('autoplay');
        }
    }
};

rewind.onTriggered=function()
{
    videoElement.currentTime=0;
    textureOut.set(emptyTexture);
    // updateTexture();

};

time.onChange=function()
{
    videoElement.currentTime=time.get() || 0;
    updateTexture();
};

fps.onChange=function()
{
    if(fps.get()<0.1)fps.set(1);
    clearTimeout(timeout);
    timeout=setTimeout(updateTexture, 1000/fps.get());
};

play.onChange=function()
{

    if(!embedded)
    {
        embedVideo(true);
    }

    if(play.get())
    {
        videoElement.currentTime=time.get() || 0;

        videoElement.play();
        updateTexture();
        videoElement.playbackRate = speed.get();
    }
    else videoElement.pause();
};

speed.onChange = function() {
    videoElement.playbackRate = speed.get();
};

loop.onChange=function()
{
    videoElement.loop = loop.get();
};

muted.onChange=function()
{
    videoElement.muted = muted.get();
};

tfilter.onChange=function()
{
    if(tfilter.get()=='nearest') cgl_filter=CGL.Texture.FILTER_NEAREST;
    if(tfilter.get()=='linear') cgl_filter=CGL.Texture.FILTER_LINEAR;
    if(tfilter.get()=='mipmap') cgl_filter=CGL.Texture.FILTER_MIPMAP;
    tex=null;
};

wrap.onChange=function()
{
    if(wrap.get()=='repeat') cgl_wrap=CGL.Texture.WRAP_REPEAT;
    if(wrap.get()=='mirrored repeat') cgl_wrap=CGL.Texture.WRAP_MIRRORED_REPEAT;
    if(wrap.get()=='clamp to edge') cgl_wrap=CGL.Texture.WRAP_CLAMP_TO_EDGE;
    tex=null;
};

function updateTexture()
{

    if(play.get())
    {
        clearTimeout(timeout);
        timeout=setTimeout( updateTexture, 1000/fps.get() );
    }
    else
    {
        return;
    }

    if(!tex)reInitTexture();
    if(!videoElementPlaying)return;

    tex.height=videoElement.videoHeight;
    tex.width=videoElement.videoWidth;

    var perc=(videoElement.currentTime)/videoElement.duration;
    if(!isNaN(perc)) outProgress.set(perc);
    outTime.set(videoElement.currentTime);

    cgl.gl.bindTexture(cgl.gl.TEXTURE_2D, tex.tex);

    if(firstTime)
    {
        cgl.gl.texImage2D(cgl.gl.TEXTURE_2D, 0, cgl.gl.RGBA, cgl.gl.RGBA, cgl.gl.UNSIGNED_BYTE, videoElement);
        cgl.gl.pixelStorei(cgl.gl.UNPACK_FLIP_Y_WEBGL, flip.get());
        tex._setFilter();
    }
    else
    {
        cgl.gl.pixelStorei(cgl.gl.UNPACK_FLIP_Y_WEBGL, flip.get());
        cgl.gl.texSubImage2D( cgl.gl.TEXTURE_2D, 0, 0, 0, cgl.gl.RGBA, cgl.gl.UNSIGNED_BYTE, videoElement);
    }

    firstTime=false;

    textureOut.set(tex);

    CGL.profileData.profileVideosPlaying++;


    if(videoElement.readyState==4) loading.set(false);
        else loading.set(false);
}

function initVideo()
{
    videoElement.controls = false;
    videoElement.muted = muted.get();
    videoElement.loop = loop.get();
    if(play.get()) videoElement.play()
    updateTexture();
    canPlayThrough.set(true);
}

function updateVolume()
{
    videoElement.volume=(volume.get() || 0)*op.patch.config.masterVolume;
}

volume.onChange=updateVolume;
op.onMasterVolumeChanged=updateVolume;


function loadedMetaData()
{
    outDuration.set(videoElement.duration);

    // console.log('loaded metadata...');
    // console.log('length ',videoElement.buffered.length);
    // console.log('duration ',videoElement.duration);
    // console.log('bytesTotal ',videoElement.bytesTotal);
    // console.log('bufferedBytes ',videoElement.bufferedBytes);
    // console.log('buffered ',videoElement.buffered);
}


var addedListeners=false;

function embedVideo(force)
{
    canPlayThrough.set(false);
    if(filename.get()!=0 && filename.get().length>1)
    if(inPreload.get()||force)
    {
        // console.log("embedVideo"+filename.get() );
        clearTimeout(timeout);
        loading.set(true);
        videoElement.preload = 'true';
        var url=op.patch.getFilePath(filename.get());
        videoElement.setAttribute('src',url);
        videoElement.setAttribute('crossOrigin','anonymous');
        videoElement.playbackRate = speed.get();
        if(!addedListeners)
        {
            addedListeners=true;
            videoElement.addEventListener("canplaythrough", initVideo, true);
            videoElement.addEventListener('loadedmetadata', loadedMetaData );
            videoElement.addEventListener("playing", function() {videoElementPlaying = true;}, true);
        }
        embedded=true
    }
}


function loadVideo()
{
    setTimeout(embedVideo,100);

}

function reload()
{
    if(!filename.get())return;
    loadVideo();
}

filename.onChange=reload;

};

Ops.Gl.Textures.VideoTexture.prototype = new CABLES.Op();
CABLES.OPS["569af220-e7c5-4dde-a5d9-b63cebdc8bfd"]={f:Ops.Gl.Textures.VideoTexture,objName:"Ops.Gl.Textures.VideoTexture"};




// **************************************************************
// 
// Ops.User.julianstein.sidebarsmall
// 
// **************************************************************

Ops.User.julianstein.sidebarsmall = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    inExec=op.inTrigger("Trigger"),
    inIfSuspended=op.inValueBool("Only if Audio Suspended"),
    inReset=op.inTriggerButton("Reset"),
    outNext=op.outTrigger("Next"),
    outState=op.outString("Audiocontext State"),
    outClicked=op.outValueBool("Clicked");

op.toWorkPortsNeedToBeLinked(inExec);

const canvas = op.patch.cgl.canvas.parentElement;
var wasClicked=false;
var ele=null;
var elePlay=null;
//createElements();

function createElements()
{
    if(elePlay) elePlay.remove();
    if(ele) ele.remove();

    ele=document.createElement("div");
    elePlay=document.createElement("div");


    ele.style.width="100px";
    ele.style.height="100px";
    ele.style.left="50%";
    ele.style.top="50%";
    ele.style['border-radius']="50px";
    ele.style['margin-left']="-50px";
    ele.style['margin-top']="-50px";
    ele.style.position="absolute";
    ele.style.cursor="pointer";
    ele.style.opacity=0.7;
    ele.style['z-index']=999999;
    ele.style['background-color']="rgba(55,55,55)";

    elePlay.style["border-style"]="solid";
    elePlay.style["border-color"]="transparent transparent transparent #ccc";
    elePlay.style["box-sizing"]="border-box";
    elePlay.style.width="50px";
    elePlay.style.height="50px";
    elePlay.style['margin-top']="15px";
    elePlay.style['margin-left']="33px";
    elePlay.style["border-width"]="35px 0px 35px 50px";
    elePlay.style['pointer-events']="none";

    canvas.appendChild(ele);
    ele.appendChild(elePlay);
    ele.addEventListener('mouseenter', hover);
    ele.addEventListener('mouseleave', hoverOut);
    ele.addEventListener('click', clicked);
    ele.addEventListener('touchStart', clicked);
    op.onDelete=removeElements;
}

inReset.onTriggered=function()
{
    createElements();
    wasClicked=false;
    outClicked.set(wasClicked);

};

inExec.onTriggered=function()
{
    if(window.audioContext)
    {
        outState.set(window.audioContext.state);
    }

    if(inIfSuspended.get() && window.audioContext.state=='running') clicked();
    if(wasClicked) outNext.trigger();

};

function clicked()
{
    removeElements();
    if(window.audioContext && window.audioContext.state=='suspended')window.audioContext.resume();
    wasClicked=true;
    outClicked.set(wasClicked);
}

function removeElements()
{
    if(elePlay) elePlay.remove();
    if(ele) ele.remove();
}

function hoverOut()
{
    if(ele) ele.style.opacity=0.7;
}

function hover()
{
    if(ele) ele.style.opacity=1.0;
}

};

Ops.User.julianstein.sidebarsmall.prototype = new CABLES.Op();





// **************************************************************
// 
// Ops.Gl.Meshes.TextMesh
// 
// **************************************************************

Ops.Gl.Meshes.TextMesh = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={textmesh_frag:"UNI sampler2D tex;\nIN vec2 texCoord;\nUNI float r;\nUNI float g;\nUNI float b;\nUNI float a;\n\nvoid main()\n{\n   vec4 col=texture(tex,texCoord);\n   col.a=col.r;\n   col.r*=r;\n   col.g*=g;\n   col.b*=b;\n   col*=a;\n\n   outColor=col;\n}",textmesh_vert:"UNI sampler2D tex;\nUNI mat4 projMatrix;\nUNI mat4 modelMatrix;\nUNI mat4 viewMatrix;\nUNI float scale;\nIN vec3 vPosition;\nIN vec2 attrTexCoord;\nIN mat4 instMat;\nIN vec2 attrTexOffsets;\nIN vec2 attrTexSize;\n\nOUT vec2 texCoord;\n\nvoid main()\n{\n   texCoord=(attrTexCoord*(attrTexSize)) + attrTexOffsets;\n   mat4 instModelMat=instMat;\n   instModelMat[3][0]*=scale;\n\n   vec4 vert=vec4( vPosition.x*(attrTexSize.x/attrTexSize.y)*scale,vPosition.y*scale,vPosition.z*scale, 1. );\n\n   mat4 mvMatrix=viewMatrix * modelMatrix * instModelMat;\n\n   #ifndef BILLBOARD\n       gl_Position = projMatrix * mvMatrix * vert;\n   #endif\n}\n",};
var render=op.inTrigger("Render");
var next=op.outTrigger("Next");
var textureOut=op.outTexture("texture");
var str=op.inValueString("Text","cables");
var scale=op.inValue("Scale",1);
var inFont=op.inValueString("Font","Arial");
var align=op.inValueSelect("align",['left','center','right'],'center');
var valign=op.inValueSelect("vertical align",['Top','Middle','Bottom'],'Middle');
var lineHeight=op.inValue("Line Height",1);
var letterSpace=op.inValue("Letter Spacing");

var loaded=op.outValue("Font Available",0);

var cgl=op.patch.cgl;

var textureSize=2048;
var fontLoaded=false;

const BIASX=17;

align.onChange=generateMesh;
str.onChange=generateMesh;

lineHeight.onChange=generateMesh;
var cgl=op.patch.cgl;
var geom=null;
var mesh=null;

var createMesh=true;
var createTexture=true;

textureOut.set(null);
inFont.onChange=function()
    {
        createTexture=true;
        createMesh=true;
        checkFont();
    };

function checkFont()
{
    var oldFontLoaded=fontLoaded;
    try
    {
    fontLoaded=document.fonts.check('20px '+inFont.get());
    }
    catch(ex)
    {
        console.log(ex);
    }

    if(!oldFontLoaded && fontLoaded)
    {
        loaded.set(true);
        createTexture=true;
        createMesh=true;
    }

    if(!fontLoaded) setTimeout(checkFont,250);
}

var canvasid=null;


CABLES.OpTextureMeshCanvas={};

var valignMode=0;

valign.onChange=function()
{
    if(valign.get()=='Middle')valignMode=0;
    if(valign.get()=='Top')valignMode=1;
    if(valign.get()=='Bottom')valignMode=2;
};

function getFont()
{
    canvasid=''+inFont.get();
    if(CABLES.OpTextureMeshCanvas.hasOwnProperty(canvasid))
    {
        return CABLES.OpTextureMeshCanvas[canvasid];
    }

    var fontImage = document.createElement('canvas');
    fontImage.dataset.font=inFont.get();
    fontImage.id = "texturetext_"+CABLES.generateUUID();
    fontImage.style.display = "none";
    var body = document.getElementsByTagName("body")[0];
    body.appendChild(fontImage);
    var _ctx= fontImage.getContext('2d');
    CABLES.OpTextureMeshCanvas[canvasid]=
        {
            ctx:_ctx,
            canvas:fontImage,
            chars:{},
            characters:'?',
            fontSize:320
        };
    return CABLES.OpTextureMeshCanvas[canvasid];
}

op.onDelete=function()
{
    // fontImage.remove();
    if(canvasid && CABLES.OpTextureMeshCanvas[canvasid])
        CABLES.OpTextureMeshCanvas[canvasid].canvas.remove();
};

var shader=new CGL.Shader(cgl,'TextMesh');
shader.setSource(attachments.textmesh_vert,attachments.textmesh_frag);
var uniTex=new CGL.Uniform(shader,'t','tex',0);
var uniScale=new CGL.Uniform(shader,'f','scale',scale);


const r = op.inValueSlider("r", 1);
const g = op.inValueSlider("g", 1);
const b = op.inValueSlider("b", 1);
r.setUiAttribs({ colorPick: true });

const runiform=new CGL.Uniform(shader,'f','r',r);
const guniform=new CGL.Uniform(shader,'f','g',g);
const buniform=new CGL.Uniform(shader,'f','b',b);

var a=op.inValueSlider("a");
a.uniform=new CGL.Uniform(shader,'f','a',a);
a.set(1.0);

var height=0;

var vec=vec3.create();
var lastTextureChange=-1;
var disabled=false;

render.onTriggered=function()
{

    var font=getFont();
    if(font.lastChange!=lastTextureChange)
    {
        createMesh=true;
        lastTextureChange=font.lastChange;
    }

    if(createTexture) generateTexture();
    if(createMesh)generateMesh();

    if(mesh && mesh.numInstances>0)
    {
        cgl.pushBlendMode(CGL.BLEND_NORMAL,true);
        cgl.pushShader(shader);

        cgl.setTexture(0,textureOut.get().tex);

        if(valignMode==2) vec3.set(vec, 0,height,0);
        if(valignMode==1) vec3.set(vec, 0,0,0);
        if(valignMode==0) vec3.set(vec, 0,height/2,0);
        vec[1]-=lineHeight.get();
        cgl.pushModelMatrix();
        mat4.translate(cgl.mMatrix,cgl.mMatrix, vec);
        if(!disabled)mesh.render(cgl.getShader());

        cgl.popModelMatrix();

        cgl.setTexture(0,null);
        cgl.popShader();
        cgl.popBlendMode();
    }

    next.trigger();
};

letterSpace.onChange=function()
{
    createMesh=true;
};


function generateMesh()
{
    var theString=String(str.get()+'');
    if(!textureOut.get())return;

    var font=getFont();
    if(!font.geom)
    {
        font.geom=new CGL.Geometry("textmesh");

        font.geom.vertices = [
            1.0, 1.0, 0.0,
            0.0, 1.0, 0.0,
            1.0, 0.0, 0.0,
            0.0, 0.0, 0.0
        ];

        font.geom.texCoords = new Float32Array([
            1.0, 1.0,
            0.0, 1.0,
            1.0, 0.0,
            0.0, 0.0
        ]);

        font.geom.verticesIndices = [
            0, 1, 2,
            3, 1, 2
        ];
    }

    if(!mesh)mesh=new CGL.Mesh(cgl,font.geom);

    var strings=(theString).split('\n');

    var transformations=[];
    var tcOffsets=[];//new Float32Array(str.get().length*2);
    var tcSize=[];//new Float32Array(str.get().length*2);
    var charCounter=0;
    createTexture=false;
    var m=mat4.create();


    for(var s=0;s<strings.length;s++)
    {
        var txt=strings[s];
        var numChars=txt.length;

        var pos=0;
        var offX=0;
        var width=0;

        for(var i=0;i<numChars;i++)
        {
            var chStr=txt.substring(i,i+1);
            var char=font.chars[String(chStr)];
            if(char) width+=(char.texCoordWidth/char.texCoordHeight);
        }

        height=0;

        if(align.get()=='left') offX=0;
        else if(align.get()=='right') offX=width;
        else if(align.get()=='center') offX=width/2;

        height=(s+1)*lineHeight.get();

        for(var i=0;i<numChars;i++)
        {
            var chStr=txt.substring(i,i+1);
            var char=font.chars[String(chStr)];


            if(!char)
            {
                createTexture=true;
                return;
            }
            else
            {
                tcOffsets.push(char.texCoordX,1-char.texCoordY-char.texCoordHeight);
                tcSize.push(char.texCoordWidth,char.texCoordHeight);

                mat4.identity(m);
                mat4.translate(m,m,[pos-offX,0-s*lineHeight.get(),0]);

                pos+=(char.texCoordWidth/char.texCoordHeight)+letterSpace.get();
                transformations.push(Array.prototype.slice.call(m));

                charCounter++;
            }
        }
    }

    var transMats = [].concat.apply([], transformations);

    disabled=false;
    if(transMats.length==0)disabled=true;

    mesh.numInstances=transMats.length/16;

    if(mesh.numInstances==0)
    {
        disabled=true;
        return;
    }

    mesh.setAttribute('instMat',new Float32Array(transMats),16,{"instanced":true});
    mesh.setAttribute('attrTexOffsets',new Float32Array(tcOffsets),2,{"instanced":true});
    mesh.setAttribute('attrTexSize',new Float32Array(tcSize),2,{"instanced":true});

    createMesh=false;

    if(createTexture) generateTexture();
}

function printChars(fontSize,simulate)
{
    var font=getFont();
    if(!simulate) font.chars={};

    var ctx=font.ctx;

    ctx.font = fontSize+'px '+inFont.get();
    ctx.textAlign = "left";

    var posy=0,i=0;
    var posx=0;
    var lineHeight=fontSize*1.4;
    var result=
        {
            "fits":true
        };

    for(var i=0;i<font.characters.length;i++)
    {
        var chStr=String(font.characters.substring(i,i+1));
        var chWidth=(ctx.measureText(chStr).width);

        if(posx+chWidth>=textureSize)
        {
            posy+=lineHeight+2;
            posx=0;
        }

        if(!simulate)
        {
            font.chars[chStr]=
                {
                    str:chStr,
                    texCoordX:posx/textureSize,
                    texCoordY:posy/textureSize,
                    texCoordWidth:chWidth/textureSize,
                    texCoordHeight:lineHeight/textureSize,
                };

            ctx.fillText(chStr, posx, posy+fontSize);
        }

        posx+=chWidth+BIASX;
    }

    if(posy>textureSize-lineHeight)
    {
        result.fits=false;
    }

    result.spaceLeft=textureSize-posy;

    return result;
}

function generateTexture()
{
    var font=getFont();
    var string=String(str.get());
    if(string==null || string==undefined)string='';
    for(var i=0;i<string.length;i++)
    {
        var ch=string.substring(i,i+1);
        if(font.characters.indexOf(ch)==-1)
        {
            font.characters+=ch;
            createTexture=true;
        }
    }

    var ctx=font.ctx;
    font.canvas.width=font.canvas.height=textureSize;

    if(!font.texture)
        font.texture=CGL.Texture.createFromImage(cgl,font.canvas,
            {
                filter:CGL.Texture.FILTER_MIPMAP
            });

    font.texture.setSize(textureSize,textureSize);

    ctx.fillStyle = 'transparent';
    ctx.clearRect(0,0,textureSize,textureSize);
    ctx.fillStyle = 'rgba(255,255,255,255)';

    var fontSize=font.fontSize+40;

    var simu=printChars(fontSize,true);
    while(!simu.fits)
    {
        fontSize-=5;
        simu=printChars(fontSize,true);
    }

    printChars(fontSize,false);

    ctx.restore();

    font.texture.initTexture(font.canvas,CGL.Texture.FILTER_MIPMAP);
    font.texture.unpackAlpha=true;
    textureOut.set(font.texture);

    font.lastChange=CABLES.now();

    createMesh=true;
    createTexture=false;
}


};

Ops.Gl.Meshes.TextMesh.prototype = new CABLES.Op();
CABLES.OPS["780487b9-503c-4e2c-913f-e1ffda7a6329"]={f:Ops.Gl.Meshes.TextMesh,objName:"Ops.Gl.Meshes.TextMesh"};




// **************************************************************
// 
// Ops.Gl.TextureEffects.RectangleTexture_v3
// 
// **************************************************************

Ops.Gl.TextureEffects.RectangleTexture_v3 = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={rectangle_frag:"IN vec2 texCoord;\nUNI sampler2D tex;\n\nUNI float width;\nUNI float height;\nUNI float x;\nUNI float y;\n\nUNI float r;\nUNI float g;\nUNI float b;\nUNI float a;\n\nUNI float aspect;\nUNI float amount;\nUNI float rotate;\nUNI float roundness;\n\n#define DEG2RAD 0.785398163397\n\n{{CGL.BLENDMODES}}\n\nmat2 rot(float angle)\n{\n    float s=sin(angle);\n    float c=cos(angle);\n\n    return mat2(c,-s,s,c);\n}\n\n// polynomial smooth min (k = 0.1);\n// float smin( float a, float b, float k )\n// {\n//     float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );\n//     return mix( b, a, h ) - k*h*(1.0-h);\n// }\n\nvoid main()\n{\n    vec4 base=texture(tex,texCoord);\n    vec4 col;\n    vec2 p=texCoord;\n\n    p.y=1.0-p.y;\n    p.y*=aspect;\n    float d=1.0;\n\n    vec2 pp=vec2(p.x-x,p.y-y);\n    pp-=vec2(width/2.0,height/2.0);\n    pp=pp*rot(rotate*DEG2RAD/45.0);\n\n    float roundn=roundness*min(width,height);\n\n    vec2 size=max(vec2(width/2.0,height/2.0)-roundn,0.0);\n    vec2 absPos=abs(pp)-size;\n\n    d=max(absPos.x,absPos.y);\n    d=min(d,length(max(absPos,0.0))-roundn);\n    d=step(0.0,d);\n\n    // d+=absPos.x+1.0;\n    // d=max(d,0.0);\n    // d=max(d,0.0);\n\n    // col = vec4( (1.0-d)*vec3(r,g,b),1.0);\n\n    col=vec4( _blend(base.rgb,vec3(r,g,b)) ,1.0);\n    col=vec4( mix( col.rgb, base.rgb ,1.0-base.a*(1.0-d)*amount),1.0);\n    outColor=col;\n    // outColor= cgl_blend(base,col,amount);\n\n\n}\n\n\n\n",};
const render=op.inTrigger('render'),
    blendMode=CGL.TextureEffect.AddBlendSelect(op,"Blend Mode","normal"),
    amount=op.inValueSlider("Amount",1),
    inWidth=op.inValueSlider("Width",0.25),
    inHeight=op.inValueSlider("Height",0.25),
    inAspect=op.inBool("Aspect Ratio",false),
    inPosX=op.inValueSlider("X",0.5),
    inPosY=op.inValueSlider("Y",0.5),
    inRot=op.inValue("Rotate",0),
    inRoundness=op.inValueSlider("roundness",0);

const r = op.inValueSlider("r", Math.random()),
    g = op.inValueSlider("g", Math.random()),
    b = op.inValueSlider("b", Math.random()),
    a = op.inValueSlider("a",1.0);
r.setUiAttribs({ colorPick: true });

op.setPortGroup("Size",[inWidth,inHeight,inAspect]);
op.setPortGroup("Position",[inPosX,inPosY]);
op.setPortGroup("Color",[r,g,b,a]);


var trigger=op.outTrigger('trigger');

var cgl=op.patch.cgl;
var shader=new CGL.Shader(cgl,'textureeffect rectangle');
shader.setSource(shader.getDefaultVertexShader(),attachments.rectangle_frag||'');
var textureUniform=new CGL.Uniform(shader,'t','tex',0);

var uniHeight=new CGL.Uniform(shader,'f','height',inHeight);
var unWidth=new CGL.Uniform(shader,'f','width',inWidth);
var uniX=new CGL.Uniform(shader,'f','x',inPosX);
var uniY=new CGL.Uniform(shader,'f','y',inPosY);
var uniRot=new CGL.Uniform(shader,'f','rotate',inRot);
var uniRoundness=new CGL.Uniform(shader,'f','roundness',inRoundness);

r.set(1.0);
g.set(1.0);
b.set(1.0);
a.set(1.0);

var uniformR=new CGL.Uniform(shader,'f','r',r);
var uniformG=new CGL.Uniform(shader,'f','g',g);
var uniformB=new CGL.Uniform(shader,'f','b',b);
var uniformA=new CGL.Uniform(shader,'f','a',a);
var uniformAspect=new CGL.Uniform(shader,'f','aspect',1);

CGL.TextureEffect.setupBlending(op,shader,blendMode,amount);
var uniformAmount=new CGL.Uniform(shader,'f','amount',amount);

render.onTriggered=function()
{
    if(!CGL.TextureEffect.checkOpInEffect(op)) return;

    cgl.pushShader(shader);
    cgl.currentTextureEffect.bind();

    const texture=cgl.currentTextureEffect.getCurrentSourceTexture();
    if(inAspect.get()) uniformAspect.set(texture.height/texture.width);
    else uniformAspect.set(1);


    cgl.setTexture(0, texture.tex );

    cgl.currentTextureEffect.finish();
    cgl.popShader();

    trigger.trigger();
};



};

Ops.Gl.TextureEffects.RectangleTexture_v3.prototype = new CABLES.Op();
CABLES.OPS["6570ce79-8940-4684-80f1-ff42ce6190e2"]={f:Ops.Gl.TextureEffects.RectangleTexture_v3,objName:"Ops.Gl.TextureEffects.RectangleTexture_v3"};




// **************************************************************
// 
// Ops.Trigger.SetNumberOnTrigger
// 
// **************************************************************

Ops.Trigger.SetNumberOnTrigger = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const
    setValuePort = op.inTriggerButton("Set"),
    valuePort = op.inValueFloat("Number"),
    outNext=op.outTrigger("Next"),
    outValuePort = op.outValue("Out Value");

outValuePort.changeAlways = true;

setValuePort.onTriggered = function()
{
    outValuePort.set(valuePort.get());
    outNext.trigger();
};

};

Ops.Trigger.SetNumberOnTrigger.prototype = new CABLES.Op();
CABLES.OPS["9989b1c0-1073-4d5f-bfa0-36dd98b66e27"]={f:Ops.Trigger.SetNumberOnTrigger,objName:"Ops.Trigger.SetNumberOnTrigger"};




// **************************************************************
// 
// Ops.Gl.Meshes.FullscreenRectangle
// 
// **************************************************************

Ops.Gl.Meshes.FullscreenRectangle = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={shader_frag:"UNI sampler2D tex;\nIN vec2 texCoord;\n\nvoid main()\n{\n   outColor= texture(tex,vec2(texCoord.x,(1.0-texCoord.y)));\n}\n",shader_vert:"{{MODULES_HEAD}}\n\nIN vec3 vPosition;\nUNI mat4 projMatrix;\nUNI mat4 mvMatrix;\n\nOUT vec2 texCoord;\nIN vec2 attrTexCoord;\n\nvoid main()\n{\n   vec4 pos=vec4(vPosition,  1.0);\n\n   texCoord=attrTexCoord;\n\n   gl_Position = projMatrix * mvMatrix * pos;\n}\n",};
const
    render=op.inTrigger('render'),
    centerInCanvas=op.inValueBool("Center in Canvas"),
    flipY=op.inValueBool("Flip Y"),
    flipX=op.inValueBool("Flip X"),
    inTexture=op.inTexture("Texture"),
    trigger=op.outTrigger('trigger');

const cgl=op.patch.cgl;
var mesh=null;
var geom=new CGL.Geometry("fullscreen rectangle");
var x=0,y=0,z=0,w=0,h=0;

centerInCanvas.onChange=rebuild;
    flipX.onChange=rebuildFlip;
    flipY.onChange=rebuildFlip;

const shader=new CGL.Shader(cgl,'fullscreenrectangle');
shader.setModules(['MODULE_VERTEX_POSITION','MODULE_COLOR','MODULE_BEGIN_FRAG']);

shader.setSource(attachments.shader_vert,attachments.shader_frag);
shader.fullscreenRectUniform=new CGL.Uniform(shader,'t','tex',0);

var useShader=false;
var updateShaderLater=true;
render.onTriggered=doRender;

op.toWorkPortsNeedToBeLinked(render);

inTexture.onChange=function()
{
    updateShaderLater=true;
};

function updateShader()
{
    var tex=inTexture.get();
    if(tex) useShader=true;
        else useShader=false;
}

op.preRender=function()
{
    updateShader();
    // if(useShader)
    {
        shader.bind();
        if(mesh)mesh.render(shader);
        doRender();
    }
};

function doRender()
{
    if( cgl.getViewPort()[2]!=w || cgl.getViewPort()[3]!=h ||!mesh ) rebuild();

    if(updateShaderLater) updateShader();

    cgl.pushPMatrix();
    mat4.identity(cgl.pMatrix);
    mat4.ortho(cgl.pMatrix, 0, w,h, 0, -10.0, 1000);

    cgl.pushModelMatrix();
    mat4.identity(cgl.mMatrix);

    cgl.pushViewMatrix();
    mat4.identity(cgl.vMatrix);

    if(centerInCanvas.get())
    {
        var x=0;
        var y=0;
        if(w<cgl.canvasWidth) x=(cgl.canvasWidth-w)/2;
        if(h<cgl.canvasHeight) y=(cgl.canvasHeight-h)/2;

        cgl.setViewPort(x,y,w,h);
    }

    if(useShader)
    {
        if(inTexture.get())
        {
            cgl.setTexture(0,inTexture.get().tex);
            // cgl.gl.bindTexture(cgl.gl.TEXTURE_2D, inTexture.get().tex);
        }

        mesh.render(shader);
    }
    else
    {
        mesh.render(cgl.getShader());
    }

    cgl.gl.clear(cgl.gl.DEPTH_BUFFER_BIT);

    cgl.popPMatrix();
    cgl.popModelMatrix();
    cgl.popViewMatrix();

    trigger.trigger();
}

function rebuildFlip()
{
    mesh=null;
}


function rebuild()
{
    const currentViewPort=cgl.getViewPort();

    if(currentViewPort[2]==w && currentViewPort[3]==h && mesh)return;

    var xx=0,xy=0;

    w=currentViewPort[2];
    h=currentViewPort[3];

    geom.vertices = new Float32Array([
         xx+w, xy+h,  0.0,
         xx,   xy+h,  0.0,
         xx+w, xy,    0.0,
         xx,   xy,    0.0
    ]);

    var tc=null;

    if(flipY.get())
        tc=new Float32Array([
            1.0, 0.0,
            0.0, 0.0,
            1.0, 1.0,
            0.0, 1.0
        ]);
    else
        tc=new Float32Array([
            1.0, 1.0,
            0.0, 1.0,
            1.0, 0.0,
            0.0, 0.0
        ]);

    if(flipX.get())
    {
        tc[0]=0.0;
        tc[2]=1.0;
        tc[4]=0.0;
        tc[6]=1.0;
    }

    geom.setTexCoords(tc);

    geom.verticesIndices = new Float32Array([
        2, 1, 0,
        3, 1, 2
    ]);


    geom.vertexNormals=new Float32Array([
        0,0,1,
        0,0,1,
        0,0,1,
        0,0,1,
        ]);
    geom.tangents=new Float32Array([
        -1,0,0,
        -1,0,0,
        -1,0,0,
        -1,0,0]);
    geom.biTangents==new Float32Array([
        0,-1,0,
        0,-1,0,
        0,-1,0,
        0,-1,0]);

                // norms.push(0,0,1);
                // tangents.push(-1,0,0);
                // biTangents.push(0,-1,0);


    if(!mesh) mesh=new CGL.Mesh(cgl,geom);
        else mesh.setGeom(geom);
}


};

Ops.Gl.Meshes.FullscreenRectangle.prototype = new CABLES.Op();
CABLES.OPS["255bd15b-cc91-4a12-9b4e-53c710cbb282"]={f:Ops.Gl.Meshes.FullscreenRectangle,objName:"Ops.Gl.Meshes.FullscreenRectangle"};




// **************************************************************
// 
// Ops.Gl.Textures.ColorTexture
// 
// **************************************************************

Ops.Gl.Textures.ColorTexture = function()
{
CABLES.Op.apply(this,arguments);
const op=this;
const attachments={};
const r = op.inValueSlider("r", Math.random());
const g = op.inValueSlider("g", Math.random());
const b = op.inValueSlider("b", Math.random());
const a = op.inValueSlider("a", 1.0);
const texOut=op.outTexture("texture_out");

r.setUiAttribs({ colorPick: true });
const cgl=op.patch.cgl;

var fb=null;

r.onChange=
    g.onChange=
    b.onChange=
    a.onChange=render;

render();

function render()
{
    if(!fb)
    {
        if(cgl.glVersion==1) fb=new CGL.Framebuffer(cgl,4,4);
            else fb=new CGL.Framebuffer2(cgl,4,4);
        fb.setFilter(CGL.Texture.FILTER_MIPMAP);
    }

    fb.renderStart();
    cgl.gl.clearColor(r.get(),g.get(),b.get(),a.get());
    cgl.gl.clear(cgl.gl.COLOR_BUFFER_BIT);
    fb.renderEnd();

    texOut.set(fb.getTextureColor());
}

};

Ops.Gl.Textures.ColorTexture.prototype = new CABLES.Op();
CABLES.OPS["59b94270-0364-4c0f-a9fc-ba2561696a23"]={f:Ops.Gl.Textures.ColorTexture,objName:"Ops.Gl.Textures.ColorTexture"};


window.addEventListener('load', function(event) {
CABLES.jsLoaded=new Event('CABLES.jsLoaded');
document.dispatchEvent(CABLES.jsLoaded);
});
